static const NSInteger kPinCodeLength = 4;

#import "RPSignInVC.h"
#import "RPServerManager.h"
#import "RPUserServerModel.h"
#import "RPRouter.h"
#import "SHSPhoneLibrary.h"
#import "RPTextField.h"

@interface RPSignInVC () <UITextFieldDelegate, RPTextFieldProtocol>

@property (weak, nonatomic) IBOutlet UITextField *pinCodeTextField;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;

@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *pinCodeView;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *allTextFields;

@end

@implementation RPSignInVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

#pragma mark - Methods

- (void)initialize{
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
}

#pragma mark - Getters

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if ([self phone].length != 10) {
        [self.emailView shake];
        return;
    }
    if (self.pinCodeTextField.text.length != kPinCodeLength) {
        [self.pinCodeView shake];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"login"] = [self phone];
    params[@"user_type"] = @(self.userType);
    params[@"pin"] = self.pinCodeTextField.text;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postLoginWithParams:[params copy] onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [RPRouter setMainViewController];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (IBAction)rememberPasswordButtonPressed:(id)sender {
    if ([self phone].length != 10) {
        [self.emailView shake];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"phone"] = [self phone];
    params[@"user_type"] = @(self.userType);
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postRecoverPasswordWithParams:params onSuccess:^(RPErrorServerModel *error, NSString *info) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (info){
            [self showMessage:info];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.phoneTextField4) {
        [self.pinCodeTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""] && textField == self.pinCodeTextField) {
        return YES;
    }
    if (textField == self.pinCodeTextField && textField.text.length == kPinCodeLength) {
        return NO;
    }
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    if (textField == _pinCodeTextField) {
        return isNumeric;
    }
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            if (textField == self.phoneTextField4) {
                [self.pinCodeTextField becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return NO;
}

@end
