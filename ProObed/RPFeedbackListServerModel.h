//
//  RPFeedbackTypes.h
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPFeedbackServerModel;

@interface RPFeedbackListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPFeedbackServerModel *> *types;

+(EKObjectMapping *)objectMapping;

@end

@interface RPFeedbackServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger ID;

@end
