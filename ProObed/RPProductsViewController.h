//
//  RPProductsViewController.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPMenuServerModel;

@interface RPProductsViewController : UIViewController
@property (strong, nonatomic) RPMenuServerModel *menu;

- (void)calculateTotalPrice;
@end
