//
//  RPStockDetailVC+UIViewControllerTransitioningDelegate.m
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockDetailVC+UIViewControllerTransitioningDelegate.h"

@implementation RPStockDetailVC (UIViewControllerTransitioningDelegate)

#pragma mark - <RMPZoomTransitionAnimating>

- (UIImageView *)transitionSourceImageView
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.stockImageView.image];
    imageView.clipsToBounds = YES;
    imageView.contentMode = self.stockImageView.contentMode;
    imageView.userInteractionEnabled = NO;
    imageView.frame = [self.stockImageView convertRect:self.stockImageView.bounds toView:self.view];
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_1"]];
}

- (CGRect)transitionDestinationImageViewFrame
{
    return  [self.stockImageView convertRect:self.stockImageView.bounds toView:self.view];
}

#pragma mark - <RMPZoomTransitionDelegate>

- (void)zoomTransitionAnimator:(RMPZoomTransitionAnimator *)animator
         didCompleteTransition:(BOOL)didComplete
      animatingSourceImageView:(UIImageView *)imageView
{
    NSLog(@"finish 2");
    self.stockImageView.image = imageView.image;
}

@end
