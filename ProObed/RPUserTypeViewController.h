//
//  RPUserTypeViewController.h
//  ProObed
//
//  Created by Ruslan on 19.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPUserTypeViewController : UIViewController

@property (nonatomic, assign) BOOL isLogin;

@end
