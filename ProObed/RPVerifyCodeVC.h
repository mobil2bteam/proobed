//
//  RPVerifyCodeVC.h
//  ProObed
//
//  Created by Ruslan on 2/6/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPVerifyCodeVC : UIViewController
@property (strong, nonatomic) RPCityServerModel *selectedCity;
@property (strong, nonatomic) RPCompanyServerModel *selectedCompany;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *patronymic;
@property (strong, nonatomic) NSString *phone;
@property (assign, nonatomic) NSInteger info;
@property (strong, nonatomic) NSString *numberID;
@property (strong, nonatomic) NSString *gender;
@property (nonatomic, assign) UserType user_type;
@end
