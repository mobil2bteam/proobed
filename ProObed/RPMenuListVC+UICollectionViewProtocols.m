//
//  RPMenuListVC+UICollectionViewProtocols.m
//  ProObed
//
//  Created by Ruslan on 2/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

static CGFloat const kMenuCollectionViewCellHeight = 50.f;
static NSInteger const kDateCollectionViewNumberOfVisibleCells = 3;
static CGFloat const kDateCollectionViewCellPadding = 10.f;

#import "RPMenuListVC+UICollectionViewProtocols.h"
#import "RPMenuCollectionViewCell.h"
#import "RPMenuHeaderCollectionViewCell.h"
#import "RPDateCollectionViewCell.h"
#import "RPMenuListServerModel.h"

@implementation RPMenuListVC (UICollectionViewProtocols)

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.dateCollectionView) {
        return self.menuList.menu.count;
    }
    if (collectionView == self.menuCollectionView && self.menuList.menu.count) {
        return self.menuList.menu[self.selectedDateIndex].groups[section].items.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (collectionView == self.dateCollectionView) {
        return 1;
    }
    if (collectionView == self.menuCollectionView && self.menuList.menu.count) {
        return self.menuList.menu[self.selectedDateIndex].groups.count;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.menuCollectionView) {
        CGFloat height, width;
        width = CGRectGetWidth(self.menuCollectionView.bounds);
        height = kMenuCollectionViewCellHeight;
        return CGSizeMake(width, height);
    } else {
        CGFloat height, width;
        width = (CGRectGetWidth(self.dateCollectionView.bounds) - (kDateCollectionViewNumberOfVisibleCells - 1) * kDateCollectionViewCellPadding) / kDateCollectionViewNumberOfVisibleCells;
        height = CGRectGetHeight(self.dateCollectionView.bounds);
        return CGSizeMake(width, height);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.dateCollectionView) {
        return 10.0;
    }
    return 0.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.dateCollectionView) {
        RPDateCollectionViewCell *cell = (RPDateCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPDateCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWithDate:self.menuList.menu[indexPath.row].date];
        if (indexPath.row == self.selectedDateIndex) {
            [cell setSelected:YES];
        }
        return cell;
    } else {
        RPMenuCollectionViewCell *cell = (RPMenuCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPMenuCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWithItem:self.menuList.menu[self.selectedDateIndex].groups[indexPath.section].items[indexPath.row]];
        return cell;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPMenuHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPMenuHeaderCollectionViewCell class]) forIndexPath:indexPath];
        [headerView configureCellWithGroup:self.menuList.menu[self.selectedDateIndex].groups[indexPath.section]];
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (collectionView == self.menuCollectionView) {
        return CGSizeMake(CGRectGetWidth(self.menuCollectionView.frame), kMenuCollectionViewCellHeight);
    } else {
        return CGSizeZero;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.dateCollectionView) {
        self.selectedDateIndex = indexPath.row;
        [self.dateCollectionView reloadData];
        [self.dateCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [self.menuCollectionView reloadData];
    }
}

@end
