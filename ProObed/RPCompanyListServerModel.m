
#import "RPCompanyListServerModel.h"

@implementation RPCompanyListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPCompanyServerModel class] forKeyPath:@"companies"];
        [mapping hasMany:[RPCompanyServerModel class] forKeyPath:@"schools"];
    }];
}

@end


@implementation RPCompanyServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"cityId", @"prefix", @"type"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"description" toProperty:@"company_description"];
        [mapping hasMany:[RPDeliveryServerModel class] forKeyPath:@"receipts" forProperty:@"deliveries"];
    }];
}

@end


@implementation RPDeliveryServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping hasMany:[RPWorkServerModel class] forKeyPath:@"works"];
    }];
}

@end


@implementation RPWorkServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"hours_start", @"hours_finish", @"free_day"]];
    }];
}

@end

