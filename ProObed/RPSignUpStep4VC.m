#import "RPSignUpStep4VC.h"
#import "RPSignUpStep5VC.h"
#import "RPSignUpStep6VC.h"

@interface RPSignUpStep4VC ()

@end

@implementation RPSignUpStep4VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Actions

- (IBAction)yesButtonPressed:(id)sender {
    RPSignUpStep5VC *signUpVC = [[RPSignUpStep5VC alloc]initWithNib];
    signUpVC.user_type = self.user_type;
    signUpVC.selectedCity = self.selectedCity;
    signUpVC.selectedCompany = self.selectedCompany;
    signUpVC.name = self.name;
    signUpVC.lastName = self.lastName;
    signUpVC.patronymic = self.patronymic;
    signUpVC.gender = self.gender;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

- (IBAction)noButtonPressed:(id)sender {
    RPSignUpStep6VC *signUpVC = [[RPSignUpStep6VC alloc]initWithNib];
    signUpVC.user_type = self.user_type;
    signUpVC.selectedCity = self.selectedCity;
    signUpVC.selectedCompany = self.selectedCompany;
    signUpVC.name = self.name;
    signUpVC.lastName = self.lastName;
    signUpVC.gender = self.gender;
    signUpVC.patronymic = self.patronymic;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

@end
