#import "RPLoadDataVC.h"
#import "RPServerManager.h"
#import "UIViewController+Alert.h"
#import "MBProgressHUD.h"
#import "RPRouter.h"
#import <Masonry/Masonry.h>

@implementation RPLoadDataVC

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addBackground];
    [self loadOptions];
}

- (void)addBackground{
    //добавляем фоновое изображение
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.translatesAutoresizingMaskIntoConstraints = NO;
    bgImageView.backgroundColor = [UIColor greenColor];
    bgImageView.image = [UIImage imageNamed:@"background_main"];
    [self.view insertSubview:bgImageView atIndex:0];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(self.view.mas_height);
        make.width.equalTo(self.view.mas_width);
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
}

#pragma mark - Load Options and necessary data

- (void)loadOptions{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]getOptionsOnSuccess:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [RPRouter setUpStartController];
    } onFailure:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [self showMessage:RPErrorMessage withRepeatHandler:^(UIAlertAction *action) {
//            [self loadOptions];
//        }];
        [self showMessage:RPErrorMessage withCompilation:^{
            [self loadOptions];
        }];
    }];
}

@end
