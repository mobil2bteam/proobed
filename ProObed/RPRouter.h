//
//  RPRouter.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPRouter : NSObject

+ (void)setUpStartController;

+ (void)setMainViewController;

+ (void)setMainViewControllerWithCity:(RPCityServerModel *) city company:(RPCompanyServerModel *)company;

@end
