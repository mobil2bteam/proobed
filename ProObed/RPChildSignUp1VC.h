//
//  RPChildSignUp1VC.h
//  ProObed
//
//  Created by Ruslan on 19.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPCityServerModel;
@class RPCompanyServerModel;
@class RPUserServerModel;

@interface RPChildSignUp1VC : UIViewController
@property (nonatomic, strong) RPCityServerModel *selectedCity;
@property (nonatomic, strong) RPCompanyServerModel *selectedSchool;
@property (nonatomic, strong) RPUserServerModel *child;
@property (nonatomic, strong) NSString *number;
@end
