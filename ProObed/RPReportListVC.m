static CGFloat const kReportTableViewCellHight = 50.f;
#define kUserTickets kAppDelegate.currentUser.info.tickets
#define kUserBalls kAppDelegate.currentUser.info.balls

#import "RPReportListVC.h"
#import "RPReportTableViewCell.h"
#import "RPBallServerModel.h"
#import "RPTicketListServerModel.h"

@interface RPReportListVC ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UIView *paperView;

@property (weak, nonatomic) IBOutlet UITableView *reportTableView;

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@end

@implementation RPReportListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    
}

- (IBAction)swap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Methods

- (void)initialize{
    // set font to UISegmentControl
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Schist Regular" size:20]};
    [self.segmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    // set backgroundColor from Image
    self.paperView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papper"]];
    
    // if user has more than one ticket set total balance
    self.totalLabel.textColor = kRedColor;

    if (kUserTickets) {
        if (kUserTickets.count != 0) {
            NSInteger totalBalance = kUserTickets[0].value;
            self.totalLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)totalBalance];
            //        self.totalLabel.textColor = totalBalance > 0 ? kGreenColor : kRedColor;
            self.totalLabel.textColor = kRedColor;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)segmentControlChanged:(id)sender {
    NSInteger totalBalance = 0;
    switch (self.segmentControl.selectedSegmentIndex) {
        case 0: //tickets
            totalBalance = kUserTickets.count ? kUserTickets[0].value : 0;
            break;
        case 1: //balls
            totalBalance = kUserBalls.value;
            break;
    }
    self.totalLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)totalBalance];
    self.totalLabel.textColor = kRedColor;
    [self.reportTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentControl.selectedSegmentIndex == 0) { //tickets
        if (kUserTickets.count) {
            return kUserTickets[0].operations.count;
        }
        return 0;
    } else if (self.segmentControl.selectedSegmentIndex == 1){ //balls
        return kUserBalls.operations.count;
    }
    return 0;
}

- (RPReportTableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPReportTableViewCell class]) forIndexPath:indexPath];
    switch (self.segmentControl.selectedSegmentIndex) {
        case 0: //tickets
            [cell configureCellWithTicketOperation:kUserTickets[0].operations[indexPath.row]];
            break;
        case 1: //balls
            [cell configureCellWithOperation:kUserBalls.operations[indexPath.row]];
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kReportTableViewCellHight;
}

@end
