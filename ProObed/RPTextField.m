//
//  RPTextField.m
//  ProObed
//
//  Created by Ruslan on 6/1/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPTextField.h"

@implementation RPTextField

- (void)deleteBackward;
{
    [super deleteBackward];
    if (self.RP_delegate) {
        [self.RP_delegate RP_textFieldDeleteBackwardPressed:self];
    }
}

@end
