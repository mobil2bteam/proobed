//
//  RPProductCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^RPProductCollectionViewCellBlock)(void);

@class RPItemServerModel;
@class RPCartServerModel;

@interface RPProductCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIStepper *countStepper;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIView *stepperContainerView;
@property (nonatomic, copy) RPProductCollectionViewCellBlock block;

- (void)configureCellWithItem:(RPItemServerModel *) item;
- (void)configureCellWithProduct:(RPCartServerModel *) product;

@end
