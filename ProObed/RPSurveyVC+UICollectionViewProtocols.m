//
//  RPSurveyVC+UICollectionViewProtocols.m
//  ProObed
//
//  Created by Ruslan on 2/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPSurveyVC+UICollectionViewProtocols.h"
#import "RPSurveyCollectionViewCell.h"
#import "RPSurveyHeaderCollectionViewCell.h"
#import "RPSurveyServerModel.h"

@implementation RPSurveyVC (UICollectionViewProtocols)

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.survey.questions[section].options.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.survey.questions.count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.surveyCollectionView.bounds), 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPSurveyCollectionViewCell *cell = (RPSurveyCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPSurveyCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithOption:self.survey.questions[indexPath.section].options[indexPath.row] answerType:self.survey.questions[indexPath.section].type];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPSurveyHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPSurveyHeaderCollectionViewCell class]) forIndexPath:indexPath];
        headerView.questionLabel.text = [NSString stringWithFormat:@"%ld.) %@", (long)indexPath.row + 1, self.survey.questions[indexPath.section].text];
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CGRectGetWidth(self.surveyCollectionView.frame), 80);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    RPSurveyCollectionViewCell *cell = (RPSurveyCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    BOOL isSelected = self.survey.questions[indexPath.section].options[indexPath.row].isSelected == YES;
    if ([self.survey.questions[indexPath.section].type isEqualToString:@"radio"]) {
        for (NSInteger i = 0; i < self.survey.questions[indexPath.section].options.count; i++){
            RPSurveyCollectionViewCell *newCell = (RPSurveyCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
            [newCell checkAnswer:NO];
            self.survey.questions[indexPath.section].options[i].isSelected = NO;
        }
    }
    [cell checkAnswer:!isSelected];
    self.survey.questions[indexPath.section].options[indexPath.row].isSelected = !isSelected;
}

@end
