//
//  RPProductCollectionViewCell.m
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPProductCollectionViewCell.h"
#import "RPMenuListServerModel.h"
#import "RPOrderServerModel.h"

@interface RPProductCollectionViewCell()
@property (nonatomic, strong) RPItemServerModel *item;
@end

@implementation RPProductCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithItem:(RPItemServerModel *) item{
    self.item = item;
    self.menuLabel.text = item.name;
    if (item.price) {
        self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)item.price];
    }
    if (item.count) {
        [self.countStepper setValue:item.count];
        self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)item.count];
    }
}

- (void)configureCellWithProduct:(RPCartServerModel *) product{
    self.stepperContainerView.hidden = YES;
    self.menuLabel.text = product.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)product.price];
    self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)product.count];
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.priceLabel.text = @"";
    self.menuLabel.text = @"";
    self.countLabel.text = @"0";
    self.countStepper.value = 0;
}

- (IBAction)stepperValueChanged:(id)sender {
    NSInteger count = self.countStepper.value;
    self.item.count = count;
    self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)count];
    if (self.block) {
        self.block();
    }
}

@end
