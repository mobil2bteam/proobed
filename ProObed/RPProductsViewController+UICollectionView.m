//
//  RPProductsViewController+UICollectionView.m
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPProductsViewController+UICollectionView.h"
#import "RPMenuListServerModel.h"
#import "RPProductCollectionViewCell.h"
#import "RPMenuHeaderCollectionViewCell.h"

static CGFloat const kMenuCollectionViewCellHeight = 60.f;

@implementation RPProductsViewController (UICollectionView)

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.menu.groups[section].items.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.menu.groups.count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    width = CGRectGetWidth(collectionView.bounds);
    height = 80;
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPProductCollectionViewCell *cell = (RPProductCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPProductCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithItem:self.menu.groups[indexPath.section].items[indexPath.row]];
    __weak typeof(self) weakSelf = self;
    cell.block = ^{
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf calculateTotalPrice];
    };
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPMenuHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPMenuHeaderCollectionViewCell class]) forIndexPath:indexPath];
        [headerView configureCellWithGroup:self.menu.groups[indexPath.section]];
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame), kMenuCollectionViewCellHeight);
}

@end
