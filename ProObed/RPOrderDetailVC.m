#import "RPOrderDetailVC.h"
#import "RPOrderServerModel.h"
#import "RPProductCollectionViewCell.h"
#import "UICollectionView+Extensions.h"
#import "RPOrderProductTableViewCell.h"
#import "RPServerManager.h"
#import "RPOrderServerModel.h"
#import "RPServerManager.h"
#import "RPOrderDetailVC.h"

@interface RPOrderDetailVC ()
@property (weak, nonatomic) IBOutlet UIStackView *orderInfoStackView;
@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *workLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UITableView *productsTableView;
@end

@implementation RPOrderDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // если заказ отменет, скрыть кнопку отмены
    if (self.order.statusIdTest == 5) {
        self.cancelOrderButton.hidden = YES;
    }
    if (self.isNewOrder) {
        self.orderInfoStackView.hidden = YES;
        self.cancelOrderButton.hidden = YES;
        if (kAppDelegate.currentUser.info.orders.count > 0) {
            if (kAppDelegate.currentUser.info.orders[kAppDelegate.currentUser.info.orders.count - 1].statusIdTest == 4) {
                [self showMessage:@"Ваш заказ будет собран после полной оплаты"];
            }
        }
    }
//    self.backButton.hidden = self.isNewOrder == YES;
    self.okButton.hidden = !(self.isNewOrder == YES);
    [self.productsTableView registerNib:[UINib nibWithNibName:@"RPOrderProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.productsTableView.estimatedRowHeight = 60;
    self.productsTableView.rowHeight = UITableViewAutomaticDimension;
    self.productsTableView.tableFooterView = [[UIView alloc] init];
    self.dateLabel.text = self.order.date;
    self.orderStatusLabel.text = self.order.statusTest;
    self.orderNumberLabel.text = [NSString stringWithFormat:@"%@", self.order.number];
    self.nameLabel.text = self.order.point.name;
    self.addressLabel.text = self.order.point.address;
    self.workLabel.text = self.order.point.work;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld р.", (long)self.order.total];
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okButtonPressed:(id)sender {
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager] postOrderForProducts:self.cart date:self.date receiptId:self.point.ID onSuccess:^(RPOrderInfoServerModel *order, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (order) {
            strongSelf.order = order;
            [strongSelf finishOrder];
        } else {
            [strongSelf showMessage:error.text];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error.localizedDescription];
    }];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)cancelOrderButtonPressed:(id)sender {
    NSDictionary *params = @{@"order_id": [NSString stringWithFormat:@"%ld", (long)self.order.ID]};
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager] cancelOrderWithParams:params onSuccess:^(BOOL isSuccess, NSString *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (isSuccess) {
            [strongSelf finishCancelOrder];
        } else {
            [strongSelf showMessage:error];
        }
    } onFailure:^(NSString *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error];
    }];
}

- (void)finishOrder{
    self.dateLabel.text = self.order.date;
    self.orderStatusLabel.text = self.order.statusTest;
    self.orderNumberLabel.text = [NSString stringWithFormat:@"%@", self.order.number];
    self.orderInfoStackView.hidden = NO;
    self.cancelOrderButton.hidden = NO;
    self.okButton.hidden = YES;
    self.backButton.hidden = YES;
    self.closeButton.hidden = NO;
}

- (void)finishCancelOrder{
    __weak typeof(self) weakSelf = self;
    [self showMessage:@"Заказ успешно отменён" withCompilation:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (self.isNewOrder) {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
//            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.order.cart.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RPOrderProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.nameLabel.text = self.order.cart[indexPath.row].name;
    NSInteger price = self.order.cart[indexPath.row].price *  self.order.cart[indexPath.row].count;
    cell.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)price];
    cell.bottomPriceLabel.text = [NSString stringWithFormat:@"%ld x %ld руб.", (long)self.order.cart[indexPath.row].count, self.order.cart[indexPath.row].price];
    return cell;
}

@end
