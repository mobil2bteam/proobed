#import "RPAboutProjectVC.h"
#import "RPOptionsServerModel.h"

@interface RPAboutProjectVC ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RPAboutProjectVC
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *htmlString = kAppDelegate.options.about;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    self.textView.attributedText = attributedString;
    self.textView.textColor = [UIColor whiteColor];
    self.textView.font = [UIFont fontWithName:@"Schist Regular" size:17];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
