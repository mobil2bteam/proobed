#import "RPChildSignUp4VC.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPServerManager.h"
#import "RPUserServerModel.h"
#import "RPChildSignUp1VC.h"

@interface RPChildSignUp4VC ()
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@end

@implementation RPChildSignUp4VC

#pragma mark - Actions

- (void)viewDidLoad {
    [super viewDidLoad];
    self.schoolLabel.text = self.selectedSchool.name;
    self.schoolLabel.textColor = kGreenColor;
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (!self.numberTextField.text.length) {
        [self.numberTextField shake];
        return;
    }
    if (!self.surnameTextField.text.length) {
        [self.surnameTextField shake];
        return;
    }
    NSString *number = self.numberTextField.text;
    NSString *surname = self.surnameTextField.text;
    NSString *token = kAppDelegate.currentUser.info.accessToken;
    NSDictionary *params = @{@"ticket":number,
                             @"companyId": @(self.selectedSchool.ID),
                             @"token": token,
                             @"surname":surname};
    __weak typeof(self) weakSelf = self;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] getChildInfo:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (error) {
            [strongSelf showMessage:error.text];
        } else if (user){
            [strongSelf nextStep:user];
        } else {
            [strongSelf showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        NSLog(@"error - %@", error.localizedDescription);
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:RPErrorMessage];
    }];
}

- (void)nextStep:(RPUserServerModel *)child {
    RPChildSignUp1VC *vc = [[RPChildSignUp1VC alloc] initWithNib];
    vc.selectedSchool = self.selectedSchool;
    vc.selectedCity = self.selectedCity;
    vc.child = child;
    vc.number = self.numberTextField.text;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
