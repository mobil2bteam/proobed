//
//  RPSelectDateViewController.h
//  ProObed
//
//  Created by Ruslan on 25.04.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOrderServerModel;
@class RPPointServerModel;

@interface RPSelectDateViewController : UIViewController

@property (nonatomic, strong) RPOrderServerModel *order;
@property (nonatomic, strong) RPPointServerModel *point;
@property (nonatomic, copy) NSString *cart;

@end
