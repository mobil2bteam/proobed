//
//  RPStockListVC+UICollectionViewProtocols.m
//  ProObed
//
//  Created by Ruslan on 2/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockListVC+UICollectionViewProtocols.h"
#import "RPStockPreviewCollectionVeiwCell.h"
#import "RPStockListServerModel.h"
#import "RPStockDetailVC.h"

@implementation RPStockListVC (UICollectionViewProtocols)

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.stocks.past.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    width = CGRectGetWidth(self.stocksCollectionView.bounds);
    height = width / 1.5;
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    RPStockServerModel *selectedStock = self.stocks.past[indexPath.row];
    RPStockDetailVC *stockDetailVC = [[RPStockDetailVC alloc]initWithNib];
    stockDetailVC.stock = selectedStock;
    stockDetailVC.transitioningDelegate = self;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    stockDetailVC.view.frame = CGRectMake(0, 0, width, height);
    [stockDetailVC.view layoutIfNeeded];
    [self presentViewController:stockDetailVC animated:YES completion:nil];
}

- (RPStockPreviewCollectionVeiwCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPStockPreviewCollectionVeiwCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPStockPreviewCollectionVeiwCell class]) forIndexPath:indexPath];
    [cell configureCellWithStock:self.stocks.past[indexPath.row]];
    return cell;
}

@end
