
#import "RPRouter.h"
#import "RPStartVC.h"
#import "RPPinCodeVC.h"
#import "RPTransitionNavController.h"
#import "RPMainVC.h"
#import "RPParentMainVC.h"
#import "RPUserServerModel.h"
#import "RPChildVC.h"

@implementation RPRouter

+ (void)setUpStartController{
    RPStartVC *startVC = [[RPStartVC alloc] initWithNib];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    startVC.view.frame = CGRectMake(0, 0, width, height);
    [startVC.view layoutIfNeeded];

    //если пользователь авторизован
    if (kAppDelegate.currentUser) {
        //нужен ли пин-код для входа
        if ([kUserDefaults boolForKey:kAskPinCodeKey]) {
            RPPinCodeVC *pinCodeVC = [[RPPinCodeVC alloc]initWithNib];
            RPTransitionNavController *navVC = [[RPTransitionNavController alloc]init];
            navVC.viewControllers = @[startVC, pinCodeVC];
            [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
        } else {
            [self setMainViewController];
        }
    } else {
        RPTransitionNavController *navVC = [[RPTransitionNavController alloc]initWithRootViewController:startVC];
        [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
    }
}

+ (void)setMainViewController{
    if (kAppDelegate.currentUser.info.user_type == UserTypeWorker) {
        RPMainVC *mainVC = [[RPMainVC alloc]initWithNib];
        mainVC.isDemonstrateMode = NO;
        RPTransitionNavController *navVC = [[RPTransitionNavController alloc]initWithRootViewController:mainVC];
        [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
    }
    if (kAppDelegate.currentUser.info.user_type == UserTypeParent) {
        RPParentMainVC *mainVC = [[RPParentMainVC alloc]initWithNib];
        RPTransitionNavController *navVC = [[RPTransitionNavController alloc]initWithRootViewController:mainVC];
        [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
    }
    if (kAppDelegate.currentUser.info.user_type == UserTypeChild) {
        RPChildVC *vc = [[RPChildVC alloc] init];
        vc.isChildMode = YES;
        vc.child = kAppDelegate.currentUser;
        RPTransitionNavController *navVC = [[RPTransitionNavController alloc]initWithRootViewController:vc];
        [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
    }
}

+ (void)setMainViewControllerWithCity:(RPCityServerModel *) city company:(RPCompanyServerModel *)company{
    RPMainVC *mainVC = [[RPMainVC alloc]initWithNib];
    mainVC.selectedCompany = company;
    mainVC.selectedCity = city;
    mainVC.isDemonstrateMode = YES;
    RPTransitionNavController *navVC = [[RPTransitionNavController alloc]initWithRootViewController:mainVC];
    [[[UIApplication sharedApplication] delegate] window].rootViewController = navVC;
}
@end
