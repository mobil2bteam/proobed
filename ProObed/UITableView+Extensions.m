//
//  UITableView+Extensions.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UITableView+Extensions.h"

@implementation UITableView (Extensions)

@dynamic cellName;

- (void)registerCell:(Class)classType{
    NSString *nibName = NSStringFromClass(classType);
    [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void)setCellName:(NSString *)cellName{
    [self registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}

@end
