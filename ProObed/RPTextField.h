//
//  RPTextField.h
//  ProObed
//
//  Created by Ruslan on 6/1/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPTextField;

@protocol RPTextFieldProtocol <NSObject>

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField;

@end

@interface RPTextField : UITextField

@property (weak, nonatomic) id<RPTextFieldProtocol> RP_delegate;

@end
