//
//  UIViewController+Extensions.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+Extensions.h"

@implementation UIViewController (Extensions)

- (instancetype)initWithNib{
    NSString *nibBame = NSStringFromClass([self class]);
    return [self initWithNibName:nibBame bundle:nil];
}

- (void)RP_presentViewController:(UIViewController *)controller animated:(BOOL)animated{
    controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
    controller.modalTransitionStyle = UIModalPresentationCustom;
    [self presentViewController:controller animated:animated completion:nil];
}

- (void)RP_presentViewControllerCrossDisolve:(UIViewController *)controller animated:(BOOL)animated{
    controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:controller animated:animated completion:nil];
}

- (void)RP_customPresentViewController:(UIViewController *)controller animated:(BOOL)animated{
    [self RP_customPresentViewController:controller animated:animated delegate:nil];
}

- (void)RP_customPresentViewController:(UIViewController *)controller animated:(BOOL)animated delegate:(NSObject<UIViewControllerTransitioningDelegate> *)delegate{
    controller.transitioningDelegate = delegate;
    controller.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:controller animated:animated completion:nil];
}

@end
