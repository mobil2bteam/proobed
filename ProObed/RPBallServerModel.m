#import "RPBallServerModel.h"

@implementation RPBallServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPOperationServerModel class] forKeyPath:@"operations"];
        [mapping mapPropertiesFromArray:@[@"value"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];

    }];
}

@end

@implementation RPOperationServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"balsId"]];
        [mapping mapPropertiesFromArray:@[@"value"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];}

@end
