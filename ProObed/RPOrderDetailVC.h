//
//  RPOrderDetailVC.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOrderServerModel;
@class RPPointServerModel;

@class RPOrderInfoServerModel;

@interface RPOrderDetailVC : UIViewController
@property (nonatomic, strong) RPOrderInfoServerModel *order;
@property (nonatomic, assign) BOOL isNewOrder;
@property (nonatomic, strong) RPPointServerModel *point;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, copy) NSString *cart;

@end
