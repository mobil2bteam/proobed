
#import "RPSignUpStep8VC.h"
#import "RPRouter.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPServerManager.h"

@interface RPSignUpStep8VC ()
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;
@property (weak, nonatomic) IBOutlet UIView *cityView;
@property (weak, nonatomic) IBOutlet UIView *companyView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *numberView;
@end

@implementation RPSignUpStep8VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.user_type != UserTypeWorker) {
        self.companyView.hidden = YES;
        self.cityView.hidden = YES;
        self.numberView.hidden = YES;
    }
    self.emailLabel.text = [NSString stringWithFormat:@"+7%@", self.phone];;
    self.userNameLabel.text = [NSString stringWithFormat:@"%@\n%@", self.name, self.lastName];;
    self.cityLabel.text = self.selectedCity.name;
    self.companyLabel.text = self.selectedCompany.name;
    if (self.numberID) {
        self.pinLabel.text = [NSString stringWithFormat:@"N%@", self.numberID];
    } else {
        self.pinLabel.text = @"";
    }
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Actions

- (IBAction)menuButtonPressed:(id)sender {
    [self continueRegistration];
}

- (void)continueRegistration{
    switch (self.user_type) {
        case UserTypeWorker:
            [self signUpWorker];
            break;
        case UserTypeParent:
            [self signUpParent];
            break;
        case UserTypeChild:
            [self signUpPupil];
            break;
        default:
            break;
    }
}

- (void)signUpWorker{
    NSMutableDictionary *params = [@{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"phone":self.phone,
                                     @"cityId":@(self.selectedCity.ID),
                                     @"companyId":@(self.selectedCompany.ID),
                                     @"user_type":@(1),
                                     @"code":self.code,
                                     @"gender":self.gender,
                                     @"pin":self.pin} mutableCopy];
    if (self.numberID){
        params[@"number"] = self.numberID;
        params[@"type"] = @"verify";
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postRegisterWithParams:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (user){
            if (self.askPinCode == YES) {
                [kUserDefaults setBool:YES forKey:kAskPinCodeKey];
                [kUserDefaults synchronize];
            }
            [RPRouter setMainViewController];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpParent{
    NSMutableDictionary *params = [@{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"phone":self.phone,
                                     @"user_type":@(2),
                                     @"code":self.code,
                                     @"gender":self.gender,
                                     @"pin":self.pin} mutableCopy];
    if (self.numberID){
        params[@"number"] = self.numberID;
        params[@"type"] = @"verify";
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postRegisterWithParams:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (user){
            if (self.askPinCode == YES) {
                [kUserDefaults setBool:YES forKey:kAskPinCodeKey];
                [kUserDefaults synchronize];
            }
            [RPRouter setMainViewController];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpPupil{
    NSDictionary *params = @{@"phone":[self phone],
                             @"code":self.code,
                             @"pin":self.pin,
                             @"user_type":@(3)};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postVerifyChildWithParams:params onSuccess:^(RPUserServerModel *child, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (child){
            if (self.askPinCode == YES) {
                [kUserDefaults setBool:YES forKey:kAskPinCodeKey];
                [kUserDefaults synchronize];
            }
            [RPRouter setMainViewController];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

@end
