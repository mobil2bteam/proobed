//
//  RPOrderListVC.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOrderInfoServerModel;

@interface RPOrderListVC : UIViewController
@property (nonatomic, strong) NSArray <RPOrderInfoServerModel *> *orderList;
@end
