
#import "RPCache+CoreDataProperties.h"

@implementation RPCache (CoreDataProperties)

@dynamic key;
@dynamic params;
@dynamic seconds;
@dynamic date;
@dynamic json;

@end
