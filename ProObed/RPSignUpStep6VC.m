
#import "RPSignUpStep6VC.h"
#import "RPSignUpStep7VC.h"
#import "RPServerManager.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "SHSPhoneLibrary.h"
#import "RPVerifyCodeVC.h"
#import "RPTextField.h"
#import "RPSignUpStep3VC.h"

@interface RPSignUpStep6VC () <RPTextFieldProtocol>

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;

@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;

@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UIView *emailView;

@end

@implementation RPSignUpStep6VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.phoneTextField1 becomeFirstResponder];
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
}

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if ([self phone].length == 10) {
        [self continueRegistration];
    } else {
        [self.emailView shake];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    return YES;
}

#pragma mark - Methods

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

- (void)continueRegistration{
    switch (self.user_type) {
        case UserTypeWorker:
            [self signUpWorker];
            break;
        case UserTypeParent:
            [self signUpParent];
            break;
        case UserTypeChild:
            [self signUpPupil];
            break;
        default:
            break;
    }
}

- (void)signUpWorker{
    NSMutableDictionary *params = [@{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"cityId":@(self.selectedCity.ID),
                                     @"companyId":@(self.selectedCompany.ID),
                                     @"phone":[self phone],
                                     @"gender":self.gender,
                                     @"user_type":@(self.user_type)} mutableCopy];
    if (self.numberID){
        params[@"number"] = self.numberID;
        params[@"type"] = @"verify";
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postPreRegisterWithParams:params onSuccess:^(NSString *userInfo, RPErrorServerModel *error) {
        NSLog(@"user info - %@", userInfo);
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            RPVerifyCodeVC *signUpStep7VC = [[RPVerifyCodeVC alloc]initWithNib];
            signUpStep7VC.name = self.name;
            signUpStep7VC.gender = self.gender;
            signUpStep7VC.lastName = self.lastName;
            signUpStep7VC.patronymic = self.patronymic;
            signUpStep7VC.selectedCity = self.selectedCity;
            signUpStep7VC.selectedCompany = self.selectedCompany;
            signUpStep7VC.user_type = self.user_type;
            signUpStep7VC.phone = [self phone];
            if (self.numberID.length) {
                signUpStep7VC.numberID = self.numberID;
            }
            [self.navigationController pushViewController:signUpStep7VC animated:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpParent{
    NSDictionary *params = @{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"phone":[self phone],
                                     @"gender":self.gender,
//                                     @"companyId":@(1),
                                     @"user_type":@(self.user_type)};
    
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postPreRegisterWithParams:params onSuccess:^(NSString *userInfo, RPErrorServerModel *error) {
        NSLog(@"user info - %@", userInfo);
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            RPVerifyCodeVC *signUpStep7VC = [[RPVerifyCodeVC alloc]initWithNib];
            signUpStep7VC.name = self.name;
            signUpStep7VC.gender = self.gender;
            signUpStep7VC.lastName = self.lastName;
            signUpStep7VC.patronymic = self.patronymic;
            signUpStep7VC.selectedCity = self.selectedCity;
            signUpStep7VC.selectedCompany = self.selectedCompany;
            signUpStep7VC.user_type = self.user_type;
            signUpStep7VC.phone = [self phone];
            if (self.numberID.length) {
                signUpStep7VC.numberID = self.numberID;
            }
            [self.navigationController pushViewController:signUpStep7VC animated:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpPupil{
    NSDictionary *params = @{@"phone":[self phone],
                              @"user_type":@(3)};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postVerifyChildWithParams:params onSuccess:^(RPUserServerModel *child, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            RPVerifyCodeVC *signUpStep7VC = [[RPVerifyCodeVC alloc]initWithNib];
            signUpStep7VC.name = self.name;
            signUpStep7VC.gender = self.gender;
            signUpStep7VC.lastName = self.lastName;
            signUpStep7VC.patronymic = self.patronymic;
            signUpStep7VC.selectedCity = self.selectedCity;
            signUpStep7VC.selectedCompany = self.selectedCompany;
            signUpStep7VC.user_type = self.user_type;
            signUpStep7VC.phone = [self phone];
            if (self.numberID.length) {
                signUpStep7VC.numberID = self.numberID;
            }
            [self.navigationController pushViewController:signUpStep7VC animated:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

@end
