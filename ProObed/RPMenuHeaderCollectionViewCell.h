//
//  RPMenuHeaderCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 1/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPGroupServerModel;

@interface RPMenuHeaderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

- (void)configureCellWithGroup:(RPGroupServerModel *) group;

@end
