//
//  RPMenuListVC.h
//  ProObed
//
//  Created by Ruslan on 1/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPMenuListServerModel;

@interface RPMenuListVC : UIViewController

@property (strong, nonatomic) RPMenuListServerModel *menuList;

@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *dateCollectionView;

@property (assign, nonatomic) NSInteger selectedDateIndex;

@property (strong, nonatomic) NSDictionary *params;

@end
