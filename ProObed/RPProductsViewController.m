
#import "RPProductsViewController.h"
#import "RPMenuListServerModel.h"
#import "RPProductCollectionViewCell.h"
#import "RPMenuHeaderCollectionViewCell.h"
#import "UICollectionView+Extensions.h"
#import "RPOrderServerModel.h"
#import "RPServerManager.h"
#import "RPDeliveryListViewController.h"

@interface RPProductsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;
@end

@implementation RPProductsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.productsCollectionView registerNib:[RPProductCollectionViewCell class]];
    [self.productsCollectionView registerNib:[RPMenuHeaderCollectionViewCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader];
}

#pragma mark - Methods

- (void)calculateTotalPrice{
    NSInteger totalPrice = 0;
    for (RPGroupServerModel *group in self.menu.groups) {
        for (RPItemServerModel *item in group.items) {
            if (item.count) {
                totalPrice += item.count * item.price;
            }
        }
    }
    self.priceLabel.text = [NSString stringWithFormat:@"%ld р.", (long)totalPrice];
}

#pragma mark - Navigation

- (void)showConfirmOrderViewControllerForOrder:(RPOrderServerModel *)order cart:(NSString *)cart{
    RPDeliveryListViewController *vc = [[RPDeliveryListViewController alloc] initWithNib];
    vc.order = order;
    vc.cart = cart;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (IBAction)payButtonPressed:(id)sender {
    NSMutableArray <NSString *> *products = [[NSMutableArray alloc] init];
    for (RPGroupServerModel *group in self.menu.groups) {
        for (RPItemServerModel *item in group.items) {
            if (item.count) {
                NSString *str = [NSString stringWithFormat:@"%ld-%ld", (long)item.ID, (long)item.count];
                [products addObject:str];
            }
        }
    }
    if (!products.count) {
        return;
    }
    NSString *cart = [products componentsJoinedByString:@";"];
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager] getCartForProducts:cart onSuccess:^(RPOrderServerModel *order, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (order) {
            [strongSelf showConfirmOrderViewControllerForOrder:order cart:cart];
        } else {
            [strongSelf showMessage:error.text];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error.localizedDescription];
    }];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
