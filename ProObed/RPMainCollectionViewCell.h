//
//  RPMainCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 12/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPMainCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@end
