//
//  RPReportTableViewCell.m
//  ProObed
//
//  Created by Ruslan on 1/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPReportTableViewCell.h"
#import "RPBallServerModel.h"
#import "RPTicketListServerModel.h"

@implementation RPReportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

- (void)configureCellWithOperation:(RPOperationServerModel *)operation{
    self.dateLabel.text = operation.date;
    self.transactionLabel.text = [NSString stringWithFormat:@"%.2f руб.", operation.value];
    if (operation.value > 0) {
        self.transactionLabel.textColor = kGreenColor;
    } else {
        self.transactionLabel.textColor = kRedColor;
    }
}

- (void)configureCellWithTicketOperation:(RPTicketOperationServerModel *)ticketOperation{
    self.dateLabel.text = ticketOperation.date;
    self.transactionLabel.text = [NSString stringWithFormat:@"%.2f руб.", ticketOperation.value];
    if (ticketOperation.value > 0) {
        self.transactionLabel.textColor = kGreenColor;
    } else {
        self.transactionLabel.textColor = kRedColor;
    }
}

@end
