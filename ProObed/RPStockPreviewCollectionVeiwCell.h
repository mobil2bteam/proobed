//
//  RPStockPreviewCollectionVeiwCell.h
//  ProObed
//
//  Created by Ruslan on 1/9/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPStockServerModel;

@interface RPStockPreviewCollectionVeiwCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *stockImageView;

@property (weak, nonatomic) IBOutlet UILabel *stockLabel;

- (void)configureCellWithStock:(RPStockServerModel *)stock;

@end
