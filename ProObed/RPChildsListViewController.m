#import "RPChildsListViewController.h"
#import <SHSPhoneComponent/SHSPhoneLibrary.h>
#import "RPCityTableViewCell.h"
#import "RPServerManager.h"
#import "RPLoyaltyFinishAlertViewController.h"
#import "RPLogoutViewController.h"

@interface RPChildsListViewController () <RPLoyaltyRegisterDelegate>
@property (weak, nonatomic) IBOutlet UIView *birthDayView;
@property (weak, nonatomic) IBOutlet UITableView *childsTableView;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *birthDayTextField;
@property (nonatomic, strong) NSMutableArray <NSString *> *childs;
@end

@implementation RPChildsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.childs = [[NSMutableArray alloc] init];
    [self.birthDayTextField.formatter setDefaultOutputPattern:@"##.##.####"];
    [self.childsTableView registerNib:[UINib nibWithNibName:@"RPCityTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPCityTableViewCell"];
}

- (IBAction)addButtonPressed:(id)sender {
    if (self.birthDayTextField.text.length != 10) {
        [self.birthDayView shake];
        return;
    }
    NSArray <NSString *> *values = [self.birthDayTextField.text componentsSeparatedByString:@"."];
    NSInteger day = values[0].integerValue;
    NSInteger month = values[1].integerValue;
    NSInteger year = values[2].integerValue;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    if (day > 31 || month > 12 || year > currentYear || currentYear - year > 100) {
        [self.birthDayView shake];
        return;
    }
    [self.childs addObject:self.birthDayTextField.text];
    [self.childsTableView reloadData];
    self.birthDayTextField.text = @"";
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    NSString *dates = [self.childs componentsJoinedByString:@";"];
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    NSMutableDictionary *params = [@{@"token":self.token,
                             @"birthday":self.birthday,
                             @"phone": self.phone,
                             @"email": self.email,
                             @"name": self.name,
                             @"marital": self.marital,
                             @"gender": self.gender,
                             @"surname": self.surname,
                             @"lastName": self.lastName} mutableCopy];
    if (dates.length) {
        params[@"dates"] = dates;
        params[@"children"] = @"1";
    } else {
        params[@"children"] = @"0";
    }
    [[RPServerManager sharedManager] setLoyalty:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (user) {
            [self showSuccessAlert];
        } else {
            [strongSelf showMessage:error.text];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error.localizedDescription];
    }];
}
- (void)showSuccessAlert {
    RPLoyaltyFinishAlertViewController *vc = [[RPLoyaltyFinishAlertViewController alloc] initWithNib];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.childs.count;
}

- (RPCityTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPCityTableViewCell class]) forIndexPath:indexPath];
    cell.cityLabel.text = self.childs[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPLogoutViewController *vc = [[RPLogoutViewController alloc] initWithNib];
    vc.text = @"Вы уверены, что хотите удалить этот день рождения?";
    vc.compilationBlock = ^{
        [self.childs removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    };
    [self RP_presentViewControllerCrossDisolve:vc animated:YES];
}

- (void)didFinish{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
