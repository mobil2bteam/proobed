
#import "RPParentProfileVC.h"
#import "RPSignUpStep6VC.h"
#import "RPTextField.h"
#import "RPServerManager.h"
#import "RPSignUpStep3VC.h"
#import "RPEditUserInfoVC.h"
#import "RPModalTransitionDelegate.h"
#import <MessageUI/MessageUI.h>

@interface RPParentProfileVC () <MFMailComposeViewControllerDelegate>

@property (nonatomic) RPModalTransitionDelegate* transitionDelegate;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *secondParentView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end

@implementation RPParentProfileVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureViews];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:true completion:nil];
}

- (NSString *)mailText {
    NSMutableString *temp = [[NSMutableString alloc] initWithString:@"Ваше сообщение:"];
    [temp appendString:@"\n"];
    NSString *name = kAppDelegate.currentUser.info.surname;
    NSString *name2 = kAppDelegate.currentUser.info.name;
    NSString *name3 = kAppDelegate.currentUser.info.patronymic;
    
    NSString *user = [NSString stringWithFormat:@"Ф.И.О.: %@ %@ %@\n", name, name2, name3];
    [temp appendString:user];
    
    [temp appendString:@"Информация о детях:"];
    [temp appendString:@"\n"];
    
    for (RPUserServerModel *child in kAppDelegate.currentUser.info.childs) {
        NSString *name = child.info.surname;
        NSString *name2 = child.info.name;
        NSString *name3 = child.info.patronymic;
        NSString *number = child.info.number;
        NSString *user = [NSString stringWithFormat:@"Ф.И.О.: %@ %@ %@; Лицевой счет: %@", name, name2, name3, number];
        [temp appendString:user];
        [temp appendString:@"\n"];
    }
    [temp appendString:@"Техническая информация:\n"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [temp appendString:[NSString stringWithFormat:@"Версия приложения: %@\n", version]];
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
    [temp appendString:[NSString stringWithFormat:@"Версия ОС: %@\n", systemVersion]];
    [temp appendString:@"ОС: iOS"];
    return [NSString stringWithString:temp];
}

- (void)configureViews {
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@ %@",RPCurrentUser.info.surname, RPCurrentUser.info.name, RPCurrentUser.info.patronymic];
    self.secondParentView.hidden = !kAppDelegate.currentUser.info.parent;
    if (kAppDelegate.currentUser.info.parent) {
        self.phoneLabel.text = kAppDelegate.currentUser.info.parent.phone;
        if (kAppDelegate.currentUser.info.parent.active) {
            self.statusLabel.textColor = [UIColor whiteColor];
            self.statusLabel.text = [kAppDelegate.currentUser.info.parent formattedName];
        } else {
            self.statusLabel.text = @"Не подтвержден!";
            self.statusLabel.textColor = kRedColor;
        }
    }
}

#pragma mark - Actions

- (IBAction)mailButtonPressed:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        [mailCont setSubject:@""];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"esup@npr.procervic.ru"]];
        [mailCont setMessageBody:[self mailText] isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editUserInfoButtonPresssed:(id)sender {
    RPEditUserInfoVC *editUserInfoVC = [[RPEditUserInfoVC alloc]initWithNib];
    self.transitionDelegate.modalAppearanceDirection = RPModalAppearanceDirectionFromLeft;
    [self RP_customPresentViewController:editUserInfoVC animated:YES delegate:self.transitionDelegate];
}

#pragma mark - UIModalTransitionDelegate

- (RPModalTransitionDelegate* )transitionDelegate {
    if(!_transitionDelegate) {
        _transitionDelegate = [RPModalTransitionDelegate new];
    }
    return _transitionDelegate;
}

@end
