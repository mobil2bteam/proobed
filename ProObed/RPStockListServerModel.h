
#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPStockServerModel;

@interface RPStockListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPStockServerModel *> *now;

@property (nonatomic, strong) NSArray<RPStockServerModel *> *actual;

@property (nonatomic, strong) NSArray<RPStockServerModel *> *past;

+(EKObjectMapping *)objectMapping;

@end


@interface RPStockServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *periodFrom;

@property (nonatomic, copy) NSString *periodTo;

@property (nonatomic, copy) NSString *stock_description;

@property (nonatomic, assign) NSInteger companyId;

@property (nonatomic, assign) NSInteger sort;

@end
