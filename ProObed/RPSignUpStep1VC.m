#import "RPSignUpStep1VC.h"
#import "RPSignUpStep2VC.h"
#import "RPCityTableViewCell.h"
#import "RPCityListServerModel.h"

@interface RPSignUpStep1VC () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;
@end

@implementation RPSignUpStep1VC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return kAppDelegate.cities.cities.count;
}

- (RPCityTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPCityTableViewCell class]) forIndexPath:indexPath];
    cell.cityLabel.text = kAppDelegate.cities.cities[indexPath.row].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPCityServerModel *selectedCity = kAppDelegate.cities.cities[indexPath.row];
    RPSignUpStep2VC *signUpVC = [[RPSignUpStep2VC alloc]initWithNib];
    signUpVC.isDemonstration = self.isDemonstration;
    signUpVC.selectedCity = selectedCity;
    signUpVC.user_type = self.user_type;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

@end
