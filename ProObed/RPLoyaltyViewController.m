#import "RPLoyaltyViewController.h"
#import "RPLoyaltyRegisterViewController.h"

@interface RPLoyaltyViewController ()
@property (weak, nonatomic) IBOutlet UILabel *descrLabel;
@end

@implementation RPLoyaltyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    NSString *html = RPCurrentUser.info.loyalty.text;
    NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                          NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil
                                                                  error:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.descrLabel.attributedText = attr;
        self.descrLabel.textColor = [UIColor whiteColor];
    });
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)acceptButtonPressed:(id)sender {
    RPLoyaltyRegisterViewController *vc = [[RPLoyaltyRegisterViewController alloc] initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
