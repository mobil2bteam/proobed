
static NSInteger const PinLength = 4;

#import "RPVerifyCodeVC.h"
#import "RPSignUpStep7VC.h"
#import "RPServerManager.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"

@interface RPVerifyCodeVC ()

@property (weak, nonatomic) IBOutlet UITextField *codeTextField;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *pinLabelsArray;

@property (strong, nonatomic) NSString *codeString;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

@end

@implementation RPVerifyCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.codeTextField.tintColor = [UIColor clearColor];
    self.codeString = @"";
    [self.codeTextField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) {
        if (self.codeString.length == 0) {
            return NO;
        }
        self.codeString = [self.codeString substringToIndex:[self.codeString length]-1];
        ((UILabel *)self.pinLabelsArray[self.codeString.length]).text = @"";
        return NO;
    }
    if (self.codeString.length == PinLength) {
        return NO;
    }
    ((UILabel *)self.pinLabelsArray[self.codeString.length]).text = string;
    self.codeString = [NSString stringWithFormat:@"%@%@", self.codeString, string];
    return YES;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (self.codeString.length == PinLength) {
        [self continueRegistration];
    } else {
        
    }
}

- (void)finishRegistration{
    RPSignUpStep7VC *signUpStep7VC = [[RPSignUpStep7VC alloc]initWithNib];
    signUpStep7VC.phone = self.phone;
    signUpStep7VC.name = self.name;
    signUpStep7VC.gender = self.gender;
    signUpStep7VC.lastName = self.lastName;
    signUpStep7VC.patronymic = self.patronymic;
    signUpStep7VC.selectedCity = self.selectedCity;
    signUpStep7VC.selectedCompany = self.selectedCompany;
    signUpStep7VC.code = self.codeString;
    signUpStep7VC.user_type = self.user_type;
    if (self.numberID.length) {
        signUpStep7VC.numberID = self.numberID;
    }
    [self.navigationController pushViewController:signUpStep7VC animated:YES];
}

- (void)continueRegistration{
    switch (self.user_type) {
        case UserTypeWorker:
            [self signUpWorker];
            break;
        case UserTypeParent:
            [self signUpParent];
            break;
        case UserTypeChild:
            [self signUpPupil];
            break;
        default:
            break;
    }
}

- (void)signUpWorker{
    NSMutableDictionary *params = [@{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"cityId":@(self.selectedCity.ID),
                                     @"companyId":@(self.selectedCompany.ID),
                                     @"phone":[self phone],
                                     @"code":self.codeString,
                                     @"user_type":@(1)} mutableCopy];
    if (self.numberID){
        params[@"number"] = self.numberID;
        params[@"type"] = @"verify";
    }
    NSLog(@"params - %@", params);
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postPreRegisterWithParams:params onSuccess:^(NSString *userInfo, RPErrorServerModel *error) {
        NSLog(@"user info - %@", userInfo);
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [self finishRegistration];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpParent{
    NSDictionary *params = @{@"name":self.name,
                                     @"surname":self.lastName,
                                     @"patronymic":self.patronymic,
                                     @"companyId":@(1),
                                     @"phone":self.phone,
                                     @"code":self.codeString,
                                     @"user_type":@(2)};
    NSLog(@"params - %@", params);
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postPreRegisterWithParams:params onSuccess:^(NSString *userInfo, RPErrorServerModel *error) {
        NSLog(@"user info - %@", userInfo);
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [self finishRegistration];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)signUpPupil{
    NSDictionary *params = @{@"phone":[self phone],
                             @"code":self.codeString,
                             @"user_type":@(3)};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postVerifyChildWithParams:params onSuccess:^(RPUserServerModel *child, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [self finishRegistration];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

@end
