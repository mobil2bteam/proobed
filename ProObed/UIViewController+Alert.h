//
//  UIViewController+Alert.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const RPWarningMessage = @"Заполните необходимые поля";
static NSString * const RPErrorMessage = @"Произошла ошибка";
static NSString * const RPConnectionErrorMessage = @"Проверьте, пожалуйста, соединение с интернетом";

@interface UIViewController (Alert)

- (void)showMessage:(NSString *)message;

- (void)showMessage:(NSString *)message
    withCompilation:(void (^)(void))compilationBlock;

- (void)showMessage:(NSString *)message title:(NSString *)title
    withCompilation:(void (^)(void))compilationBlock;

- (void)addMBProgressHUD;

- (void)hideMBProgressHUD;

@end
