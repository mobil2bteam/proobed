//
//  RPOptions.h
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPTypeServerModel;
@class RPUrlServerModel;

@interface RPOptionsServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPTypeServerModel *> *types;

@property (nonatomic, strong) NSArray<RPUrlServerModel *> *urls;

@property (nonatomic, copy) NSString *about;

+(EKObjectMapping *)objectMapping;

@end


@interface RPTypeServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger weight;

@end


@interface RPUrlServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *url;

@end

