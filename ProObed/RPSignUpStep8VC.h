
#import <UIKit/UIKit.h>
#import "RPBackgroundVC.h"

@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPSignUpStep8VC : RPBackgroundVC

@property (assign, nonatomic) BOOL askPinCode;

@property (strong, nonatomic) NSString *phone;

@property (strong, nonatomic) RPCityServerModel *selectedCity;

@property (strong, nonatomic) RPCompanyServerModel *selectedCompany;

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *pin;

@property (strong, nonatomic) NSString *lastName;

@property (strong, nonatomic) NSString *patronymic;

@property (strong, nonatomic) NSString *numberID;

@property (strong, nonatomic) NSString *gender;

@property (assign, nonatomic) NSString *code;

@property (nonatomic, assign) UserType user_type;

@end
