//
//  RPSignUpStep5VC.h
//  ProObed
//
//  Created by Ruslan on 12/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPSignUpStep5VC : UIViewController
@property (strong, nonatomic) RPCityServerModel *selectedCity;
@property (strong, nonatomic) RPCompanyServerModel *selectedCompany;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *patronymic;
@property (strong, nonatomic) NSString *gender;
@property (nonatomic, assign) UserType user_type;
@end
