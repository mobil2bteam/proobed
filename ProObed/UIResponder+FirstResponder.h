//
//  UIResponder+FirstResponder.h
//  ProObed
//
//  Created by Ruslan on 11/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIResponder (FirstResponder)

+ (id)currentFirstResponder;

@end
