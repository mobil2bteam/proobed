//
//  RPReportTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 1/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOperationServerModel;
@class RPTicketOperationServerModel;

@interface RPReportTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (weak, nonatomic) IBOutlet UILabel *transactionLabel;

- (void)configureCellWithOperation:(RPOperationServerModel *)operation;

- (void)configureCellWithTicketOperation:(RPTicketOperationServerModel *)ticketOperation;

@end
