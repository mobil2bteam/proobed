//
//  RPChildVC.h
//  ProObed
//
//  Created by Ruslan on 19.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPUserServerModel;
@class RPMenuListServerModel;

@interface RPChildVC : UIViewController
@property (nonatomic, strong) RPUserServerModel *child;
@property (nonatomic, assign) BOOL isChildMode;
@property (strong, nonatomic) RPMenuListServerModel *menuList;
@end
