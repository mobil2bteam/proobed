//
//  RPSignUpStep2VC.h
//  ProObed
//
//  Created by Ruslan on 12/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPCityServerModel;

@interface RPSignUpStep2VC : UIViewController
@property (assign, nonatomic) BOOL isDemonstration;
@property (strong, nonatomic) RPCityServerModel *selectedCity;
@property (nonatomic, assign) UserType user_type;
@end
