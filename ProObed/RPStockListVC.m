#import "RPStockListVC.h"
#import "RPStockPreviewCollectionVeiwCell.h"
#import "UICollectionView+Extensions.h"
#import "RPServerManager.h"

@interface RPStockListVC ()

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation RPStockListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // register nib
    [self.stocksCollectionView registerNib:[RPStockPreviewCollectionVeiwCell class]];
    
    // add refresh control to update menu
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.stocksCollectionView addSubview:refreshControl];
}

- (void)handleRefresh : (id)sender
{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]getStocksWithParams:self.params cache:NO onSuccess:^(RPStockListServerModel *stocks, RPErrorServerModel *error) {
        [self.refreshControl endRefreshing];
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            self.stocks = stocks;
            [self.stocksCollectionView reloadData];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)reloadStocks{
  
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
