//
//  RPCities.m
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCityListServerModel.h"

@implementation RPCityListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPCityServerModel class] forKeyPath:@"cities"];
    }];
}

@end

@implementation RPCityServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end
