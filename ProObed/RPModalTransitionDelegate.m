//
//  RPModalTransitionDelegate.m
//  ProObed
//
//  Created by Ruslan on 1/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPModalTransitionAnimator.h"

@implementation RPModalTransitionDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    RPModalTransitionAnimator *animator = [RPModalTransitionAnimator new];
    animator.modalAppearanceDirection = self.modalAppearanceDirection;
    return animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    RPModalTransitionAnimator *animator = [RPModalTransitionAnimator new];
    animator.modalAppearanceDirection = self.modalAppearanceDirection;
    return animator;
}

@end
