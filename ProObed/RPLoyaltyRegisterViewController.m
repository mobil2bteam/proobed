#import "RPLoyaltyRegisterViewController.h"
#import "RPTextField.h"
#import "RPServerManager.h"
#import "RPLoyaltyFinishAlertViewController.h"
#import <SHSPhoneComponent/SHSPhoneLibrary.h>
#import "RPChildsListViewController.h"

@interface RPLoyaltyRegisterViewController () <RPTextFieldProtocol, RPLoyaltyRegisterDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *statusSegmentControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *childSegmentControl;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *patronymicTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *lastNameView;
@property (weak, nonatomic) IBOutlet UIView *patronymicView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *birthDayTextField;
@property (weak, nonatomic) IBOutlet UIView *birthDayView;

@end

@implementation RPLoyaltyRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([RPCurrentUser.info.gender.lowercaseString isEqualToString:@"male"]) {
        [self.statusSegmentControl setTitle:@"Женат" forSegmentAtIndex:0];
        [self.statusSegmentControl setTitle:@"Холост" forSegmentAtIndex:1];
    } else {
        [self.statusSegmentControl setTitle:@"Замужем" forSegmentAtIndex:0];
        [self.statusSegmentControl setTitle:@"Не замужем" forSegmentAtIndex:1];
    }
    [self.birthDayTextField.formatter setDefaultOutputPattern:@"##.##.####"];
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
    self.lastNameTextField.text = RPCurrentUser.info.surname;
    self.nameTextField.text = RPCurrentUser.info.name;
    self.patronymicTextField.text = RPCurrentUser.info.patronymic;
    if (RPCurrentUser.info.phone.length == 10) {
        NSString *phone = RPCurrentUser.info.phone;
        self.phoneTextField1.text = [phone substringToIndex:3];
        self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
        self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
        self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
    }
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (!self.lastNameTextField.text.length) {
        [self.lastNameView shake];
        return;
    }
    if (!self.nameTextField.text.length) {
        [self.nameView shake];
        return;
    }
    if (!self.patronymicTextField.text.length) {
        [self.patronymicView shake];
        return;
    }
    if (!self.emailTextField.text.length) {
        [self.scrollView scrollsToTop];
        [self.emailView shake];
        return;
    }
    if ([self phone].length != 10) {
        [self.phoneView shake];
        return;
    }
    if (self.birthDayTextField.text.length != 10) {
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
        [self.birthDayView shake];
        return;
    }
    NSArray <NSString *> *values = [self.birthDayTextField.text componentsSeparatedByString:@"."];
    NSInteger day = values[0].integerValue;
    NSInteger month = values[1].integerValue;
    NSInteger year = values[2].integerValue;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    if (day > 31 || month > 12 || year > currentYear || currentYear - year > 100) {
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
        [self.birthDayView shake];
        return;
    }
    [self loyaltyAccept];
}

- (void)loyaltyAccept {
    NSString *marital = [self.statusSegmentControl titleForSegmentAtIndex:self.statusSegmentControl.selectedSegmentIndex];
    if (self.childSegmentControl.selectedSegmentIndex == 0) {
        RPChildsListViewController *vc = [[RPChildsListViewController alloc] initWithNib];
        vc.token = RPCurrentUser.info.accessToken;
        vc.gender = RPCurrentUser.info.gender;
        vc.birthday = self.birthDayTextField.text;
        vc.phone = [self phone];
        vc.email = self.emailTextField.text;
        vc.name = self.nameTextField.text;
        vc.surname = self.lastNameTextField.text;
        vc.lastName = self.patronymicTextField.text;
        vc.marital = marital;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    NSDictionary *params = @{@"token":RPCurrentUser.info.accessToken,
                             @"birthday":self.birthDayTextField.text,
                             @"phone": [self phone],
                             @"email": self.emailTextField.text,
                             @"name": self.nameTextField.text,
                             @"marital": marital,
                             @"children": @"0",
                             @"gender": RPCurrentUser.info.gender,
                             @"surname": self.lastNameTextField.text,
                             @"lastName": self.patronymicTextField.text};
    [[RPServerManager sharedManager] setLoyalty:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (user) {
            [self showSuccessAlert];
        } else {
            [strongSelf showMessage:error.text];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error.localizedDescription];
    }];
}

- (void)showSuccessAlert {
    RPLoyaltyFinishAlertViewController *vc = [[RPLoyaltyFinishAlertViewController alloc] initWithNib];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.nameTextField) {
        [self.lastNameTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.lastNameTextField) {
        [self.patronymicTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextFieldDelegate

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    return YES;
}

- (void)didFinish{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
