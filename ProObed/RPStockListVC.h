//
//  RPStocksViewController.h
//  ProObed
//
//  Created by Ruslan on 12/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPStockListServerModel;

@interface RPStockListVC : UIViewController <UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) RPStockListServerModel *stocks;

@property (weak, nonatomic) IBOutlet UICollectionView *stocksCollectionView;

@property (strong, nonatomic) NSDictionary *params;

@end
    
