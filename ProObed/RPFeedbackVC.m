#import "RPFeedbackVC.h"
#import "RPFeedbackListServerModel.h"
#import "RPFeedbackTableViewCell.h"
#import "RPServerManager.h"
#import "RPUserServerModel.h"

@interface RPFeedbackVC () <UITextViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *feedbackTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedbackViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;

@property (assign, nonatomic) NSInteger selectedFeedbackID;

@property (weak, nonatomic) IBOutlet UITextView *feedBackTextView;

@property (weak, nonatomic) IBOutlet UIView *closeView;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation RPFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (kAppDelegate.feedbackList.types.count) {
        self.feedbackLabel.text = kAppDelegate.feedbackList.types[0].name;
        self.selectedFeedbackID = kAppDelegate.feedbackList.types[0].ID;
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud valueForKey:@"saved_email"]) {
        NSString *email = [ud stringForKey:@"saved_email"];
        self.emailTextField.text = email;
    }
    self.emailTextField.tintColor = [UIColor colorWithWhite:85/256 alpha:1];
}

#pragma mark - UITextView

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Текст обращения..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Текст обращения...";
        textView.textColor = [UIColor darkGrayColor];
    }
    [textView resignFirstResponder];
}

#pragma mark - Actions

- (IBAction)choseFeedbackButtonPressed:(id)sender {
    self.feedbackViewHeightConstraint.constant = ((kAppDelegate.feedbackList.types.count + 1) * 50);
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)sendFeedbackButtonPressed:(id)sender {
    if (!self.feedBackTextView.text.length) {
        [self showMessage:@"Введите текст обращения"];
        return;
    }
    NSMutableDictionary *params = [@{@"typeId":@(self.selectedFeedbackID),
                             @"text": self.feedBackTextView.text} mutableCopy];
    if (self.emailTextField.text.length != 0) {
        params[@"email"] = self.emailTextField.text;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setValue:self.emailTextField.text forKey:@"saved_email"];
        [ud synchronize];
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postFeedbackWithParams:params onSuccess:^(BOOL status, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            self.closeView.hidden = NO;
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error - %@", error.localizedDescription);
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return kAppDelegate.feedbackList.types.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPFeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPFeedbackTableViewCell class]) forIndexPath:indexPath];
    cell.feedbackLabel.text = kAppDelegate.feedbackList.types[indexPath.row].name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.feedbackLabel.text = kAppDelegate.feedbackList.types[indexPath.row].name;
    self.selectedFeedbackID = kAppDelegate.feedbackList.types[indexPath.row].ID;
    self.feedbackViewHeightConstraint.constant = 50;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
