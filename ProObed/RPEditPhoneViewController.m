#import "RPEditPhoneViewController.h"
#import "RPTextField.h"
#import "RPServerManager.h"

@interface RPEditPhoneViewController () <RPTextFieldProtocol>
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@end

@implementation RPEditPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
    if (self.childPhone.length == 10) {
        NSString *phone = self.childPhone;
        self.phoneTextField1.text = [phone substringToIndex:3];
        self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
        self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
        self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
    }
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changePressed:(id)sender {
    NSString *phone = [self phone];
    if (phone.length != 10) {
        [self.phoneView shake];
        return;
    }
    [self updateChild];
}

- (void)updateChild{
    NSDictionary *params = @{@"child_id":@(self.childId),
                             @"phone": [self phone],
                             @"token":self.token};
    __weak typeof(self) weakSelf = self;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] updateChild:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (error) {
            [strongSelf showMessage:error.text];
        } else if (user){
            [strongSelf updatePhone];
            [strongSelf dismissViewControllerAnimated:YES completion:^{
                if (strongSelf.compilationBlock) {
                    strongSelf.compilationBlock();
                }
            }];
        } else {
            [strongSelf showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"%@", error.localizedDescription);
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:RPErrorMessage];
    }];
}

- (void)updatePhone{
    for (RPUserServerModel *child in kAppDelegate.currentUser.info.childs) {
        if (child.info.userId == self.childId) {
            child.info.phone = [self phone];
        }
    }
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - UITextFieldDelegate

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    return YES;
}
@end
