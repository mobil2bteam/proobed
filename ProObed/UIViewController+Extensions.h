//
//  UIViewController+Extensions.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extensions)

- (instancetype)initWithNib;

- (void)RP_presentViewController:(UIViewController *)controller animated:(BOOL)animated;

- (void)RP_presentViewControllerCrossDisolve:(UIViewController *)controller animated:(BOOL)animated;

- (void)RP_customPresentViewController:(UIViewController *)controller animated:(BOOL)animated;

- (void)RP_customPresentViewController:(UIViewController *)controller animated:(BOOL)animated delegate:(NSObject<UIViewControllerTransitioningDelegate> *)delegate;

@end
