#import "RPParentMainVC.h"
#import "RPUserServerModel.h"
#import "RPChildSignUp2VC.h"
#import "RPRouter.h"
#import "RPChildTableViewCell.h"
#import "RPChildVC.h"
#import "RPParentProfileVC.h"
#import "RPServerManager.h"

@interface RPParentMainVC ()

@property (weak, nonatomic) IBOutlet UITableView *childTableView;

@end

@implementation RPParentMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[RPServerManager sharedManager] checkUserOnSuccess:^{ }];
    [self.childTableView reloadData];
}

#pragma mark - Actions

- (IBAction)addChildButtonPressed:(id)sender {
    RPChildSignUp2VC *vc = [[RPChildSignUp2VC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)profileButtonPressed:(id)sender {
    RPParentProfileVC *vc = [[RPParentProfileVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return kAppDelegate.currentUser.info.childs.count;
}

- (RPChildTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPChildTableViewCell class]) forIndexPath:indexPath];
    [cell configureForChild:kAppDelegate.currentUser.info.childs[indexPath.row]];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPChildVC *vc = [[RPChildVC alloc] initWithNib];
    vc.child = kAppDelegate.currentUser.info.childs[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
