
#import "RPOrderServerModel.h"

@implementation RPOrderServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPPointServerModel class] forKeyPath:@"points"];
        [mapping hasOne:[RPOrderInfoServerModel class] forKeyPath:@"order"];

    }];
}

@end


@implementation RPOrderInfoServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"totalCart", @"name", @"number", @"balls", @"date", @"createdAt", @"total"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"status.name" toProperty:@"statusTest"];
        [mapping mapKeyPath:@"status.id" toProperty:@"statusIdTest"];
        [mapping hasMany:[RPCartServerModel class] forKeyPath:@"products" forProperty:@"cart"];
        [mapping hasOne:[RPPointServerModel class] forKeyPath:@"point"];
    }];
}

@end

@implementation RPPointServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"address", @"name", @"work"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];

    }];
}

@end

@implementation RPStatusOrderServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        
    }];
}

@end

@implementation RPOrderListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPOrderServerModel class] forKeyPath:@"orders"];
    }];
}

@end


@implementation RPStatusServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"color", @"text_color", @"type"]];
    }];
}

@end

@implementation RPCartServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"count", @"price", @"total"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end
