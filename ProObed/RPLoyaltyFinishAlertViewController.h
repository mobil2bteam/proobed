//
//  RPLoyaltyFinishAlertViewController.h
//  ProObed
//
//  Created by Ruslan on 24.07.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPLoyaltyRegisterViewController.h"

typedef void(^RPSuccessBlock)(void);

@interface RPLoyaltyFinishAlertViewController : UIViewController

@property (nonatomic, assign) id<RPLoyaltyRegisterDelegate> delegate;

@end
