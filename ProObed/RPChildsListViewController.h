//
//  RPChildsListViewController.h
//  ProObed
//
//  Created by Ruslan on 21.08.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPChildsListViewController : UIViewController
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *marital;
@end
