#import "RPMenuListVC.h"
#import "RPMenuListServerModel.h"
#import "RPMenuCollectionViewCell.h"
#import "RPMenuHeaderCollectionViewCell.h"
#import "RPDateCollectionViewCell.h"
#import "UICollectionView+Extensions.h"
#import "RPServerManager.h"

@interface RPMenuListVC ()

@property (weak, nonatomic) IBOutlet UIView *menuView;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation RPMenuListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // register nibs
    [self.dateCollectionView registerNib:[RPDateCollectionViewCell class]];
    [self.menuCollectionView registerNib:[RPMenuCollectionViewCell class]];
    [self.menuCollectionView registerNib:[RPMenuHeaderCollectionViewCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader];
    
    // init start selected date index
    self.selectedDateIndex = 0;

    // set background color from image pattern
    self.menuView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papper"]];
    
    // add refresh control to update menu
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.menuCollectionView addSubview:refreshControl];
}

- (void)handleRefresh : (id)sender
{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] getMenuListWithParams:self.params cache:NO onSuccess:^(RPMenuListServerModel *menuList, RPErrorServerModel *error) {
        [self.refreshControl endRefreshing];
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            self.menuList = menuList;
            [self.menuCollectionView reloadData];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (IBAction)swap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
