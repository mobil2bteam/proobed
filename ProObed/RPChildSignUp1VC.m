#import "RPChildSignUp1VC.h"
#import "RPTextField.h"
#import "RPChildSignUp2VC.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPServerManager.h"

@interface RPChildSignUp1VC () <RPTextFieldProtocol>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *patronymicTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *lastNameView;
@property (weak, nonatomic) IBOutlet UIView *patronymicView;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UISwitch *userDataSwitch;
@end

@implementation RPChildSignUp1VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
    self.lastNameTextField.text = self.child.info.surname;
    self.nameTextField.text = self.child.info.name;
    self.patronymicTextField.text = self.child.info.patronymic;
    if (self.child.info.phone.length == 10) {
        NSString *phone = self.child.info.phone;
        self.phoneTextField1.text = [phone substringToIndex:3];
        self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
        self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
        self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
    }
    if ([self.child.info.gender isEqualToString:@"male"]) {
        self.genderSegmentControl.selectedSegmentIndex = 0;
    } else {
        self.genderSegmentControl.selectedSegmentIndex = 1;
    }
}

#pragma mark - Methods

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (![self.userDataSwitch isOn]) {
        [self showMessage:@"Разрешите использовать ваши персональные данные"];
        return;
    }
    NSString *token = kAppDelegate.currentUser.info.accessToken;
    NSString *gender = @"male";
    if (self.genderSegmentControl.selectedSegmentIndex == 1) {
        gender = @"female";
    }
    NSDictionary *params = @{@"ticket":self.number,
                             @"companyId": @(self.selectedSchool.ID),
                             @"surname": self.child.info.surname,
                             @"gender": gender,
                             @"token": token};
    __weak typeof(self) weakSelf = self;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postRegisterChildWithParams:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (error) {
            [strongSelf showMessage:error.text];
        } else if (user){
            [kAppDelegate.currentUser addChild:user];
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [strongSelf showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        NSLog(@"error - %@", error.localizedDescription);
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:RPErrorMessage];
    }];
}

#pragma mark - UITextFieldDelegate

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.nameTextField) {
        [self.lastNameTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.lastNameTextField) {
        [self.patronymicTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

@end
