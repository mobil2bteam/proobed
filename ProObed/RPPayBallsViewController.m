#import "RPPayBallsViewController.h"
#import "RPBallServerModel.h"
#import "RPQRCodeVC.h"

@interface RPPayBallsViewController ()
@property (weak, nonatomic) IBOutlet UISlider *ballSlider;
@property (weak, nonatomic) IBOutlet UILabel *ballLabel;

@end

@implementation RPPayBallsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSInteger balls = RPCurrentUser.info.balls.value;
    self.ballLabel.text = [NSString stringWithFormat:@"%d/%ld ball", 0, (long)balls];
    self.ballSlider.maximumValue = balls;
}

- (IBAction)payButtonPressed:(id)sender {
    NSInteger currentBalls = self.ballSlider.value;
//    NSString *code = [NSString stringWithFormat:@"%@;%ld", kAppDelegate.currentUser.info.number, (long)currentBalls];
    NSString *code = [NSString stringWithFormat:@"%ld", (long)currentBalls];
    RPQRCodeVC *vc = [[RPQRCodeVC alloc] initWithNib];
    vc.showPayBallsCode = YES;
    vc.ballsCode = code;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sliderValueChanged:(id)sender {
    NSInteger balls = RPCurrentUser.info.balls.value;
    NSInteger currentBalls = self.ballSlider.value;
    self.ballLabel.text = [NSString stringWithFormat:@"%ld/%ld ball", (long)currentBalls, (long)balls];
}

@end
