

#import "RPUserTypeViewController.h"
#import "RPSignInVC.h"
#import "RPSignUpStep1VC.h"
#import "RPSignUpStep3VC.h"
#import "RPSignUpStep6VC.h"

@interface RPUserTypeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@end

@implementation RPUserTypeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    self.statusLabel.text = self.isLogin ? @"Авторизация" : @"Регистрация";
}

#pragma mark - Actions

- (IBAction)workerButtonPressed:(id)sender {
    if (self.isLogin) {
        RPSignInVC *vc = [[RPSignInVC alloc] init];
        vc.userType = UserTypeWorker;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        RPSignUpStep1VC *signUpVC = [[RPSignUpStep1VC alloc]initWithNib];
        signUpVC.user_type = UserTypeWorker;
        [self.navigationController pushViewController:signUpVC animated:YES];
    }
}

- (IBAction)parentButtonPressed:(id)sender {
    if (self.isLogin) {
        RPSignInVC *vc = [[RPSignInVC alloc] init];
        vc.userType = UserTypeParent;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        RPSignUpStep3VC *signUpVC = [[RPSignUpStep3VC alloc]initWithNib];
        signUpVC.user_type = UserTypeParent;
        [self.navigationController pushViewController:signUpVC animated:YES];
    }
}

- (IBAction)pupilButtonPressed:(id)sender {
    if (self.isLogin) {
        RPSignInVC *vc = [[RPSignInVC alloc] init];
        vc.userType = UserTypeChild;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        RPSignUpStep6VC *signUpVC = [[RPSignUpStep6VC alloc]initWithNib];
        signUpVC.user_type = UserTypeChild;
        [self.navigationController pushViewController:signUpVC animated:YES];
    }
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
