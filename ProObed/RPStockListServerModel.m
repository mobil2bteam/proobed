#import "RPStockListServerModel.h"

@implementation RPStockListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPStockServerModel class] forKeyPath:@"now"];
        [mapping hasMany:[RPStockServerModel class] forKeyPath:@"actual"];
        [mapping hasMany:[RPStockServerModel class] forKeyPath:@"past"];
    }];
}

@end

@implementation RPStockServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"image", @"periodFrom", @"periodTo", @"companyId", @"sort"]];
        [mapping mapKeyPath:@"description" toProperty:@"stock_description"];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];}

@end
