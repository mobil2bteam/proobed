//
//  RPSignUpStep1VC.h
//  ProObed
//
//  Created by Ruslan on 12/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBackgroundVC.h"

@interface RPSignUpStep1VC : RPBackgroundVC
// режим демонстрации (только для работников)
@property (assign, nonatomic) BOOL isDemonstration;
// используется для определения типа пользователя, который регитрируется
@property (nonatomic, assign) UserType user_type;
@end
