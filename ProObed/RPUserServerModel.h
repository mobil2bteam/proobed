

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPInfoServerModel;
@class RPParentServerModel;
@class RPOrderInfoServerModel;
@class RPLoyaltyServerModel;
@class RPBallServerModel;
@class RPMenuServerModel;
@class RPTicketServerModel;

@interface RPUserServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, strong) RPInfoServerModel *info;
@property (strong, nonatomic) RPMenuServerModel *menu;

+ (void)logOut;
+(EKObjectMapping *)objectMapping;
- (void)addChild:(RPUserServerModel *)child;
- (void)addSecondParent:(RPUserServerModel *)secondParent;
@end

@interface RPInfoServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *surname;
@property (nonatomic, copy) NSString *patronymic;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *user_description;
@property (nonatomic, assign) NSInteger companyId;
@property (nonatomic, copy) NSString *role;
@property (nonatomic, copy) NSString *guid;
@property (nonatomic, copy) NSString *code1c;
@property (nonatomic, copy) NSString *qr;
@property (nonatomic, copy) NSString *pin;
@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSString *verifyToken;
@property (nonatomic, copy) NSString *authKey;
@property (nonatomic, copy) NSString *card;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) CGFloat balance;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, assign) NSInteger summTicket; // на один день
@property (nonatomic, assign) NSInteger wallet;
@property (nonatomic, assign) UserType user_type; // тип пользователя
@property (nonatomic, strong) NSArray <RPUserServerModel *> *childs;
@property (nonatomic, strong) NSArray <RPOrderInfoServerModel *> *orders;


@property (nonatomic, strong) RPBallServerModel *balls;
@property (nonatomic, strong) NSArray <RPTicketServerModel *> *tickets;
@property (nonatomic, strong) RPLoyaltyServerModel *loyalty;

@property (nonatomic, strong) RPParentServerModel *parent;

- (NSString *)formattedName;
@end

@interface RPParentServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) NSInteger parentId;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *surname;
@property (nonatomic, copy) NSString *patronymic;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *gender;
- (NSString *)formattedName;
@end

@interface RPLoyaltyServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) CGFloat maxBalls;
@property (nonatomic, assign) BOOL isRegister;
@property (nonatomic, copy) NSString *text;
@end

