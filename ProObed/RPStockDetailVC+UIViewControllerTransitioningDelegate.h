//
//  RPStockDetailVC+UIViewControllerTransitioningDelegate.h
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockDetailVC.h"
#import "RMPZoomTransitionAnimator.h"

@interface RPStockDetailVC (UIViewControllerTransitioningDelegate) <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>

@end
