
#import "RPBackgroundVC.h"
#import <Masonry/Masonry.h>

@implementation RPBackgroundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //добавляем фоновое изображение
    UIImageView *bgImageView = [[UIImageView alloc]init];
    bgImageView.translatesAutoresizingMaskIntoConstraints = NO;
    bgImageView.backgroundColor = [UIColor greenColor];
    bgImageView.image = [UIImage imageNamed:@"background"];
    [self.view insertSubview:bgImageView atIndex:0];
    
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(self.view.mas_height);
        make.width.equalTo(self.view.mas_width);
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
    
}

@end
