//
//  RPModalTransitionAnimator.h
//  ProObed
//
//  Created by Ruslan on 1/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPModalTransitionDelegate.h"

@interface RPModalTransitionAnimator : NSObject<UIViewControllerAnimatedTransitioning>

@property (assign, nonatomic) RPModalAppearanceDirection modalAppearanceDirection;

@end
