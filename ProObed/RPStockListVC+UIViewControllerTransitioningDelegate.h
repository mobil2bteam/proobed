//
//  RPStockListVC+UIViewControllerTransitioningDelegate.h
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockListVC.h"
#import "RMPZoomTransitionAnimator.h"

@interface RPStockListVC (UIViewControllerTransitioningDelegate) <RMPZoomTransitionAnimating, UIViewControllerTransitioningDelegate>

@end
