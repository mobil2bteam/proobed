//
//  RPNavControllerDelegate.m
//  ProObed
//
//  Created by Ruslan on 12/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPNavControllerDelegate.h"
#import "RPPushTransition.h"
#import "RPPopTransition.h"

@implementation RPNavControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC
{
    if (operation == UINavigationControllerOperationPush) {
        return [RPPushTransition new];
    }
    if (operation == UINavigationControllerOperationPop) {
        return [RPPopTransition new];
    }
    return [RPPushTransition new];
}

@end
