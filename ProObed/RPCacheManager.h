//
//  RPCacheManager.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPCache.h"

@interface RPCacheManager : NSObject

+ (RPCacheManager*) sharedManager;

/*!
 * @discussion Get cache data
 * @param key - key for cache
 * @return Cache or nil if cache was not found
 */
+ (RPCache *)cachedJSONForKey:(NSString *)key andParameters:(NSDictionary *)parameters;

+ (RPCache *)cachedJSONForKey:(NSString *)key;

/*!
 * @discussion Update cache, if some cache is has expired than it'll be removed
 */
+ (void)updateCache;

/*!
 * @discussion Add JSON to cache
 */
+ (void)cacheJSON:(NSDictionary *)json withParameters:(NSDictionary *)parameters forKey:(NSString *)key;

/*!
 * @discussion Add JSON to cache
 */
+ (void)cacheJSON:(NSDictionary *)json forKey:(NSString *)key;

/*!
 * @discussion Clear all cache
 */
+ (void)clearCache;

@end
