//
//  RPMenuCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 1/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPItemServerModel;

@interface RPMenuCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (void)configureCellWithItem:(RPItemServerModel *) item;

@end
