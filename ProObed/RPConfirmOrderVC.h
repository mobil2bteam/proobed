//
//  RPConfirmOrderVC.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOrderServerModel;

@interface RPConfirmOrderVC : UIViewController
@property (nonatomic, strong) RPOrderServerModel *order;
@property (nonatomic, copy) NSString *cart;
@end
