
#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPQuestionServerModel;
@class RPOptionServerModel;

@interface RPSurveyServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPQuestionServerModel *> *questions;

@property (nonatomic, assign) NSInteger ID;

+(EKObjectMapping *)objectMapping;

@end


@interface RPQuestionServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, strong) NSArray<RPOptionServerModel *> *options;

@end


@interface RPOptionServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, assign) BOOL isSelected;

@end
