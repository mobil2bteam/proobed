#import "RPMainVC.h"
#import "RPRouter.h"
#import "RPServerManager.h"
#import "RPStockListServerModel.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPUserOfficeVC.h"
#import "RPTransitionNavController.h"
#import "RPStockListVC.h"
#import "RPString+Convert.h"
#import "RPSignUpStep1VC.h"
#import "RPMenuListServerModel.h"
#import "RPMenuListVC.h"
#import "RPUserServerModel.h"
#import "RPFeedbackVC.h"
#import "RPCacheManager.h"
#import "RPModalTransitionDelegate.h"
#import "RPSurveyServerModel.h"
#import "RPSurveyVC.h"
#import "RPProductsViewController.h"
#import "RPMenuListServerModel.h"

@interface RPMainVC ()

@property (nonatomic) RPModalTransitionDelegate *transitionDelegate;
@property (weak, nonatomic) IBOutlet UIView *bottomUserView;
@property (weak, nonatomic) IBOutlet UIView *bottomCompanyView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIView *orderView; // заказ полуфабрикатов
@end

@implementation RPMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    if (kAppDelegate.currentUser.menu == nil || kAppDelegate.currentUser.menu.groups.count == 0) {
        self.orderView.hidden = YES;
    } else {
        self.orderView.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[RPServerManager sharedManager] checkUserOnSuccess:^{ }];
    if (self.isDemonstrateMode) {
        self.cityLabel.text = [NSString stringWithFormat:@"г. %@", self.selectedCity.name];
        self.companyLabel.text = self.selectedCompany.name;
    }
    if (kAppDelegate.currentUser.menu == nil || kAppDelegate.currentUser.menu.groups.count == 0) {
        self.orderView.hidden = YES;
    } else {
        self.orderView.hidden = NO;
    }
}

#pragma mark - UIModalTransitionDelegate

- (RPModalTransitionDelegate* )transitionDelegate {
    if(!_transitionDelegate) {
        _transitionDelegate = [RPModalTransitionDelegate new];
        _transitionDelegate.modalAppearanceDirection = RPModalAppearanceDirectionFromRight;
    }
    return _transitionDelegate;
}

#pragma mark - Methods

- (void)initialize{
    // hide navigationBar
    self.navigationController.navigationBar.hidden = YES;
    
    // if it is demonstration mode, than remove user view and fill infо about company. Else remove company view
    if (self.isDemonstrateMode) {
        [self.bottomUserView removeFromSuperview];
        self.cityLabel.text = [NSString stringWithFormat:@"г. %@", self.selectedCity.name];
        self.companyLabel.text = self.selectedCompany.name;
    } else {
        [self.bottomCompanyView removeFromSuperview];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

- (void)showMenuListVCWithMenuList:(RPMenuListServerModel *)menuList withParams:(NSDictionary *)params{
    RPMenuListVC *menuListVC = [[RPMenuListVC alloc]initWithNib];
    menuListVC.menuList = menuList;
    menuListVC.params = params;
    [self RP_customPresentViewController:menuListVC animated:YES delegate:self.transitionDelegate];
}

- (void)showProductListVCWithMenu:(RPMenuServerModel *)menu{
    RPProductsViewController *vc = [[RPProductsViewController alloc] initWithNib];
    vc.menu = menu;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (IBAction)logInButtonPressed:(id)sender {
    [RPRouter setUpStartController];
}

- (IBAction)orderButtonPressed:(id)sender {
    RPProductsViewController *vc = [[RPProductsViewController alloc] initWithNib];
    vc.menu = kAppDelegate.currentUser.menu;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)menuButtonPressed:(id)sender {
    // set format of date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    
    // get today and date after a week
    NSDate *today = [[NSDate alloc] init];
    NSDate *finishDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay value:6 toDate:today options:0];

    NSMutableDictionary *params = [@{@"dateFrom":[dateFormat stringFromDate:today],
                                     @"dateTo":[dateFormat stringFromDate:finishDate]} mutableCopy];
    if (_isDemonstrateMode) {
        params[@"companyId"] = @(self.selectedCompany.ID);
    } else {
        params[@"companyId"] = @(kAppDelegate.currentUser.info.companyId);
        params[kTokenKey] = kAppDelegate.currentUser.info.accessToken;
    }
    
    RPCache *menuListCache = [RPCacheManager cachedJSONForKey:APIKeyMenu andParameters:params];
    // if we've cached menuList than get it from cache, else load it
    if (menuListCache){
        NSDictionary *menuListData = [menuListCache.json dictionaryValue];
        RPMenuListServerModel *menuList = [EKMapper objectFromExternalRepresentation:menuListData withMapping:[RPMenuListServerModel objectMapping]];
        [self showMenuListVCWithMenuList:menuList withParams:params];
    } else {
        [self addMBProgressHUD];
        [[RPServerManager sharedManager]getMenuListWithParams:params cache:NO onSuccess:^(RPMenuListServerModel *menuList, RPErrorServerModel *error) {
            [self hideMBProgressHUD];
            if (error) {
                [self showMessage:error.text];
            } else {
                [self showMenuListVCWithMenuList:menuList withParams:params];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideMBProgressHUD];
            [self showMessage:RPErrorMessage];
        }];
    }
}

- (IBAction)surveyButtonPressed:(id)sender {
    RPCache *surveyCache = [RPCacheManager cachedJSONForKey:APIKeySurvey];
    // if we've cached menuList than get it from cache, else load it
    if (surveyCache){
        NSDictionary *surveyData = [surveyCache.json dictionaryValue];
        RPSurveyServerModel *survey = [EKMapper objectFromExternalRepresentation:surveyData withMapping:[RPSurveyServerModel objectMapping]];
        RPSurveyVC *surveyVC = [[RPSurveyVC alloc]initWithNib];
        surveyVC.survey = survey;
        [self RP_presentViewController:surveyVC animated:YES];
    } else {
        [self addMBProgressHUD];
        [[RPServerManager sharedManager] getSurveyOnSuccess:^(RPSurveyServerModel *survey, RPErrorServerModel *error) {
            [self hideMBProgressHUD];
            if (error) {
                [self showMessage:error.text];
            } else {
                RPSurveyVC *surveyVC = [[RPSurveyVC alloc]initWithNib];
                surveyVC.survey = survey;
                [self RP_presentViewController:surveyVC animated:YES];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideMBProgressHUD];
            [self showMessage:RPErrorMessage];
        }];
    }
}

- (IBAction)feedbackButtonPressed:(id)sender {
    RPFeedbackVC *feedbackVC = [[RPFeedbackVC alloc]initWithNib];
    [self RP_presentViewController:feedbackVC animated:YES];
}

- (IBAction)changeCompanyButtonPressed:(id)sender {
    RPSignUpStep1VC *signUpVC = [[RPSignUpStep1VC alloc]initWithNib];
    signUpVC.isDemonstration = YES;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

- (IBAction)stocksButtonPressed:(id)sender {
    NSDictionary *params;
    if (_isDemonstrateMode) {
        params = @{@"companyId":@(self.selectedCompany.ID)};
    } else {
        params = @{@"companyId":@(kAppDelegate.currentUser.info.companyId)};
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]getStocksWithParams:params cache:YES onSuccess:^(RPStockListServerModel *stocks, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [self showStockList:stocks withParams:params];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)showStockList:(RPStockListServerModel *)stocks withParams:(NSDictionary *)params{
    RPStockListVC *stockListVC = [[RPStockListVC alloc]initWithNib];
    stockListVC.stocks = stocks;
    stockListVC.params = params;
    [self RP_presentViewController:stockListVC animated:YES];
}

- (IBAction)userOfficeButtonPressed:(id)sender {
    RPUserOfficeVC *userOfficeVC = [[RPUserOfficeVC alloc]initWithNib];
    [self.navigationController pushViewController:userOfficeVC animated:YES];
}

@end
