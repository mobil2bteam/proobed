
#import "RPChildVC.h"
#import "RPUserServerModel.h"
#import "RPServerManager.h"
#import "RPRouter.h"
#import "RPMenuListServerModel.h"
#import "RPMenuCollectionViewCell.h"
#import "RPMenuHeaderCollectionViewCell.h"
#import "RPDateCollectionViewCell.h"
#import "UICollectionView+Extensions.h"
#import "RPTicketListServerModel.h"
#import "RPReportTableViewCell.h"
#import "RPMenuListVC.h"
#import "RPModalTransitionDelegate.h"
#import "RPCompanyListServerModel.h"
#import "RPRemoveChildViewController.h"
#import "RPEditPhoneViewController.h"

@interface RPChildVC ()
@property (weak, nonatomic) IBOutlet UIStackView *editStackView;
@property (nonatomic) RPModalTransitionDelegate *transitionDelegate;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITableView *reportTableView;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (strong, nonatomic) RPTicketListServerModel *childTickets;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation RPChildVC

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [self configureViews];
    if (self.isChildMode) {
        [self.navigationController setNavigationBarHidden:YES];
        self.editStackView.hidden = YES;
    }
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        self.reportTableView.refreshControl = self.refreshControl;
    } else {
        [self.reportTableView addSubview:self.refreshControl];
    }

    [self loadTickets];
}

#pragma mark - Methods

- (void)refreshTable {
    [[RPServerManager sharedManager] checkUserOnSuccess:^{}];
    [self configureViews];
    [self loadTickets];
}

- (void)configureViews {
    self.phoneLabel.text = [NSString stringWithFormat:@"Тел.: %@",self.child.info.phone];
    self.nameLabel.text = [self.child.info formattedName];
    self.numberLabel.text = self.child.info.number;
    self.balanceLabel.text = [NSString stringWithFormat:@"%.2f p.", self.child.info.balance];
//    self.balanceLabel.textColor = self.child.info.balance > 0 ? kGreenColor : kRedColor;
    self.schoolLabel.textColor = kGreenColor;
    for (RPCompanyServerModel *company in kAppDelegate.companies.schools) {
        if (company.ID == self.child.info.companyId) {
            for (RPCityServerModel *city in kAppDelegate.cities.cities) {
                if (city.ID == company.cityId) {
                    self.schoolLabel.text = [NSString stringWithFormat:@"%@, %@", city.name, company.name];
                    return;
                }
            }
        }
    }
}

- (void)loadTickets{
    if (kAppDelegate.currentUser.info.user_type == UserTypeParent) {
        NSDictionary *params = @{@"token": kAppDelegate.currentUser.info.accessToken,
                                 @"child_id": @(self.child.info.userId)};
        __weak typeof(self) weakSelf = self;
        [[RPServerManager sharedManager] getTicketsWithParams:params forParent:YES onSuccess:^(RPTicketListServerModel *tickets, RPErrorServerModel *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.refreshControl endRefreshing];
            if (tickets) {
                strongSelf.childTickets = tickets;
                [strongSelf.reportTableView reloadData];
            }
            if (error) {
                [strongSelf showMessage:error.text];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self.refreshControl endRefreshing];
            [self showMessage:error.localizedDescription];
        }];
    }
    if (kAppDelegate.currentUser.info.user_type == UserTypeChild) {
        NSDictionary *params = @{@"token": kAppDelegate.currentUser.info.accessToken,
                                 @"child_id": @(self.child.info.userId)};
        __weak typeof(self) weakSelf = self;
        [[RPServerManager sharedManager] getTicketsWithParams:params forParent:NO onSuccess:^(RPTicketListServerModel *tickets, RPErrorServerModel *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.refreshControl endRefreshing];
            if (tickets) {
                strongSelf.childTickets = tickets;
                [strongSelf.reportTableView reloadData];
            }
            if (error) {
                [strongSelf showMessage:error.text];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self.refreshControl endRefreshing];
            [self showMessage:error.localizedDescription];
        }];
    }
}

- (void)loadMenu{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    
    NSDate *today = [[NSDate alloc] init];
   // NSDate *finishDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay value:6 toDate:today options:0];
    
    NSDictionary *params = @{@"dateFrom":[dateFormat stringFromDate:today],
                             @"companyId": @(self.child.info.companyId)
                             //@"dateTo":[dateFormat stringFromDate:finishDate]
                             };
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] getMenuListWithParams:params cache:NO onSuccess:^(RPMenuListServerModel *menuList, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            self.menuList = menuList;
            [self showMenuViewControllerFor:menuList params:params];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)showMenuViewControllerFor:(RPMenuListServerModel *)menuList params:(NSDictionary *)params {
    RPMenuListVC *menuListVC = [[RPMenuListVC alloc]initWithNib];
    menuListVC.menuList = menuList;
    menuListVC.params = params;
    [self RP_customPresentViewController:menuListVC animated:YES delegate:self.transitionDelegate];
}

#pragma mark - UIModalTransitionDelegate

- (RPModalTransitionDelegate* )transitionDelegate {
    if(!_transitionDelegate) {
        _transitionDelegate = [RPModalTransitionDelegate new];
        _transitionDelegate.modalAppearanceDirection = RPModalAppearanceDirectionFromRight;
    }
    return _transitionDelegate;
}

- (IBAction)removeChild:(id)sender {
    RPRemoveChildViewController *vc = [[RPRemoveChildViewController alloc] initWithNib];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.compilationBlock = ^{
        [self removeChild];
    };
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)editPhone:(id)sender {
    RPEditPhoneViewController *vc = [[RPEditPhoneViewController alloc] initWithNib];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.token = kAppDelegate.currentUser.info.accessToken;
    vc.childId = self.child.info.userId;
    vc.childPhone = self.child.info.phone;
    vc.compilationBlock = ^() {
        [self configureViews];
    };
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)removeChild {
    NSString *token = kAppDelegate.currentUser.info.accessToken;
    NSDictionary *params = @{@"child_id":@(self.child.info.userId),
                             @"token":token};

    __weak typeof(self) weakSelf = self;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] removeChild:params onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (error) {
            [strongSelf showMessage:error.text];
        } else if (user){
            kAppDelegate.currentUser = user;
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [strongSelf showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:RPErrorMessage];
    }];
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    if (self.isChildMode) {
        [RPUserServerModel logOut];
        [RPRouter setUpStartController];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (self.childTickets.tickets.count) {
        return self.childTickets.tickets[0].operations.count;
    }
    return 0;
}

- (RPReportTableViewCell *)tableView:(UITableView *)tableView
               cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPReportTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithTicketOperation:self.childTickets.tickets[0].operations[indexPath.row]];
    cell.dateLabel.textColor = [UIColor whiteColor];
    cell.transactionLabel.textColor = [UIColor whiteColor];
    cell.separatorView.hidden = YES;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

@end
