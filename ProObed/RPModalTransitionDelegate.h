//
//  RPModalTransitionDelegate.h
//  ProObed
//
//  Created by Ruslan on 1/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, RPModalAppearanceDirection) {
    RPModalAppearanceDirectionFromTop,
    RPModalAppearanceDirectionFromBottom,
    RPModalAppearanceDirectionFromRight,
    RPModalAppearanceDirectionFromLeft
};

@interface RPModalTransitionDelegate : NSObject<UIViewControllerTransitioningDelegate>

@property (assign, nonatomic) RPModalAppearanceDirection modalAppearanceDirection;

@end
