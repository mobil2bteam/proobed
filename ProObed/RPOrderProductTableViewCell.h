//
//  RPOrderProductTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 25.04.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
