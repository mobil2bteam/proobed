
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;

@class RPCityListServerModel;
@class RPCompanyListServerModel;
@class RPUserServerModel;
@class RPOptionsServerModel;
@class RPFeedbackListServerModel;
@class RPBallServerModel;
@class RPTicketListServerModel;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *fcmToken;

/*!
 * @brief array with cities that we need for registration and etc.
 */
@property (strong, nonatomic) RPCityListServerModel *cities;

/*!
 * @brief array with companies that we need for registration and etc.
 */
@property (strong, nonatomic) RPCompanyListServerModel *companies;

/*!
 * @brief options with urls and etc.
 */
@property (strong, nonatomic) RPOptionsServerModel *options;

/*!
 * @brief array with feedback's types
 */
@property (strong, nonatomic) RPFeedbackListServerModel *feedbackList;

/*!
 * @brief info about current user
 */
@property (strong, nonatomic) RPUserServerModel *currentUser;

/*!
 * @brief all balls for current user
 */
@property (strong, nonatomic) RPBallServerModel *userBalls;

/*!
 * @brief all tickets for current user
 */
@property (strong, nonatomic) RPTicketListServerModel *userTickets;

- (void)setCurrentUser:(RPUserServerModel *)currentUser;

@property (strong, nonatomic) NSDictionary *userData;

@end

