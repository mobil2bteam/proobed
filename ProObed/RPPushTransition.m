//
//  RPPushTransition.m
//  ProObed
//
//  Created by Ruslan on 12/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPushTransition.h"

@implementation RPPushTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return .25f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    // Get the two view controllers
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    // Get the container view - where the animation has to happen
    UIView *containerView = [transitionContext containerView];
    
    // Add the two VC views to the container. Hide the to
    [containerView addSubview:fromVC.view];
    [containerView addSubview:toVC.view];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    

    [fromVC.view layoutIfNeeded];
    
    CGRect fromFrame = fromVC.view.frame;
    CGRect toFrame = toVC.view.frame;
    
    toFrame.size.width = width;
    toFrame.size.height = height;
    toVC.view.frame = toFrame;
    [toVC.view layoutIfNeeded];
    

    fromFrame.origin.x = 0;
    toFrame.origin.x = width;
    fromVC.view.frame = fromFrame;
    toVC.view.frame = toFrame;
    // Perform the animation
    toFrame.origin.x = 0;
    fromFrame.origin.x = -width;
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0
                        options:0
                     animations:^{
                         toVC.view.frame = toFrame;
                         fromVC.view.frame = fromFrame;
                     }
                     completion:^(BOOL finished) {
                         // Let's get rid of the old VC view
                         [fromVC.view removeFromSuperview];
                         // And then we need to tell the context that we're done
                         [transitionContext completeTransition:YES];
                     }];
    
}

@end
