//
//  NSString+ToDictionary.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Convert)

- (NSDictionary *)dictionaryValue;

@end
