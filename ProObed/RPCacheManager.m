//
//  RPCacheManager.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCacheManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPCache+CoreDataProperties.h"
#import "RPString+Convert.h"
#import "RPDictionary+Convert.h"

@implementation RPCacheManager

+ (RPCacheManager*) sharedManager {
    static RPCacheManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPCacheManager alloc] init];
    });
    return manager;
}

+ (RPCache *)cachedJSONForKey:(NSString *)key{
   return [self cachedJSONForKey:key andParameters:nil];
}

+ (RPCache *)cachedJSONForKey:(NSString *)key andParameters:(NSDictionary *)parameters{
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        if ([cache.key isEqualToString:key]) {
            if (parameters) {
                if ([cache.params isEqualToString:[parameters stringValue]]) {
                    return cache;
                }
                continue;
            }
            return cache;
        }
    }
    return nil;
}

+ (void)updateCache{
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        // если кэш устарел, то удаляем его
        NSDate *cacheDate = [cache.date dateByAddingTimeInterval:[cache.seconds doubleValue]];
        if ([cacheDate compare:[NSDate date]] == NSOrderedAscending) {
            [cache MR_deleteEntity];
        }
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)clearCache{
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        [cache MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)cacheJSON:(NSDictionary *)json forKey:(NSString *)key{
    [self cacheJSON:json withParameters:nil forKey:key];
}

+ (void)cacheJSON:(NSDictionary *)json withParameters:(NSDictionary *)parameters forKey:(NSString *)key{
    RPCache *cashe = [RPCache MR_createEntity];
    cashe.date = [NSDate date];
    cashe.key = key;
    cashe.json = [json stringValue];
    cashe.seconds = @([json[@"cache"] integerValue]);
    if (parameters) {
        cashe.params = [parameters stringValue];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
    }];
}

@end
