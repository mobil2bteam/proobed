#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
#import "RPOptionsServerModel.h"

@class RPMenuServerModel;
@class RPItemServerModel;
@class RPTypeServerModel;
@class RPGroupServerModel;


@interface RPMenuListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPMenuServerModel *> *menu;

+(EKObjectMapping *)objectMapping;

@end


@interface RPMenuServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPGroupServerModel *> *groups;

@property (nonatomic, copy) NSString *date;

@end


@interface RPGroupServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, strong) NSArray<RPItemServerModel *> *items;

@end


@interface RPItemServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *itemDescription;

@property (nonatomic, assign) NSInteger weight;

@property (nonatomic, assign) NSInteger typeWeight;

@property (nonatomic, strong) NSString *image;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *heh;

@property (nonatomic, strong) NSArray<RPTypeServerModel *> *types;

@property (nonatomic, assign) NSInteger count;

@end
