//
//  RPMenuVC.h
//  ProObed
//
//  Created by Ruslan on 12/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBackgroundVC.h"

@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPMainVC : RPBackgroundVC

@property (assign, nonatomic) BOOL isDemonstrateMode;

@property (strong, nonatomic) RPCityServerModel *selectedCity;

@property (strong, nonatomic) RPCompanyServerModel *selectedCompany;

@end
