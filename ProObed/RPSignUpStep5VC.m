
#import "RPSignUpStep5VC.h"
#import "RPSignUpStep6VC.h"
#import "RPServerManager.h"
#import "RPString+MD5.h"
#import "RPCompanyListServerModel.h"

@interface RPSignUpStep5VC ()
@property (weak, nonatomic) IBOutlet UITextField *numberIDTextField;
@property (weak, nonatomic) IBOutlet UIView *numberIDView;
@end

@implementation RPSignUpStep5VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (self.numberIDTextField.text.length) {
        [self continueRegistration];
    } else {
        [self.numberIDView shake];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Methods

- (void)continueRegistration{
    RPSignUpStep6VC *signUpVC = [[RPSignUpStep6VC alloc]initWithNib];
    signUpVC.user_type = self.user_type;
    signUpVC.selectedCity = self.selectedCity;
    signUpVC.selectedCompany = self.selectedCompany;
    signUpVC.name = self.name;
    signUpVC.lastName = self.lastName;
    signUpVC.patronymic = self.patronymic;
    signUpVC.gender = self.gender;
    signUpVC.numberID = self.numberIDTextField.text;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

@end
