//
//  RPOrderTableViewCell.m
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPOrderTableViewCell.h"
#import "RPOrderServerModel.h"

@implementation RPOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellForOrder:(RPOrderInfoServerModel *)order{
    self.orderNumberLabel.text = [NSString stringWithFormat:@"Заказ №%@", order.number];
    self.dateLabel.text = order.date;
    self.totalLabel.text = [NSString stringWithFormat:@"%ld р.", (long)order.total];
    self.statusLabel.text = order.statusTest;
    self.pointLabel.text = order.point.name;
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.orderNumberLabel.text = @"";
    self.dateLabel.text = @"";
    self.totalLabel.text = @"";
}

@end
