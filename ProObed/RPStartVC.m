
#import "RPStartVC.h"
#import "RPSignUpStep1VC.h"
#import "RPUserTypeViewController.h"

@implementation RPStartVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Actions

- (IBAction)logInButtonPressed:(id)sender {
    RPUserTypeViewController *vc = [[RPUserTypeViewController alloc] initWithNib];
    vc.isLogin = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)signUpButtonPressed:(id)sender {
    RPUserTypeViewController *vc = [[RPUserTypeViewController alloc] initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)demonstrationButtonPressed:(id)sender {
    RPSignUpStep1VC *signUpVC = [[RPSignUpStep1VC alloc]initWithNib];
    signUpVC.isDemonstration = YES;
    signUpVC.user_type = UserTypeWorker;
    [self.navigationController pushViewController:signUpVC animated:YES];
}

@end
