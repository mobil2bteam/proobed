//
//  RPCities.h
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPCityServerModel;

@interface RPCityListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPCityServerModel *> *cities;

+(EKObjectMapping *)objectMapping;

@end


@interface RPCityServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger ID;

@end
