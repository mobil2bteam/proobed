#import "RPEditUserInfoVC.h"
#import "RPRouter.h"
#import "RPUserServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPCityListServerModel.h"
#import "RPChangePinVC.h"
#import "RPLogoutViewController.h"
#import "RPAboutProjectVC.h"

@interface RPEditUserInfoVC ()
@property (weak, nonatomic) IBOutlet UIView *paperView;
@property (weak, nonatomic) IBOutlet UILabel *numberIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UISwitch *askPinCodeSwitch;
@property (weak, nonatomic) IBOutlet UIStackView *cityView;
@property (weak, nonatomic) IBOutlet UIStackView *companyView;
@property (weak, nonatomic) IBOutlet UIStackView *numberView;
@end

@implementation RPEditUserInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (IBAction)swap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Methods

- (void)initialize{
    if (kAppDelegate.currentUser.info.user_type != UserTypeWorker) {
        self.numberView.hidden = YES;
        self.cityView.hidden = YES;
        self.companyView.hidden = YES;
    }
    // set backgroundColor from image pattern
    self.paperView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"papper"]];
    
    // fill user data
    self.emailLabel.text = kAppDelegate.currentUser.info.phone;
    for (RPCompanyServerModel *company in kAppDelegate.companies.companies) {
        if (company.ID == kAppDelegate.currentUser.info.companyId) {
            self.companyLabel.text = company.name;
            for (RPCityServerModel *city in kAppDelegate.cities.cities) {
                if (city.ID == company.cityId) {
                    self.cityLabel.text = city.name;
                    break;
                }
            }
            break;
        }
    }
    [self.askPinCodeSwitch setOn:[kUserDefaults boolForKey:kAskPinCodeKey]];
    
    // if user doesn't have numberID remove numberIDView
    if (!kAppDelegate.currentUser.info.number) {
        self.numberView.hidden = YES;
    } else {
        self.numberIDLabel.text = kAppDelegate.currentUser.info.number;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

#pragma mark - Actions

- (IBAction)changePasswordButtonPressed:(id)sender {
    RPChangePinVC *changePinVC = [[RPChangePinVC alloc]init];
    [self RP_presentViewControllerCrossDisolve:changePinVC animated:YES];
}

- (IBAction)aboutButtonPressed:(id)sender {
    RPAboutProjectVC *stockListVC = [[RPAboutProjectVC alloc]initWithNib];
    [self RP_presentViewController:stockListVC animated:YES];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)askPinCodeSwitchChanged:(id)sender {
    [kUserDefaults setBool:self.askPinCodeSwitch.isOn forKey:kAskPinCodeKey];
    [kUserDefaults synchronize];
}

- (IBAction)logOutButtonPressed:(id)sender {
    RPLogoutViewController *vc = [[RPLogoutViewController alloc] initWithNib];
    vc.compilationBlock = ^{
        [RPUserServerModel logOut];
        [RPRouter setUpStartController];
    };
    [self RP_presentViewControllerCrossDisolve:vc animated:YES];
}
@end
