//
//  UICollectionView+Extensions.m
//  ProObed
//
//  Created by Ruslan on 2/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UICollectionView+Extensions.h"

@implementation UICollectionView (Extensions)

- (void)registerNib:(Class)nibClass{
    NSString *nibName = NSStringFromClass(nibClass);
    [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:nibName];
}

- (void)registerNib:(Class)nibClass forSupplementaryViewOfKind:(NSString *)kind{
    NSString *nibName = NSStringFromClass(nibClass);
    [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forSupplementaryViewOfKind:kind withReuseIdentifier:nibName];
}

@end
