#define kUserToken kAppDelegate.currentUser.info.accessToken

//----------------------------- Identifiers for keys-------------------------------------

static NSString * const OptionsID = @"options";
static NSString * const CitiesID = @"cities";
static NSString * const CompaniesID = @"companies";
static NSString * const CacheID = @"cache";
static NSString * const ErrorID = @"error";
static NSString * const TypesID = @"types";
static NSString * const StockID = @"stock";
static NSString * const MeunuListID = @"menu";
static NSString * const SurveyID = @"survey";
static NSString * const UserID = @"user";
static NSString * const BallsID = @"operations";
static NSString * const TicketsID = @"tickets";

#import "RPServerManager.h"
#import "AFHTTPSessionOperation.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "RPString+Convert.h"
#import "UIViewController+Alert.h"
#import "RPCacheManager.h"
#import "RPRouter.h"
#import "RPOptionsServerModel.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPFeedbackListServerModel.h"
#import "RPUserServerModel.h"
#import "RPStockListServerModel.h"
#import "RPBallServerModel.h"
#import "RPMenuListServerModel.h"
#import "RPTicketListServerModel.h"
#import "RPSurveyServerModel.h"
#import "RPOrderServerModel.h"
#import "SAMKeychain.h"

@interface RPServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end

@implementation RPServerManager

+ (RPServerManager*) sharedManager {
    static RPServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPServerManager alloc] init];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kAddressSite]];
        manager.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        manager.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    });
    return manager;
}

- (void)getOptionsOnSuccess:(void(^)()) success
                 onFailure:(void(^)()) failure {
    
    __block RPOptionsServerModel *options = nil;
    __block RPCityListServerModel *cities = nil;
    __block RPCompanyListServerModel *companies = nil;
    __block RPFeedbackListServerModel *feedbackTypes = nil;
    
    RPCache *optionsCache = [RPCacheManager cachedJSONForKey:APIKeyOptions];
    RPCache *citiesCache = [RPCacheManager cachedJSONForKey:APIKeyCities];
    RPCache *companiesCache = [RPCacheManager cachedJSONForKey:APIKeyCompanies];
    RPCache *feedbackTypesCache = [RPCacheManager cachedJSONForKey:APIKeyFeedbackTypes];

    if (optionsCache && citiesCache && companiesCache && feedbackTypesCache) {
        
        NSDictionary *optionsData = [optionsCache.json dictionaryValue];
        options = [EKMapper objectFromExternalRepresentation:optionsData[OptionsID] withMapping:[RPOptionsServerModel objectMapping]];
        
        NSDictionary *citiesData = [citiesCache.json dictionaryValue];
        cities = [EKMapper objectFromExternalRepresentation:citiesData withMapping:[RPCityListServerModel objectMapping]];
        
        NSDictionary *companiesData = [companiesCache.json dictionaryValue];
        companies = [EKMapper objectFromExternalRepresentation:companiesData withMapping:[RPCompanyListServerModel objectMapping]];
        
        NSDictionary *feedbackTypesData = [feedbackTypesCache.json dictionaryValue];
        feedbackTypes = [EKMapper objectFromExternalRepresentation:feedbackTypesData withMapping:[RPFeedbackListServerModel objectMapping]];

        kAppDelegate.cities = cities;
        
        kAppDelegate.companies = companies;
        kAppDelegate.options = options;
        kAppDelegate.feedbackList = feedbackTypes;
        
        [self checkUserOnSuccess:^{
            success();
        }];
        return;
    }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kAddressSite]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        if (options && cities && companies && feedbackTypes) {
            kAppDelegate.cities = cities;
            kAppDelegate.companies = companies;
            kAppDelegate.options = options;
            kAppDelegate.feedbackList = feedbackTypes;
            [self checkUserOnSuccess:^{
                success();
            }];
        } else {
            failure ();
        }
    }];
    
    // load options
    NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:@"" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[OptionsID]) {
            options = [EKMapper objectFromExternalRepresentation:responseObject[OptionsID] withMapping:[RPOptionsServerModel objectMapping]];
            if (responseObject[CacheID]) {
                [RPCacheManager cacheJSON:responseObject forKey:APIKeyOptions];
            }
        }
    } failure:nil];
    [completionOperation addDependency:op1];
    
    // load cities
    NSOperation *op2 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:CitiesID parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[CitiesID]) {
            cities = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPCityListServerModel objectMapping]];
            if (responseObject[CacheID]) {
                [RPCacheManager cacheJSON:responseObject forKey:APIKeyCities];
            }
        }
    } failure:nil];
    [completionOperation addDependency:op2];
    
    // load companies
    NSOperation *op3 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:CompaniesID parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"companies - %@", responseObject);
        if (responseObject[CompaniesID]) {
            companies = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPCompanyListServerModel objectMapping]];
            if (responseObject[CacheID]) {
                [RPCacheManager cacheJSON:responseObject forKey:APIKeyCompanies];
            }
        }
    } failure:nil];
    [completionOperation addDependency:op3];

    // load feedbackTypes
    NSOperation *op4 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:@"feedback/types" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[TypesID]) {
            feedbackTypes = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPFeedbackListServerModel objectMapping]];
            if (responseObject[CacheID]) {
                [RPCacheManager cacheJSON:responseObject forKey:APIKeyFeedbackTypes];
            }
        }
    } failure:nil];
    [completionOperation addDependency:op4];
    
    [queue addOperations:@[op1, op2, op3, op4] waitUntilFinished:false];
    [[NSOperationQueue mainQueue] addOperation:completionOperation];
}

// check if user is valid (if we'he cached [login] and [password]
- (void)checkUserOnSuccess:(void(^)()) success{
    
//    NSString *pin = [SAMKeychain passwordForService:kPinKey account:kBundleID];
//    NSString *login = [SAMKeychain passwordForService:kPhoneKey account:kBundleID];
//    NSString *token = [SAMKeychain passwordForService:kTokenKey account:kBundleID];
    NSString *pin =   [kUserDefaults valueForKey:kPinKey];
    NSString *login = [kUserDefaults valueForKey:kPhoneKey];
    NSString *token = [kUserDefaults valueForKey:kTokenKey];

    if (pin && login && token) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kAddressSite]];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
            
//            if (kAppDelegate.currentUser.info.number) {
//                [self getTicketsAndBalls:^{
//                    success();
//                }];
//            } else {
//                success();
//            }
            success();
        }];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[kLoginKey] = login;
        params[kPinKey] = pin;
        params[kTokenKey] = token;
        
        //log in
        NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"POST" URLString:[self addressForUrl:APIKeyLogin] parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"response - %@", responseObject);
              if (responseObject[UserID]) {
                 RPUserServerModel *user = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPUserServerModel objectMapping]];
                  kAppDelegate.currentUser = user;
              }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            success();
        }];
        [completionOperation addDependency:op1];
        [queue addOperations:@[op1] waitUntilFinished:false];
        [[NSOperationQueue mainQueue] addOperation:completionOperation];
    } else {
        success();
    }
}

- (void)getTicketsAndBalls:(void(^)()) success{
    success();
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kAddressSite]];
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
//        success();
//    }];
//    __block RPBallServerModel *balls = nil;
//    __block RPTicketListServerModel *tickets = nil;
//
//    NSDictionary *params = @{@"token":kAppDelegate.currentUser.info.accessToken};
//    // load balls
//    NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:[self addressForUrl:APIKeyBals] parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        if (responseObject[BallsID]) {
//             balls = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPBallServerModel objectMapping]];
//            kAppDelegate.userBalls = balls;
//        }
//    } failure:nil];
//    [completionOperation addDependency:op1];
//
//    // load tickets
//    NSOperation *op2 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:[self addressForUrl:APIKeyTickets] parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//        if (responseObject[TicketsID]) {
//             tickets = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPTicketListServerModel objectMapping]];
//            kAppDelegate.userTickets = tickets;
//        }
//    } failure:nil];
//    [completionOperation addDependency:op2];
//
//    [queue addOperations:@[op1, op2] waitUntilFinished:false];
//    [[NSOperationQueue mainQueue] addOperation:completionOperation];
}

- (void)postLoginWithParams:(NSDictionary *) params
                         onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    [self.requestOperationManager POST:[self addressForUrl:APIKeyLogin] parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"%@", data);
                                  kAppDelegate.userData = data;
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                      kAppDelegate.currentUser = user;
                                      if (user.info.number) {
                                          [self getTicketsAndBalls:^{
                                              if (user.info.companyId) {
                                                  [self getProductsForCompany:user.info.companyId dateFrom:nil dateTo:nil takeFromCache:YES cacheResponse:YES onSuccess:^(RPMenuServerModel *menu, RPErrorServerModel *error) {
                                                      user.menu = menu;
                                                      success(user, error);
                                                  } onFailure:^(NSError *error, NSInteger statusCode) {
                                                      failure(error, statusCode);
                                                  }];
                                              } else {
                                                  success(user, nil);
                                              }
                                          }];
                                      } else if (user.info.companyId) {
                                          [self getProductsForCompany:user.info.companyId dateFrom:nil dateTo:nil takeFromCache:YES cacheResponse:YES onSuccess:^(RPMenuServerModel *menu, RPErrorServerModel *error) {
                                              user.menu = menu;
                                              success(user, error);
                                          } onFailure:^(NSError *error, NSInteger statusCode) {
                                              failure(error, statusCode);
                                          }];
                                      } else {
                                          success(user, nil);
                                      }
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                      success(nil, error);
                                  }
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRegisterWithParams:(NSDictionary *) params
                     onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSLog(@"%@ - params", params);
    [self.requestOperationManager POST:[self addressForUrl:APIKeyRegistration] parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"%@ - data", data);
                                  NSLog(@"%@ - data", data[@"user"][@"info"]);

                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                      if (user.info.accessToken) {
                                          kAppDelegate.currentUser = user;
                                          if (user.info.number) {
                                              [self getTicketsAndBalls:^{
                                                  success(user, error);
                                              }];
                                          } else {
                                              success(user, nil);
                                          }
                                      }
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRegisterChildWithParams:(NSDictionary *) params
                     onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:APIKeyChildRegistration]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postPreRegisterWithParams:(NSDictionary *) params
                        onSuccess:(void(^)(NSString *userInfo, RPErrorServerModel *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block NSString *userInfo;
    NSLog(@"%@ - params", params);
    [self.requestOperationManager POST:[self addressForUrl:APIKeyRegistration] parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data pre user - %@", data);
                                  NSLog(@"succsess - %@", data[@"success"][@"message"]);
                                  if (data[@"user"][@"info"]) {
                                      userInfo = data[@"user"][@"info"];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(userInfo, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRegisterSecondParentWithParams:(NSDictionary *) params
                                 onSuccess:(void(^)(BOOL success, RPErrorServerModel *error)) success
                                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block BOOL isSuccess = NO;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:APIKeySecondParentRegistration]];
    
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"second parent sign up - %@", data);
                                  if (data[@"success"]) {
                                      isSuccess = data[@"success"][@"status"];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(isSuccess, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postVerifyChildWithParams:(NSDictionary *) params
                        onSuccess:(void(^)(RPUserServerModel *child, RPErrorServerModel *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@?token=%@", [self addressForUrl:APIKeyVerifyChild], kAppDelegate.currentUser.info.accessToken];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"verify child - %@", data);
                                  NSLog(@"user info child - %@", data[@"user"][@"info"]);

                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postFeedbackWithParams:(NSDictionary *) params
                     onSuccess:(void(^)(BOOL status, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block BOOL status;
//    NSString *method = [NSString stringWithFormat:@"%@?token=%@", [self addressForUrl:APIKeyFeedbackCreate], kAppDelegate.currentUser.info.accessToken];

    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:APIKeyFeedbackCreate]];

    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data feedback user - %@", data);
                                  if (data[@"feedback"][@"status"]) {
                                      status = [data[@"feedback"][@"status"] boolValue];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(status, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)getStocksWithParams:(NSDictionary *) params
                      cache:(BOOL)cache
                  onSuccess:(void(^)(RPStockListServerModel *stocks, RPErrorServerModel *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPStockListServerModel *stocks = nil;
    
    [self.requestOperationManager GET:[self addressForUrl:APIKeyStocks] parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"stocks - %@", data);
                                  if (data[StockID]) {
                                      stocks = [EKMapper objectFromExternalRepresentation:data[StockID] withMapping:[RPStockListServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(stocks, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)getMenuListWithParams:(NSDictionary *) params
                        cache:(BOOL)cache
                    onSuccess:(void(^)(RPMenuListServerModel *menuList, RPErrorServerModel *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPMenuListServerModel *menuList = nil;
    NSLog(@"params - %@", params);
    [self.requestOperationManager GET:[self addressForUrl:APIKeyMenu] parameters:params
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                 NSLog(@"data - %@", data);
                                 if (data[MeunuListID]) {
                                     menuList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPMenuListServerModel objectMapping]];
                                 }
                                 if (data[ErrorID]) {
                                     error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                            withMapping:[RPErrorServerModel objectMapping]];
                                 }
                                 if (data[CacheID] && cache) {
                                     [RPCacheManager cacheJSON:data withParameters:params forKey:APIKeyMenu];
                                 }

                                 if (success) {
                                     success(menuList, error);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if (failure)
                                 {
                                     failure(error,error.code);
                                 }
                             }];
}



- (void)getSurveyOnSuccess:(void(^)(RPSurveyServerModel *survey, RPErrorServerModel *error)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPSurveyServerModel *survey = nil;
    [self.requestOperationManager GET:[self addressForUrl:APIKeySurvey] parameters:nil
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                 NSLog(@"menu - %@", data);
                                 if (data[SurveyID]) {
                                     survey = [EKMapper objectFromExternalRepresentation:data withMapping:[RPSurveyServerModel objectMapping]];
                                 }
                                 if (data[ErrorID]) {
                                     error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                            withMapping:[RPErrorServerModel objectMapping]];
                                 }
                                 if (data[CacheID]) {
                                     [RPCacheManager cacheJSON:data forKey:APIKeySurvey];
                                 }
                                 
                                 if (success) {
                                     success(survey, error);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if (failure)
                                 {
                                     failure(error,error.code);
                                 }
                             }];
}

- (void)sendSurveyWithParams:(NSDictionary *) params
                           onSuccess:(void(^)(BOOL isSuccess, RPErrorServerModel *error)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
   
    __block RPErrorServerModel *error = nil;
    __block BOOL isSuccess = NO;

    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:APIKeySendSurvey]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data - %@", data);
                                  if ([data[@"success"][@"status"] boolValue] == YES) {
                                      isSuccess = YES;
                                  }

                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  
                                  if (success) {
                                      success(isSuccess, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRecoverPasswordWithParams:(NSDictionary *) params
                  onSuccess:(void(^)(RPErrorServerModel *error, NSString *info)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block NSString *info;
    [self.requestOperationManager POST:[self addressForUrl:APIKeyRecoverPassword] parameters:params
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                 NSLog(@"recover - %@", data);
                                 if (data[ErrorID]) {
                                     error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                            withMapping:[RPErrorServerModel objectMapping]];
                                 }
                                 if (data[@"user"][@"info"]) {
                                     info = data[@"user"][@"info"];
                                 }
                                 if (success) {
                                     success(error, info);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if (failure)
                                 {
                                     failure(error,error.code);
                                 }
                             }];
}

- (void)getTicketsWithParams:(NSDictionary *) params
                    forParent:(BOOL)isForParent
                           onSuccess:(void(^)(RPTicketListServerModel *tickets, RPErrorServerModel *error)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPTicketListServerModel *tickets = nil;
    
    NSString *key = !isForParent ? APIKeyTickets : APIKeyChildTickets;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:key]];
    [self.requestOperationManager GET:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"tickets response - %@", data);
                                  if (data[TicketsID]) {
                                      tickets = [EKMapper objectFromExternalRepresentation:data withMapping:[RPTicketListServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  
                                  if (success) {
                                      success(tickets, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postChangePinWithParams:(NSDictionary *) params
                      onSuccess:(void(^)(RPErrorServerModel *error, NSString *info, NSString *message)) success
                      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block NSString *info;
    __block NSString *message;
    
    NSString *method = [NSString stringWithFormat:@"user/update-pin?token=%@", kAppDelegate.currentUser.info.accessToken];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"change pin - %@", data);
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (data[@"user"][@"info"]) {
                                      info = data[@"user"][@"info"];
                                  }
                                  if (data[@"message"][@"text"]) {
                                      message = data[@"message"][@"text"];
                                  }
                                  if (success) {
                                      success(error, info, message);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}
- (NSString *)addressForUrl:(NSString *)urlString{
    for (RPUrlServerModel *url in kAppDelegate.options.urls) {
        if ([url.name isEqualToString:urlString]) {
            NSString *address = [url.url substringFromIndex:kAddressSite.length];
            if ([address characterAtIndex:0] == '/') {
                address = [address substringFromIndex:1];
                address = [NSString stringWithFormat:@"%@", address];
                if (kAppDelegate.currentUser.info.accessToken.length > 0) {
                    NSString *token = kAppDelegate.currentUser.info.accessToken;
                    address = [NSString stringWithFormat:@"%@?token=%@", address, token];
                }
            }
            return  address;
        }
    }
    return @"";
}

#pragma mark - Заказ полуфабрикатов

- (void)getProductsForCompany:(NSInteger)companyId
                     dateFrom:(NSString *)dateFrom
                       dateTo:(NSString *)dateTo
                takeFromCache:(BOOL)takeFromCache
                cacheResponse:(BOOL)cacheResponse
                    onSuccess:(void(^)(RPMenuServerModel *menu,
                                       RPErrorServerModel *error)) success
                    onFailure:(FailureBlock) failure{
    // Build parameters
    NSMutableDictionary *params = [@{@"companyId": @(companyId)} mutableCopy];
    if (dateFrom) {
        params[@"dateFrom"] = dateFrom;
    } else {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd.MM.yyyy"];
        params[@"dateFrom"] = [dateFormat stringFromDate:[NSDate date]];
    }
    if (kUserToken.length) {
        params[@"token"] = kUserToken;
    }
    if (dateTo) {
        params[@"dateTo"] = dateTo;
    }
    __block RPErrorServerModel *error = nil;
    __block RPMenuServerModel *menu = nil;
    
    // Check is response is cached
//    if (takeFromCache) {
//        RPCache *cache = [RPCacheManager cachedJSONForKey:APIKeyProducts andParameters:params];
//        if (cache) {
//            NSDictionary *cacheData = [cache.json dictionaryValue];
//            menu = [EKMapper objectFromExternalRepresentation:cacheData withMapping:[RPMenuServerModel objectMapping]];
//            kAppDelegate.currentUser.menu = menu;
//            success(menu, nil);
//            return;
//        }
//    }
    NSString *url = [self addressForUrl:APIKeyProducts];
    [self.requestOperationManager GET:url parameters:params
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                 NSLog(@"%@", data);
                                 if (data[@"menu_products"]) {
                                     menu = [EKMapper objectFromExternalRepresentation:data[@"menu_products"] withMapping:[RPMenuServerModel objectMapping]];
                                     
                                     
                                     NSLog(@"fds");
                                 }
                                 if (data[@"error"]) {
                                     error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                            withMapping:[RPErrorServerModel objectMapping]];
                                 }
                                 if (cacheResponse && data[@"cache"]) {
//                                     [RPCacheManager cacheJSON:data withParameters:params forKey:APIKeyProducts];
                                 }
                                 if (success) {
                                     success(menu, error);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if (failure)
                                 {
                                     failure(error,error.code);
                                 }
                             }];
}

- (void)getCartForProducts:(NSString *)cart
                 onSuccess:(void(^)(RPOrderServerModel *order,
                                    RPErrorServerModel *error)) success
                 onFailure:(FailureBlock) failure{
    NSDictionary *params = @{@"cart": cart,
                             @"token": kUserToken};
    __block RPErrorServerModel *error = nil;
    __block RPOrderServerModel *order = nil;
    NSString *url = [self addressForUrl:APIKeyCart];
    [self.requestOperationManager POST:url parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data){
                                  NSLog(@"%@", data);
                                  if (data[@"order"]) {
                                      order = [EKMapper objectFromExternalRepresentation:data withMapping:[RPOrderServerModel objectMapping]];
                                      
                                  }
                                  if (data[@"error"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(order, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postOrderForProducts:(NSString *)cart
                        date:(NSString *)date
                   receiptId:(NSInteger)receiptId
                   onSuccess:(void(^)(RPOrderInfoServerModel *order,
                                      RPErrorServerModel *error)) success
                   onFailure:(FailureBlock) failure{
    NSDictionary *params = @{@"cart": cart,
                             @"date": date,
                             @"receiptId": @(receiptId),
                             @"token": kUserToken};
    __block RPErrorServerModel *error = nil;
    __block RPOrderInfoServerModel *order = nil;
    NSString *url = [self addressForUrl:APIKeyOrder];
    [self.requestOperationManager POST:url parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data){
                                  if (data[@"order"]) {
                                      order = [EKMapper objectFromExternalRepresentation:data[@"order"] withMapping:[RPOrderInfoServerModel objectMapping]];
                                  }
                                  if (data[@"error"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      NSMutableArray <RPOrderInfoServerModel *> *temp = [kAppDelegate.currentUser.info.orders mutableCopy];
                                      [temp insertObject:order atIndex:0];
                                      kAppDelegate.currentUser.info.orders = [temp copy];
                                      success(order, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)getOrdersOnSuccess:(void(^)(NSArray <RPOrderInfoServerModel *> *orders,
                                    RPErrorServerModel *error)) success
                 onFailure:(FailureBlock) failure{
    NSDictionary *params = @{@"token": kUserToken};
    __block RPErrorServerModel *error = nil;
    __block NSMutableArray <RPOrderInfoServerModel *> *orders = [[NSMutableArray alloc] init];
    NSString *url = [self addressForUrl:APIKeyUserOrders];
    [self.requestOperationManager POST:url parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data){
                                  NSLog(@"orders - %@", data);
                                  if (data[@"orders"]) {
                                      for (NSDictionary *o in data[@"orders"]) {
                                          RPOrderInfoServerModel *order = [EKMapper objectFromExternalRepresentation:o withMapping:[RPOrderInfoServerModel objectMapping]];
                                          [orders addObject:order];
                                      }
                                  }
                                  if (data[@"error"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success([orders copy], error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

#pragma mark - Childs

- (void)removeChild:(NSDictionary *) params
                          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:@"url_child_remove_child"]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)updateChild:(NSDictionary *) params
          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:@"url_child_update"]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)getChildInfo:(NSDictionary *) params
          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:@"url_child_info"]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)setLoyalty:(NSDictionary *) params
         onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPErrorServerModel *error = nil;
    __block RPUserServerModel *user = nil;
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:@"url_loyalty"]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"%@", data);
                                  if (data[UserID]) {
                                      user = [EKMapper objectFromExternalRepresentation:data withMapping:[RPUserServerModel objectMapping]];
                                      kAppDelegate.currentUser = user;
                                  }
                                  if (data[ErrorID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[ErrorID]
                                                                             withMapping:[RPErrorServerModel objectMapping]];
                                  }
                                  if (success) {
                                      success(user, error);
                                  }
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)cancelOrderWithParams:(NSDictionary *) params
                   onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                   onFailure:(void(^)(NSString *error)) failure{
    NSString *method = [NSString stringWithFormat:@"%@", [self addressForUrl:@"url_order_cancel"]];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data - %@", data);
                                  if ([data[@"success"][@"result"] boolValue] == YES) {
                                      success(YES, nil);
                                      return;
                                  }
                                  if (data[ErrorID][@"message"]) {
                                      NSString *error = data[ErrorID][@"message"];
                                      failure(error);
                                      return;
                                  }
                                  failure(@"Error");
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error.localizedDescription);
                                  }
                              }];
}

- (void)sendFCMTokenToServer {
    if (!kAppDelegate.fcmToken.length || kAppDelegate.currentUser == nil) {
        return;
    }
    NSString *method = [NSString stringWithFormat:@"%@",[self addressForUrl:@"url_child_device_registration"]];
    NSDictionary *params = @{@"device_id":kAppDelegate.fcmToken};
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"token info - %@", data);
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  NSLog(@"token error - %@", error.localizedDescription);
                              }];
}
@end
