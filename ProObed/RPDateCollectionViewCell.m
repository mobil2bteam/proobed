//
//  RPDateCollectionViewCell.m
//  ProObed
//
//  Created by Ruslan on 1/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPDateCollectionViewCell.h"

@implementation RPDateCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected{
    if (selected) {
        self.backgroundColor = kRedColor;
        self.dateLabel.textColor = [UIColor whiteColor];
    } else {
        self.backgroundColor = [UIColor colorWithWhite:1.f alpha:0.5];
        self.dateLabel.textColor = kRedColor;
    }
}

- (void)configureCellWithDate:(NSString *)date{
    self.dateLabel.text = date;
}

@end
