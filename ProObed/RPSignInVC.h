//
//  RPSignInViewController.h
//  ProObed
//
//  Created by Ruslan on 11/3/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBackgroundVC.h"

@interface RPSignInVC : RPBackgroundVC
@property (nonatomic, assign) UserType userType;
@end
