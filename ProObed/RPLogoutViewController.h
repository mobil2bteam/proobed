//
//  RPLogoutViewController.h
//  ProObed
//
//  Created by Ruslan on 15.05.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPLogoutViewController : UIViewController
@property (nonatomic, copy) void (^compilationBlock)(void);
@property (nonatomic, copy) NSString *text;

@end
