//
//  RPModalTransitionAnimator.m
//  ProObed
//
//  Created by Ruslan on 1/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPModalTransitionAnimator.h"

@implementation RPModalTransitionAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.2;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController* destination = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    if([destination isBeingPresented]) {
        [self animatePresentation:transitionContext];
    } else {
        [self animateDismissal:transitionContext];
    }
}

//
// Calculate a final frame for presenting controller according to interface orientation
// Presenting controller should always slide down and its top should coincide with the bottom of screen
//
- (CGRect)presentingControllerFrameWithContext:(id<UIViewControllerContextTransitioning>)transitionContext {
    CGRect frame = transitionContext.containerView.bounds;
    
    if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) // iOS 8+
    {
        //
        // On iOS 8, UIKit handles rotation using transform matrix
        // Therefore we should always return a frame for portrait mode
        //
        return CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
    }
    else
    {
        //
        // On iOS 7, UIKit does not handle rotation
        // To make sure our view is moving in the right direction (always down) we should
        // fix the frame accoding to interface orientation.
        //
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        switch (orientation) {
            case UIInterfaceOrientationLandscapeLeft:
                return CGRectMake(CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationLandscapeRight:
                return CGRectMake(-CGRectGetWidth(frame), 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                return CGRectMake(0, -CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
            default:
            case UIInterfaceOrientationPortrait:
                return CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
                break;
        }
    }
}

- (void)animatePresentation:(id<UIViewControllerContextTransitioning>)transitionContext
{
    NSTimeInterval transitionDuration = [self transitionDuration:transitionContext];
    UIViewController* source = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* destination = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* container = transitionContext.containerView;
    
    // Orientation bug fix
    // See: http://stackoverflow.com/a/20061872/351305
    destination.view.frame = container.bounds;
    source.view.frame = container.bounds;
    
    [container addSubview:destination.view];
    
    destination.view.frame = [self fromFrame];
    
    [UIView animateWithDuration:transitionDuration delay:0.0
                        options:0 animations:^{
                            
                            destination.view.frame = container.bounds;
                            
                        } completion:^(BOOL finished) {
                            [transitionContext completeTransition:YES];
                        }];
}


- (void)animateDismissal:(id<UIViewControllerContextTransitioning>)transitionContext
{
    NSTimeInterval transitionDuration = [self transitionDuration:transitionContext];
    UIViewController* source = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* destination = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* container = transitionContext.containerView;

    destination.view.frame = container.bounds;
    source.view.frame = container.bounds;
    
    [UIView animateWithDuration:transitionDuration delay:0.0
                        options:0 animations:^{
                            source.view.frame = [self fromFrame];
                        } completion:^(BOOL finished) {
                            [transitionContext completeTransition:YES];
                        }];
}

#pragma mark - CGRect to animate

- (CGRect)fromFrame{
    CGRect fromFrame = [UIScreen mainScreen].bounds;
    switch (self.modalAppearanceDirection) {
        case RPModalAppearanceDirectionFromTop:
            fromFrame.origin.y = -CGRectGetHeight(fromFrame);
            break;
        case RPModalAppearanceDirectionFromBottom:
            fromFrame.origin.y = CGRectGetHeight(fromFrame);
            break;
        case RPModalAppearanceDirectionFromRight:
            fromFrame.origin.x = CGRectGetWidth(fromFrame);
            break;
        case RPModalAppearanceDirectionFromLeft:
            fromFrame.origin.x = -CGRectGetWidth(fromFrame);
            break;
        default:
            return fromFrame;
    }
    return fromFrame;
}

@end
