#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPTicketOperationServerModel;
@class RPTicketServerModel;


@interface RPTicketListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPTicketServerModel *> *tickets;

+(EKObjectMapping *)objectMapping;

@end

@interface RPTicketServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger userID;

@property (nonatomic, copy) NSString *number;

@property (nonatomic, assign) NSInteger value;

@property (nonatomic, strong) NSArray<RPTicketOperationServerModel *> *operations;

@end


@interface RPTicketOperationServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) CGFloat value;

@property (nonatomic, assign) NSInteger ticketId;

@property (nonatomic, strong) NSString *date;

@end
