//
//  RPError.m
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPErrorServerModel.h"

@implementation RPErrorServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"text",
                                          @"message",
                                          @"code"]];
        
    }];
}

- (NSString *)text {
    if (_text.length > 0) {
        return _text;
    } else {
        return _message;
    }
}
@end
