
static NSInteger const PinLength = 4;

#import "RPSignUpStep7VC.h"
#import "RPSignUpStep8VC.h"
#import "RPServerManager.h"
#import "RPRouter.h"

@interface RPSignUpStep7VC ()

@property (weak, nonatomic) IBOutlet UIView *pinCodeView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *pinLabelsArray;

@property (strong, nonatomic) NSString *codeString;

@property (weak, nonatomic) IBOutlet UISwitch *rememberMeSwitch;

@end

@implementation RPSignUpStep7VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Methods

- (void)initialize{
    self.codeString = @"";
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)numberButtonPressed:(UIButton *)sender {
    if (self.codeString.length == PinLength) {
        return;
    }
    ((UILabel *)self.pinLabelsArray[self.codeString.length]).text = [NSString stringWithFormat:@"%ld", (long)sender.tag];
    self.codeString = [NSString stringWithFormat:@"%@%ld", self.codeString, (long)sender.tag];
}

- (IBAction)removeLastNumberButtonPressed:(id)sender {
    if (self.codeString.length == 0) {
        return;
    }
    self.codeString = [self.codeString substringToIndex:[self.codeString length]-1];
    ((UILabel *)self.pinLabelsArray[self.codeString.length]).text = @"";
}

- (IBAction)nextButtonPressed:(id)sender {
    if (self.codeString.length == PinLength) {
        if (self.user_type == UserTypeChild) {
            [self signUpPupil];
        } else {
            RPSignUpStep8VC *signUpStep8VC = [[RPSignUpStep8VC alloc]initWithNib];
            signUpStep8VC.phone = self.phone;
            signUpStep8VC.name = self.name;
            signUpStep8VC.gender = self.gender;
            signUpStep8VC.lastName = self.lastName;
            signUpStep8VC.patronymic = self.patronymic;
            signUpStep8VC.selectedCity = self.selectedCity;
            signUpStep8VC.selectedCompany = self.selectedCompany;
            signUpStep8VC.pin = self.codeString;
            signUpStep8VC.code = self.code;
            signUpStep8VC.user_type = self.user_type;
            signUpStep8VC.askPinCode = self.rememberMeSwitch.isOn;
            if (self.numberID) {
                signUpStep8VC.numberID = self.numberID;
            }
            [self.navigationController pushViewController:signUpStep8VC animated:YES];
        }
    } else {
        [self.pinCodeView shake];
    }
}

- (void)signUpPupil{
    NSDictionary *params = @{@"phone":[self phone],
                             @"code":self.code,
                             @"pin":self.self.codeString,
                             @"user_type":@(3)};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postVerifyChildWithParams:params onSuccess:^(RPUserServerModel *child, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (child){
            if (self.rememberMeSwitch.isOn == YES) {
                [kUserDefaults setBool:YES forKey:kAskPinCodeKey];
                [kUserDefaults synchronize];
            }
            kAppDelegate.currentUser = child;
            [RPRouter setMainViewController];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

@end
