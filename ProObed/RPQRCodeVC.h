//
//  RPQRCodeVC.h
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPQRCodeVC : UIViewController
@property (nonatomic, assign) BOOL showPayBallsCode;
@property (nonatomic, copy) NSString *ballsCode;
@end
