#import "RPStockDetailVC.h"
#import "RPStockListServerModel.h"
#import "UIImageView+AFNetworking.h"

@interface RPStockDetailVC ()

@property (weak, nonatomic) IBOutlet UILabel *stockNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *stockDescriptionLabel;

@end

@implementation RPStockDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.stockNameLabel.text = self.stock.name;
    NSURL *url = [NSURL URLWithString: self.stock.image];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.stockImageView setImageWithURLRequest:imageRequest
                               placeholderImage:nil
                                        success:nil
                                        failure:nil];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *html = self.stock.stock_description;
            NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                  NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                             documentAttributes:nil
                                                                          error:nil];
            NSString *finalString = [attr string];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.stockDescriptionLabel.text = finalString;
            });
        });
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
