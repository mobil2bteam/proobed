

#import "RPSurveyServerModel.h"

@implementation RPSurveyServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPQuestionServerModel class] forKeyPath:@"survey.questions"
            forProperty:@"questions"];
        [mapping mapKeyPath:@"survey.id" toProperty:@"ID"];
    }];
}

@end

@implementation RPQuestionServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPOptionServerModel class] forKeyPath:@"options"];
        [mapping mapPropertiesFromArray:@[@"text", @"type"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end

@implementation RPOptionServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"text"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end
