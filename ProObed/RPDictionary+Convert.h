//
//  NSDictionary+Convert.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Convert)

- (NSString *)stringValue;

@end
