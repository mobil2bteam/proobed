//
//  RPMenuCollectionViewCell.m
//  ProObed
//
//  Created by Ruslan on 1/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMenuCollectionViewCell.h"
#import "RPMenuListServerModel.h"

@implementation RPMenuCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithItem:(RPItemServerModel *) item{
    self.menuLabel.text = item.name;
    if (item.price) {
        self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)item.price];
    }
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.priceLabel.text = @"";
    self.menuLabel.text = @"";
}

@end
