//
//  RPDeliveryListViewController.h
//  ProObed
//
//  Created by Ruslan on 24.04.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOrderServerModel;

@interface RPDeliveryListViewController : UIViewController
@property (nonatomic, strong) RPOrderServerModel *order;
@property (nonatomic, copy) NSString *cart;
@end
