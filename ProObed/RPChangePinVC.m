#import "RPChangePinVC.h"
#import "RPServerManager.h"
#import "RPUserServerModel.h"
#import "SAMKeychain.h"

@interface RPChangePinVC ()

@property (weak, nonatomic) IBOutlet UITextField *oldPinTextField;

@property (weak, nonatomic) IBOutlet UITextField *pinTextField;

@property (weak, nonatomic) IBOutlet UIView *oldPinCodeView;

@property (weak, nonatomic) IBOutlet UIView *pinCodeView;

@end

@implementation RPChangePinVC


#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeButtonPressed:(id)sender {
    if (self.oldPinTextField.text.length != 4) {
        [self.oldPinCodeView shake];
        return;
    }
    if (self.pinTextField.text.length != 4) {
        [self.pinCodeView shake];
        return;
    }
    NSDictionary *params = @{@"pin":self.oldPinTextField.text,
                             @"newPin":self.pinTextField.text};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postChangePinWithParams:params onSuccess:^(RPErrorServerModel *error, NSString *info, NSString *message) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (message){
            [SAMKeychain setPassword:self.pinTextField.text forService:kPinKey account:kBundleID];
            
            __weak typeof(self) weakSelf = self;
            [self showMessage:message withCompilation:^{
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }];
        } else if (info){
            [SAMKeychain setPassword:self.pinTextField.text forService:kPinKey account:kBundleID];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error - %@", error.localizedDescription);
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    if (textField.text.length == 4) {
        return NO;
    }
    return YES;
}

@end
