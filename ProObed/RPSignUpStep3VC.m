
#import "RPSignUpStep3VC.h"
#import "RPSignUpStep4VC.h"
#import "RPSignUpStep6VC.h"
#import "RPSignUpStep1VC.h"
#import "RPTextField.h"
#import "RPServerManager.h"
#import "RPVerifyCodeVC.h"

@interface RPSignUpStep3VC () <RPTextFieldProtocol>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *patronymicTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *lastNameView;
@property (weak, nonatomic) IBOutlet UIView *patronymicView;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet RPTextField *phoneTextField4;
@property (weak, nonatomic) IBOutlet UISwitch *userDataSwitch;
@property (weak, nonatomic) IBOutlet UIButton *showGenderButton;
@end

@implementation RPSignUpStep3VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.genderSegmentControl.hidden = YES;
    for (RPTextField *textField in @[self.phoneTextField1, self.phoneTextField2, self.phoneTextField3, self.phoneTextField4]) {
        textField.RP_delegate = self;
    }
    if (self.user_type == UserTypeParent) {
        [self.genderSegmentControl setTitle:@"Муж." forSegmentAtIndex:0];
        [self.genderSegmentControl setTitle:@"Жен." forSegmentAtIndex:1];
    }
    if (self.user_type == UserTypeChild) {
        [self.genderSegmentControl setTitle:@"Мальчик" forSegmentAtIndex:0];
        [self.genderSegmentControl setTitle:@"Девочка" forSegmentAtIndex:1];
        self.phoneView.hidden = NO;
    }
}

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - Actions

- (IBAction)showGenderButtonPressed:(id)sender {
    self.genderSegmentControl.hidden = NO;
    self.showGenderButton.hidden = YES;
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (![self.userDataSwitch isOn]) {
        [self showMessage:@"Разрешите использовать ваши персональные данные"];
        return;
    }
    if (!self.lastNameTextField.text.length) {
        [self.lastNameView shake];
        return;
    }
    if (!self.nameTextField.text.length) {
        [self.nameView shake];
        return;
    }
    if (!self.patronymicTextField.text.length) {
        [self.patronymicView shake];
        return;
    }
    if (self.user_type == UserTypeChild && [self phone].length != 10) {
        [self.phoneView shake];
        return;
    }
    switch (self.user_type) {
        case UserTypeWorker:
            [self signUpWorker];
            break;
        case UserTypeParent:
            [self signUpParent];
            break;
        case UserTypeChild:
            break;
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.nameTextField) {
        [self.lastNameTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.lastNameTextField) {
        [self.patronymicTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Methods

- (void)signUpWorker{
    RPSignUpStep4VC *signUpVC = [[RPSignUpStep4VC alloc]initWithNib];
    signUpVC.user_type = self.user_type;
    signUpVC.selectedCity = self.selectedCity;
    signUpVC.selectedCompany = self.selectedCompany;
    signUpVC.name = self.nameTextField.text;
    signUpVC.lastName = self.lastNameTextField.text;
    signUpVC.patronymic = self.patronymicTextField.text;
    if (self.genderSegmentControl.selectedSegmentIndex == 0) {
        signUpVC.gender = @"male";
    } else {
        signUpVC.gender = @"female";
    }
    [self.navigationController pushViewController:signUpVC animated:YES];
}

- (void)signUpParent{
    RPSignUpStep6VC *signUpVC = [[RPSignUpStep6VC alloc]initWithNib];
    signUpVC.user_type = self.user_type;
    signUpVC.name = self.nameTextField.text;
    signUpVC.lastName = self.lastNameTextField.text;
    signUpVC.patronymic = self.patronymicTextField.text;
    if (self.genderSegmentControl.selectedSegmentIndex == 0) {
        signUpVC.gender = @"male";
    } else {
        signUpVC.gender = @"female";
    }
    [self.navigationController pushViewController:signUpVC animated:YES];
}

#pragma mark - UITextFieldDelegate

- (void)RP_textFieldDeleteBackwardPressed:(RPTextField *)textField{
    if (textField.text.length) {
        return;
    }
    if (textField == self.phoneTextField2) {
        [self.phoneTextField1 becomeFirstResponder];
    }
    if (textField == self.phoneTextField3) {
        [self.phoneTextField2 becomeFirstResponder];
    }
    if (textField == self.phoneTextField4) {
        [self.phoneTextField3 becomeFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    return YES;
}

@end
