//
//  RPMenuHeaderCollectionViewCell.m
//  ProObed
//
//  Created by Ruslan on 1/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMenuHeaderCollectionViewCell.h"
#import "RPMenuListServerModel.h"

@implementation RPMenuHeaderCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithGroup:(RPGroupServerModel *) group{
    self.headerLabel.text = group.name;
}

@end
