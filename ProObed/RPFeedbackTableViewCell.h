//
//  RPFeedbackTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPFeedbackTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;

@end
