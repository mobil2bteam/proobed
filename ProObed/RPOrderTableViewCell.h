//
//  RPOrderTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 28.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOrderInfoServerModel;

@interface RPOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;

- (void)configureCellForOrder:(RPOrderInfoServerModel *)order;
@end
