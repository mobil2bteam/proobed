//
//  RPTransitionNavController.m
//  ProObed
//
//  Created by Ruslan on 12/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPTransitionNavController.h"
#import "RPNavControllerDelegate.h"

@interface RPTransitionNavController () {
    id<UINavigationControllerDelegate> _navDelegate;
}

@end

@implementation RPTransitionNavController


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _navDelegate = [RPNavControllerDelegate new];
    self.delegate = _navDelegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
