#import "RPDeliveryListViewController.h"
#import "RPDeliveryTableViewCell.h"
#import "RPOrderServerModel.h"
#import "RPSelectDateViewController.h"

@interface RPDeliveryListViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *deliveryTableView;
@end

@implementation RPDeliveryListViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.deliveryTableView registerNib:[UINib nibWithNibName:@"RPDeliveryTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.deliveryTableView.estimatedRowHeight = 60;
    self.deliveryTableView.rowHeight = UITableViewAutomaticDimension;
    self.deliveryTableView.tableFooterView = [[UIView alloc] init];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.order.points.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RPDeliveryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.nameLabel.text = self.order.points[indexPath.row].name;
    cell.addressLabel.text = self.order.points[indexPath.row].address;
    cell.workLabel.text = self.order.points[indexPath.row].work;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RPPointServerModel *point = self.order.points[indexPath.row];
    RPSelectDateViewController *vc = [[RPSelectDateViewController alloc] initWithNibName:@"RPSelectDateViewController" bundle:nil];
    vc.order = self.order;
    vc.cart = self.cart;
    vc.point = point;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
