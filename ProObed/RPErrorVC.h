//
//  RPErrorVC.h
//  ProObed
//
//  Created by Ruslan on 1/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPErrorVC : UIViewController

@property (strong, nonatomic) NSString *message;

@property (strong, nonatomic) NSString *titleText;

@property (nonatomic, copy) void (^compilationBlock)(void);

@end
