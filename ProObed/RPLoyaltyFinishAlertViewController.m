//
//  RPLoyaltyFinishAlertViewController.m
//  ProObed
//
//  Created by Ruslan on 24.07.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import "RPLoyaltyFinishAlertViewController.h"

@interface RPLoyaltyFinishAlertViewController ()

@end

@implementation RPLoyaltyFinishAlertViewController

- (IBAction)okButonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate didFinish];
    }];
}

@end
