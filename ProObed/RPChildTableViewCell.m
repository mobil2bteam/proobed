//
//  RPChildTableViewCell.m
//  ProObed
//
//  Created by Ruslan on 19.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPChildTableViewCell.h"
#import "RPUserServerModel.h"

@implementation RPChildTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.balanceLabel.textColor = kGreenColor;
}

- (void)configureForChild:(RPUserServerModel *)child{
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", child.info.surname, child.info.name];
    self.balanceLabel.text = [NSString stringWithFormat:@"%.2f р.", child.info.balance];
    // MARK: TODO check number
    self.phoneLabel.text = child.info.number;
}

@end
