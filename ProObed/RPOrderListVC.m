#import "RPOrderListVC.h"
#import "RPOrderServerModel.h"
#import "RPOrderTableViewCell.h"
#import "RPOrderDetailVC.h"

@interface RPOrderListVC () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;

@end

@implementation RPOrderListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.orderList = kAppDelegate.currentUser.info.orders;
    self.ordersTableView.estimatedRowHeight = 60;
    self.ordersTableView.rowHeight = UITableViewAutomaticDimension;
    self.ordersTableView.tableFooterView = [[UIView alloc] init];
    [self.ordersTableView registerNib:[UINib nibWithNibName:@"RPOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPOrderTableViewCell"];
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.orderList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPOrderTableViewCell class]) forIndexPath:indexPath];
    RPOrderInfoServerModel *order = self.orderList[indexPath.row];
    [cell configureCellForOrder:order];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RPOrderInfoServerModel *order = self.orderList[indexPath.row];
    RPOrderDetailVC *vc = [[RPOrderDetailVC alloc] initWithNib];
    vc.order = order;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
