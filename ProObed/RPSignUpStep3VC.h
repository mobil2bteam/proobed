//
//  RPSignUpStep3VC.h
//  ProObed
//
//  Created by Ruslan on 12/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPCityServerModel;
@class RPCompanyServerModel;

@interface RPSignUpStep3VC : UIViewController
@property (strong, nonatomic) RPCityServerModel *selectedCity;
@property (strong, nonatomic) RPCompanyServerModel *selectedCompany;
@property (nonatomic, assign) UserType user_type;
@end
