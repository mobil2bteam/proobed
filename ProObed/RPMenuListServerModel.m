#import "RPMenuListServerModel.h"

@implementation RPMenuListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPMenuServerModel class] forKeyPath:@"menu"];
    }];
}

@end


@implementation RPMenuServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPGroupServerModel class] forKeyPath:@"groups"];
        [mapping mapPropertiesFromArray:@[@"date"]];
    }];
}

@end


@implementation RPGroupServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPItemServerModel class] forKeyPath:@"items"];
        [mapping mapPropertiesFromArray:@[@"name", @"image", @"code"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        
    }];
}

@end


@implementation RPItemServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPTypeServerModel class] forKeyPath:@"types"];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"description" toProperty:@"itemDescription"];
        [mapping mapPropertiesFromArray:@[@"price", @"name", @"weight", @"typeWeight", @"image", @"code", @"heh"]];
    }];}

@end
