#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPCompanyServerModel;
@class RPDeliveryServerModel;
@class RPWorkServerModel;

@interface RPCompanyListServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, strong) NSArray<RPCompanyServerModel *> *companies;
@property (nonatomic, strong) NSArray<RPCompanyServerModel *> *schools;

+(EKObjectMapping *)objectMapping;
@end


@interface RPCompanyServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) NSInteger cityId;
@property (nonatomic, copy) NSString *prefix;
@property (nonatomic, copy) NSString *company_description;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) NSArray<RPDeliveryServerModel *> *deliveries;
@end


@interface RPDeliveryServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSArray<RPWorkServerModel *> *works;
@end

@interface RPWorkServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *hours_start;
@property (nonatomic, copy) NSString *hours_finish;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL free_day;
@end
