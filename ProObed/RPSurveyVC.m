#import "RPSurveyVC.h"
#import "RPSurveyServerModel.h"
#import "RPSurveyCollectionViewCell.h"
#import "RPSurveyHeaderCollectionViewCell.h"
#import "RPServerManager.h"
#import "RPUserServerModel.h"
#import "UICollectionView+Extensions.h"

@implementation RPSurveyVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // register nibs
    [self.surveyCollectionView registerNib:[RPSurveyCollectionViewCell class]];
    [self.surveyCollectionView registerNib:[RPSurveyHeaderCollectionViewCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader];
}

#pragma mark - Actions

- (IBAction)sendSurveyButtonPressed:(id)sender {
    if ([self isSurvayCompleted]) {
        NSDictionary *params = @{@"surveyId":@(self.survey.ID),
                                 @"data": [self encodeToJSON]};
        [self addMBProgressHUD];
        [[RPServerManager sharedManager]sendSurveyWithParams:params onSuccess:^(BOOL status, RPErrorServerModel *error) {
            [self hideMBProgressHUD];
            if (error) {
                [self showMessage:error.text];
            } else {
                self.closeView.hidden = NO;
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            NSLog(@"error - %@", error.localizedDescription);
            [self hideMBProgressHUD];
            [self showMessage:RPErrorMessage];
        }];
    } else {
        [self showMessage:@"Опрос не заполнен"];
    }
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okButtonPresse:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Methods

- (BOOL)isSurvayCompleted{
    for (NSInteger i = 0; i < self.survey.questions.count; i++) {
        for (NSInteger j = 0; j < self.survey.questions[i].options.count; j++) {
            if (self.survey.questions[i].options[j].isSelected) {
                return YES;
            }
        }
    }
    return NO;
}

- (NSString *)encodeToJSON{
    NSMutableArray *questionsArray = [[NSMutableArray alloc]init];
    for (NSInteger i = 0; i < self.survey.questions.count; i++) {
        NSMutableArray *answersArray = [[NSMutableArray alloc]init];
        for (NSInteger j = 0; j < self.survey.questions[i].options.count; j++) {
            if (self.survey.questions[i].options[j].isSelected) {
                [answersArray addObject:@(self.survey.questions[i].options[j].ID)];
            }
        }
        if (answersArray.count) {
            [questionsArray addObject:@{@"questionId":@(self.survey.questions[i].ID),
                                        @"options":answersArray}];
        }
    }

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:questionsArray
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return @"";
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json - %@", jsonString);
        return jsonString;
    }
}

- (NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint dic:(NSDictionary *)dic {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end
