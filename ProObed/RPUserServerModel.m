#import "RPUserServerModel.h"
#import "SAMKeychain.h"
#import "RPOrderServerModel.h"
#import "RPBallServerModel.h"
#import "RPTicketListServerModel.h"

@implementation RPUserServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[RPInfoServerModel class] forKeyPath:@"user.info" forProperty:@"info"];
    }];
}

+ (void)logOut{
    kAppDelegate.currentUser = nil;
//    [SAMKeychain deletePasswordForService:kPinKey account:kBundleID];
//    [SAMKeychain deletePasswordForService:kPhoneKey account:kBundleID];
//    [SAMKeychain deletePasswordForService:kTokenKey account:kBundleID];
    [kUserDefaults removeObjectForKey:kPinKey];
    [kUserDefaults removeObjectForKey:kPhoneKey];
    [kUserDefaults removeObjectForKey:kTokenKey];
    [kUserDefaults synchronize];
}

- (void)addChild:(RPUserServerModel *)child{
    self.info.childs = [self.info.childs arrayByAddingObjectsFromArray:@[child]];
}

- (void)addSecondParent:(RPUserServerModel *)secondParent{
    RPParentServerModel *parent = [[RPParentServerModel alloc] init];
    parent.name = secondParent.info.name;
    parent.surname = secondParent.info.surname;
    parent.patronymic = secondParent.info.patronymic;
    parent.phone = secondParent.info.phone;
    parent.parentId = secondParent.info.userId;
    parent.active = YES;
    self.info.parent = parent;
}

@end


@implementation RPInfoServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPUserServerModel class] forKeyPath:@"childs"];
        [mapping hasMany:[RPOrderInfoServerModel class] forKeyPath:@"orders"];
        [mapping hasOne:[RPParentServerModel class] forKeyPath:@"parent"];
        [mapping hasOne:[RPLoyaltyServerModel class] forKeyPath:@"loyalty"];
        [mapping hasOne:[RPBallServerModel class] forKeyPath:@"balls"];
        [mapping hasMany:[RPTicketServerModel class] forKeyPath:@"tickets"];
        [mapping mapPropertiesFromArray:@[@"name", @"surname", @"patronymic", @"phone", @"gender", @"number", @"email", @"companyId", @"role", @"guid", @"code1c", @"qr", @"pin", @"accessToken", @"verifyToken", @"authKey", @"balance", @"card", @"password", @"user_type", @"summTicket", @"wallet"]];
        [mapping mapKeyPath:@"description" toProperty:@"user_description"];
        [mapping mapKeyPath:@"id" toProperty:@"userId"];
    }];
}

- (NSString *)formattedName{
    return [NSString stringWithFormat:@"%@ %@ %@",self.surname, self.name, self.patronymic];
}

@end

@implementation RPParentServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"surname", @"patronymic", @"phone", @"active"]];
        [mapping mapKeyPath:@"description" toProperty:@"user_description"];
        [mapping mapKeyPath:@"id" toProperty:@"parentId"];
    }];
}

- (NSString *)formattedName{
    return [NSString stringWithFormat:@"%@ %@ %@",self.surname, self.name, self.patronymic];
}
@end

@implementation RPLoyaltyServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"text", @"maxBalls"]];
        [mapping mapKeyPath:@"register" toProperty:@"isRegister"];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end

