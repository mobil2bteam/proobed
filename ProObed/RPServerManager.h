#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RPErrorServerModel.h"

@class RPUserServerModel;
@class RPStockListServerModel;
@class RPMenuListServerModel;
@class RPMenuServerModel;
@class RPSurveyServerModel;
@class RPOrderServerModel;
@class RPOrderListServerModel;
@class RPOrderInfoServerModel;

typedef void (^FailureBlock) (NSError *error, NSInteger statusCode);

@interface RPServerManager : NSObject

+ (RPServerManager*) sharedManager;

/*!
 * @discussion Load all options that are required for application
 */
- (void)getOptionsOnSuccess:(void(^)()) success
                 onFailure:(void(^)()) failure;

/*!
 * @discussion Log in user
 * @param params Parameters for log in
 •	login — телефон (строка)
 •	pin — PIN-код (строка)
*/
- (void)postLoginWithParams:(NSDictionary *) params
                  onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Sign up user
 * @param params Parameters for sign up
 •	name — имя (Строка)
 •	surname — Фамилия (Строка)
 •	patronymic — Отчество (Строка)
 •	phone — телефон (Строка)
 •	cityId — идентификатор города, в котором находится сотрудник (Число)
 •	companyId — идентификатор компании, в которой работает сотрудник (Число)
 •	code — код для подтверждения с СМС (Строка)
 •	pin — PIN-код для быстрого доступа (Строка)
 •	user_type — тип пользователя (Число)
 
 Optional, if user has numberID:
 •	number — табельный номер сотрудника (Строка)
 •	type:verify
 */
- (void)postRegisterWithParams:(NSDictionary *) params
                     onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Pre registration to check user data. Success block return userInfo with code or error
 * @param params Parameters for pre register. The same as for sign up
 */
- (void)postPreRegisterWithParams:(NSDictionary *) params
                     onSuccess:(void(^)(NSString *userInfo, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Send feedback
 * @param params Parameters for feedback
 •	typeId  - идентификатор типа обращения Число)
 •	text - текст обращения (Строка)
 */
- (void)postFeedbackWithParams:(NSDictionary *) params
                        onSuccess:(void(^)(BOOL status, RPErrorServerModel *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
/*!
 * @discussion Load all company's stocks
 * @param params Parameters for stocks
 •	сompanyId — идентификатор компании (Число)
 •	dateFrom -дата начала акций (Строка) (YYYY-MM-DD)
 •	dateTo - дата окончания акций (Строка) (YYYY-MM-DD)
 */
- (void)getStocksWithParams:(NSDictionary *) params
                      cache:(BOOL)cache
                     onSuccess:(void(^)(RPStockListServerModel *stocks, RPErrorServerModel *error)) success
                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Load company's menu
 * @param params Parameters for menu
 •	companyId - идентификатор компании (число)
 •	date - дата (YYYY-MM-DD)
 */
- (void)getMenuListWithParams:(NSDictionary *) params
                        cache:(BOOL)cache
                 onSuccess:(void(^)(RPMenuListServerModel *menuList, RPErrorServerModel *error)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Load survay for users
 */
- (void)getSurveyOnSuccess:(void(^)(RPSurveyServerModel *survey, RPErrorServerModel *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Send user's survay
 * @param params Parameters for menu
 •	surveyId - идентификатор опроса (число)
 •	datа - данные опроса (JSON)
 */
- (void)sendSurveyWithParams:(NSDictionary *) params
                   onSuccess:(void(^)(BOOL isSuccess, RPErrorServerModel *error)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRecoverPasswordWithParams:(NSDictionary *) params
                            onSuccess:(void(^)(RPErrorServerModel *error, NSString *info)) success
                            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postChangePinWithParams:(NSDictionary *) params
                            onSuccess:(void(^)(RPErrorServerModel *error, NSString *info, NSString *message)) success
                            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRegisterChildWithParams:(NSDictionary *) params
                          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postVerifyChildWithParams:(NSDictionary *) params
                          onSuccess:(void(^)(RPUserServerModel *child, RPErrorServerModel *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
/*!
 * @discussion Предварительная регистрация 2-ого родителя
 * @param params Parameters
 */
- (void)postRegisterSecondParentWithParams:(NSDictionary *) params
                          onSuccess:(void(^)(BOOL success, RPErrorServerModel *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*!
 * @discussion Получить счета
 * @param params Parameters
 * @param isForParent Кто запрашивает счет, родитель или ребенок
 */
- (void)getTicketsWithParams:(NSDictionary *) params
                   forParent:(BOOL)isForParent
                   onSuccess:(void(^)(RPTicketListServerModel *tickets, RPErrorServerModel *error)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


#pragma mark - Заказ полуфабрикатов

/*!
 * @discussion Получить меню для заказа полуфабрикатов
 * @param companyId ИД компании
 * @param dateFrom дата от (по умолчанию, сегодня)
 * @param dateTo дата до
 * @param takeFromCache проверять ли кещ для запроса
 * @param cacheResponse нужно ли кэшировать ответ
 */
- (void)getProductsForCompany:(NSInteger)companyId
                     dateFrom:(NSString *)dateFrom
                       dateTo:(NSString *)dateTo
                takeFromCache:(BOOL)takeFromCache
                cacheResponse:(BOOL)cacheResponse
                    onSuccess:(void(^)(RPMenuServerModel *menu,
                                       RPErrorServerModel *error)) success
                    onFailure:(FailureBlock) failure;


/*!
 * @discussion Получить меню для заказа полуфабрикатов
 * @param cart товары в формате id-count (1-5;2-10;11-1)
 */
- (void)getCartForProducts:(NSString *)cart
                    onSuccess:(void(^)(RPOrderServerModel *order,
                                       RPErrorServerModel *error)) success
                    onFailure:(FailureBlock) failure;

/*!
 * @discussion Оформить заказ
 * @param cart товары в формате id-count (1-5;2-10;11-1)
 * @param date дата доставки
 * @param receiptId ИД места доставки
 */
- (void)postOrderForProducts:(NSString *)cart
                        date:(NSString *)date
                   receiptId:(NSInteger)receiptId
                 onSuccess:(void(^)(RPOrderInfoServerModel *order,
                                    RPErrorServerModel *error)) success
                 onFailure:(FailureBlock) failure;

/*!
 * @discussion Получить список всех заказов пользователя
 */
- (void)getOrdersOnSuccess:(void(^)(NSArray <RPOrderInfoServerModel *> *orders,
                                    RPErrorServerModel *error)) success
                 onFailure:(FailureBlock) failure;

- (void)removeChild:(NSDictionary *) params
          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)updateChild:(NSDictionary *) params
          onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)getChildInfo:(NSDictionary *) params
           onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)setLoyalty:(NSDictionary *) params
           onSuccess:(void(^)(RPUserServerModel *user, RPErrorServerModel *error)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)cancelOrderWithParams:(NSDictionary *) params
                    onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                    onFailure:(void(^)(NSString *error)) failure;

- (void)sendFCMTokenToServer;

- (void)checkUserOnSuccess:(void(^)()) success;
@end
