//
//  RPEditPhoneViewController.h
//  ProObed
//
//  Created by Ruslan on 04.04.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPEditPhoneViewController : UIViewController
@property (nonatomic, copy) void (^compilationBlock)(void);
@property (nonatomic, strong) NSString *token;
@property (nonatomic, assign) NSInteger childId;
@property (nonatomic, strong) NSString *childPhone;

@end
