//
//  RPError.h
//  ProObed
//
//  Created by Ruslan on 10/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPErrorServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *message;

@property (nonatomic, assign) NSInteger code;

@end
