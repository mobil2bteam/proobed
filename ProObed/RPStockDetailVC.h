//
//  RPStockDetailVC.h
//  ProObed
//
//  Created by Ruslan on 1/9/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPStockServerModel;

@interface RPStockDetailVC : UIViewController 

@property (strong, nonatomic) RPStockServerModel *stock;

@property (weak, nonatomic) IBOutlet UIImageView *stockImageView;

@end
