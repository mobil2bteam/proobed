//
//  RPChildTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 19.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPUserServerModel;

@interface RPChildTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

- (void)configureForChild:(RPUserServerModel *)child;
@end
