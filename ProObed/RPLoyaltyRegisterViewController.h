//
//  RPLoyaltyRegisterViewController.h
//  ProObed
//
//  Created by Ruslan on 24.07.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RPLoyaltyRegisterDelegate <NSObject>
- (void)didFinish;
@end

@interface RPLoyaltyRegisterViewController : UIViewController

@end
