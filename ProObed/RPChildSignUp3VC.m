#import "RPChildSignUp3VC.h"
#import "AppDelegate.h"
#import "RPCityTableViewCell.h"
#import "RPCityListServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPChildSignUp4VC.h"

@interface RPChildSignUp3VC ()
@property (weak, nonatomic) IBOutlet UITableView *schoolTableView;
@property (nonatomic, strong) NSArray <RPCompanyServerModel *> *schools;
@end

@implementation RPChildSignUp3VC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Getters

- (NSArray <RPCompanyServerModel *> *)schools{
    if (_schools) {
        return _schools;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityId == %ld", self.selectedCity.ID];
    _schools = [kAppDelegate.companies.schools filteredArrayUsingPredicate:predicate];
    return _schools;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return  self.schools.count;
}

- (RPCityTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPCityTableViewCell class]) forIndexPath:indexPath];
    cell.cityLabel.text = self.schools[indexPath.row].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPChildSignUp4VC *vc = [[RPChildSignUp4VC alloc] initWithNib];
    vc.selectedCity = self.selectedCity;
    vc.selectedSchool = self.schools[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
