//
//  RPSurveyViewController.h
//  ProObed
//
//  Created by Ruslan on 2/8/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPSurveyServerModel;

@interface RPSurveyVC : UIViewController

@property (strong, nonatomic) RPSurveyServerModel *survey;

@property (weak, nonatomic) IBOutlet UICollectionView *surveyCollectionView;

@property (weak, nonatomic) IBOutlet UIView *closeView;

@end
