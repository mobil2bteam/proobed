#import "RPTicketListServerModel.h"

@implementation RPTicketListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPTicketServerModel class] forKeyPath:@"tickets"];
    }];
}

@end


@implementation RPTicketServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPTicketOperationServerModel class] forKeyPath:@"operations"];
        [mapping mapPropertiesFromArray:@[@"value", @"number", @"userID"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


@implementation RPTicketOperationServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"value", @"ticketId"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];}

@end
