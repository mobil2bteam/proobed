//
//  NSString+ToDictionary.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPString+Convert.h"

@implementation NSString (Convert)

//// возвращает строку из словаря
//+ (NSString *)inputDictionary:(NSMutableDictionary *)dic{
//    NSError* error;
//    NSDictionary* tempDict = [dic copy]; // get Dictionary from mutable Dictionary
//    //giving error as it takes dic, array,etc only. not custom object.
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:tempDict
//                                                       options:NSJSONReadingMutableLeaves error:&error];
//    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
//                                             encoding:NSUTF8StringEncoding];
//    return nsJson;
//    
//    [NSString inputDictionary:<#(NSMutableDictionary *)#>]
//}

// возвращает строку из словаря
- (NSDictionary *)dictionaryValue{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:nil];
    return jsonResponse;
}

@end
