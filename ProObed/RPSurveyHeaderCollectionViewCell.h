//
//  RPSurveyHeaderCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 2/8/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPQuestionServerModel;

@interface RPSurveyHeaderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

- (void)configureCellWithQuestion:(RPQuestionServerModel *)question;

@end
