#import "RPQRCodeVC.h"
#import "UIImage+MDQRCode.h"
#import "RPUserServerModel.h"

@interface RPQRCodeVC ()

@property (weak, nonatomic) IBOutlet UIImageView *qrCodeImageView;

@end

@implementation RPQRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.showPayBallsCode) {
        /// {"user_guid":"676555-7888-abcbca","user_number":"тут табельный","ball_sum":345, "max_procent":50 }
        NSString *guid = RPCurrentUser.info.code1c;
        NSString *number = RPCurrentUser.info.number;
        CGFloat max = RPCurrentUser.info.loyalty.maxBalls;
        NSString *code = [NSString stringWithFormat:@"{\"user_guid\":\"%@\",\"user_number\":\"%@\",\"ball_sum\":%@,\"max_procent\":%.f}", guid, number, self.ballsCode, max];
        NSLog(@"code - %@", code);
        self.qrCodeImageView.image = [UIImage mdQRCodeForString:code size:self.qrCodeImageView.bounds.size.width fillColor:[UIColor blackColor]];
    } else {
        self.qrCodeImageView.image = [UIImage mdQRCodeForString:kAppDelegate.currentUser.info.card size:self.qrCodeImageView.bounds.size.width fillColor:[UIColor blackColor]];
    }
}

#pragma mark - Action

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
