
#import "RPCache.h"

NS_ASSUME_NONNULL_BEGIN

@interface RPCache (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) NSString *params;
@property (nullable, nonatomic, retain) NSNumber *seconds;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *json;

@end

NS_ASSUME_NONNULL_END
