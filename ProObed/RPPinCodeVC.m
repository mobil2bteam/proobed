
static NSInteger const PinLength = 4;

#import "RPPinCodeVC.h"
#import "RPServerManager.h"
#import "RPRouter.h"
#import "SAMKeychain.h"

@interface RPPinCodeVC ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *numberButtonsArray;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *dotsImageViewsArray;

@property (strong, nonatomic) NSString *codeString;

@property (weak, nonatomic) IBOutlet UIView *pinCodeView;

@end

@implementation RPPinCodeVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.codeString = @"";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Methods

- (void)logIn{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kLoginKey] = [[NSUserDefaults standardUserDefaults] stringForKey:kPhoneKey];
    params[kPinKey] = [[NSUserDefaults standardUserDefaults] stringForKey:kPinKey];
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postLoginWithParams:[params copy] onSuccess:^(RPUserServerModel *user, RPErrorServerModel *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else {
            [RPRouter setMainViewController];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (void)clearPinCode{
    self.codeString = @"";
    for (UIImageView *dotImageView in self.dotsImageViewsArray) {
        dotImageView.image = [UIImage imageNamed:@"dot-inactive"];
    }
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)numberButtonPressed:(UIButton *)sender {
    if (self.codeString.length == PinLength) {
        return;
    }
    ((UIImageView *)self.dotsImageViewsArray[self.codeString.length]).image = [UIImage imageNamed:@"dot-active"];
    self.codeString = [NSString stringWithFormat:@"%@%ld", self.codeString, (long)sender.tag];
    if (self.codeString.length == PinLength) {
        NSString *pin = [[NSUserDefaults standardUserDefaults] stringForKey:@"pin"];
        if ([pin isEqualToString:self.codeString]) {
            [self logIn];
        } else {
            [self.pinCodeView shake];
            [self clearPinCode];
        }
    }
}

- (IBAction)rememberPasswordButtonPressed:(id)sender {
    NSString *phone = [[NSUserDefaults standardUserDefaults] stringForKey:kPhoneKey];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"phone"] = kAppDelegate.currentUser.info.phone;
    params[@"user_type"] = @(kAppDelegate.currentUser.info.user_type);
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postRecoverPasswordWithParams:params onSuccess:^(RPErrorServerModel *error, NSString *info) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error.text];
        } else if (info){
            [self showMessage:info];
        } else {
            [self showMessage:RPErrorMessage];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage];
    }];
}

- (IBAction)removeLastNumberButtonPressed:(id)sender {
    if (self.codeString.length == 0) {
        return;
    }
    self.codeString = [self.codeString substringToIndex:[self.codeString length]-1];
    ((UIImageView *)self.dotsImageViewsArray[self.codeString.length]).image = [UIImage imageNamed:@"dot-inactive"];
}

@end
