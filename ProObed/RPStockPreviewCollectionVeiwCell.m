//
//  RPStockPreviewCollectionVeiwCell.m
//  ProObed
//
//  Created by Ruslan on 1/9/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockPreviewCollectionVeiwCell.h"
#import "UIImageView+AFNetworking.h"
#import "RPStockListServerModel.h"

@implementation RPStockPreviewCollectionVeiwCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithStock:(RPStockServerModel *)stock{
    self.stockLabel.text = stock.name;
    self.stockImageView.image = nil;
    NSURL *url = [NSURL URLWithString: stock.image];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.stockImageView setImageWithURLRequest:imageRequest
                                       placeholderImage:nil
                                                success:nil
                                                failure:nil];
}

@end
