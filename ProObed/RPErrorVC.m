//
//  RPErrorVC.m
//  ProObed
//
//  Created by Ruslan on 1/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPErrorVC.h"

@interface RPErrorVC ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation RPErrorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.messageLabel.text = self.message;
    if (self.titleText != nil) {
        self.titleLabel.text = [self titleText];
    }
}

- (IBAction)okButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.compilationBlock) {
            weakSelf.compilationBlock();
        }
    }];
}

@end
