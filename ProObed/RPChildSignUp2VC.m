#import "RPChildSignUp2VC.h"
#import "AppDelegate.h"
#import "RPCityListServerModel.h"
#import "RPCityTableViewCell.h"
#import "RPChildSignUp3VC.h"
@interface RPChildSignUp2VC ()
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;
@end

@implementation RPChildSignUp2VC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return kAppDelegate.cities.cities.count;
}

- (RPCityTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPCityTableViewCell class]) forIndexPath:indexPath];
    cell.cityLabel.text = kAppDelegate.cities.cities[indexPath.row].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPCityServerModel *selectedCity = kAppDelegate.cities.cities[indexPath.row];
    RPChildSignUp3VC *vc = [[RPChildSignUp3VC alloc] initWithNib];
    vc.selectedCity = selectedCity;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
