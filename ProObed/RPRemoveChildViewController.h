//
//  RPRemoveChildViewController.h
//  ProObed
//
//  Created by Ruslan on 04.04.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRemoveChildViewController : UIViewController
@property (nonatomic, copy) void (^compilationBlock)(void);
@end
