#import "RPFeedbackListServerModel.h"

@implementation RPFeedbackListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPFeedbackServerModel class] forKeyPath:@"types"];
    }];
}

@end

@implementation RPFeedbackServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end
