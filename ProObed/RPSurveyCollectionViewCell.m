//
//  RPSurveyCollectionViewCell.m
//  ProObed
//
//  Created by Ruslan on 2/8/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#define kGrayColor [UIColor colorWithRed:0.898 green:0.898 blue:0.898 alpha:1.00]

#define kBlueColor [UIColor colorWithRed:0.000 green:0.769 blue:0.980 alpha:1.00]

#import "RPSurveyCollectionViewCell.h"
#import "RPSurveyServerModel.h"

@implementation RPSurveyCollectionViewCell

- (void)configureCellWithOption:(RPOptionServerModel *)option answerType:(NSString *)answerType{
    self.answerLabel.text = option.text;
    if ([answerType isEqualToString:@"radio"]) {
        _circleView.layer.cornerRadius = CGRectGetHeight(_circleView.frame) / 2;
        _innerCircleView.layer.cornerRadius = CGRectGetHeight(_innerCircleView.frame) / 2;
    } else {
        _circleView.layer.cornerRadius = 0;
        _innerCircleView.layer.cornerRadius = 0;
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.answerLabel.textColor = [UIColor whiteColor];
        self.innerCircleView.backgroundColor = kBlueColor;
    } else {
        self.answerLabel.textColor = [UIColor blackColor];
        self.innerCircleView.backgroundColor = kGrayColor;
    }
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
}

- (void)checkAnswer:(BOOL)check{
    if (check) {
        self.answerLabel.textColor = [UIColor whiteColor];
        self.innerCircleView.backgroundColor = kBlueColor;
    } else {
        self.answerLabel.textColor = [UIColor blackColor];
        self.innerCircleView.backgroundColor = kGrayColor;
    }
}

@end
