
#import "RPSignUpStep2VC.h"
#import "RPSignUpStep3VC.h"
#import "RPCityTableViewCell.h"
#import "RPCompanyListServerModel.h"
#import "RPCityListServerModel.h"
#import "RPRouter.h"

@interface RPSignUpStep2VC ()
@property (weak, nonatomic) IBOutlet UITableView *companyTableView;
@property (strong, nonatomic) NSArray<RPCompanyServerModel *> *filtedCompanies;
@end

@implementation RPSignUpStep2VC

#pragma mark - Getters

- (NSArray<RPCompanyServerModel *> *)filtedCompanies{
    if (_filtedCompanies) {
        return _filtedCompanies;
    }
    NSMutableArray *filteredCompanies = [[NSMutableArray alloc]init];
    for (RPCompanyServerModel *company in kAppDelegate.companies.companies) {
        if (company.cityId == self.selectedCity.ID) {
            [filteredCompanies addObject:company];
        }
    }
    _filtedCompanies = [filteredCompanies copy];
    return _filtedCompanies;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.filtedCompanies.count;
}

- (RPCityTableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPCityTableViewCell class]) forIndexPath:indexPath];
    cell.cityLabel.text = self.filtedCompanies[indexPath.row].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPCompanyServerModel *selectedCompany = self.filtedCompanies[indexPath.row];
    // если режим демонстрации
    if (self.isDemonstration) {
        [RPRouter setMainViewControllerWithCity:self.selectedCity company:selectedCompany];
    } else {
        RPSignUpStep3VC *signUpVC = [[RPSignUpStep3VC alloc]initWithNib];
        signUpVC.selectedCity = self.selectedCity;
        signUpVC.selectedCompany = selectedCompany;
        signUpVC.user_type = self.user_type;
        [self.navigationController pushViewController:signUpVC animated:YES];
    }
}

@end
