#import "RPLogoutViewController.h"

@interface RPLogoutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation RPLogoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.text.length) {
        self.textLabel.text = self.text;
    }
}

- (IBAction)yesButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.compilationBlock) {
            weakSelf.compilationBlock();
        }
    }];
}

- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
