#import "RPSelectDateViewController.h"
#import <FSCalendar/FSCalendar.h>
#import "RPOrderServerModel.h"
#import "RPServerManager.h"
#import "RPOrderDetailVC.h"

@interface RPSelectDateViewController () <FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance>
@property (weak, nonatomic) IBOutlet FSCalendar *calendarView;
@property (nonatomic, strong) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *workLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation RPSelectDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set Monday as first day of a week
    self.selectedDate = [[NSDate date] dateByAddingTimeInterval: 24 * 60 *60];
    self.calendarView.firstWeekday = 2;
    self.calendarView.today = nil;
    self.calendarView.appearance.weekdayTextColor = [UIColor colorWithRed:0.71 green:0.28 blue:0.26 alpha:1.0];
    self.calendarView.appearance.headerTitleColor = [UIColor whiteColor];
    [self.calendarView selectDate: [[NSDate date] dateByAddingTimeInterval: 24 * 60 *60]];
    self.nameLabel.text = self.point.name;
    self.addressLabel.text = self.point.address;
    self.workLabel.text = self.point.work;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld р.", (long)self.order.order.total];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)orderButtonPressed:(id)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSString *date = [dateFormat stringFromDate:self.selectedDate];
    RPOrderDetailVC *vc = [[RPOrderDetailVC alloc] initWithNibName:@"RPOrderDetailVC" bundle:nil];
    vc.order = self.order.order;
    vc.isNewOrder = YES;
    vc.point = self.point;
    vc.date = date;
    vc.cart = self.cart;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)finishOrder{
    
}

#pragma mark - FSCalendar

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    
}

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar{
    return [[NSDate date] dateByAddingTimeInterval: 24 * 60 *60];
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar{
    return [[NSDate date] dateByAddingTimeInterval: 7 * 24 * 60 *60];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    return [UIColor colorWithRed:0.25 green:0.63 blue:0.40 alpha:1.0];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    return [UIColor whiteColor];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    self.selectedDate = date;
}

@end
