#import "RPUserOfficeVC.h"
#import "RPModalTransitionDelegate.h"
#import "RPUserServerModel.h"
#import "RPQRCodeVC.h"
#import "RPEditUserInfoVC.h"
#import "RPReportListVC.h"
#import "RPServerManager.h"
#import "RPOrderListVC.h"
#import "RPAboutProjectVC.h"
#import "RPLoyaltyViewController.h"
#import "RPBallServerModel.h"
#import "RPPayBallsViewController.h"

@interface RPUserOfficeVC ()

@property (nonatomic) RPModalTransitionDelegate* transitionDelegate;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (weak, nonatomic) IBOutlet UIButton *qrButton;

@property (weak, nonatomic) IBOutlet UIButton *reportButton;

@property (weak, nonatomic) IBOutlet UIButton *ordersButton;

@property (weak, nonatomic) IBOutlet UILabel *summTicketLabel;

@property (weak, nonatomic) IBOutlet UILabel *walletLabel;

@property (weak, nonatomic) IBOutlet UIView *ordersView;
@property (weak, nonatomic) IBOutlet UIButton *ballsButton;
@property (weak, nonatomic) IBOutlet UIButton *loyaltyButton;
@end

@implementation RPUserOfficeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    if (kAppDelegate.currentUser.info.orders.count == 0) {
        self.ordersView.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self prepareButtons];
}

- (void)prepareButtons {
    self.ballsButton.hidden = YES;
    self.loyaltyButton.hidden = YES;
    if (RPCurrentUser.info.loyalty != nil) {
        if (RPCurrentUser.info.loyalty.ID && !RPCurrentUser.info.loyalty.isRegister) {
            self.loyaltyButton.hidden = NO;
        }
    }
    if (RPCurrentUser.info.balls != nil && RPCurrentUser.info.loyalty.isRegister) {
        if (RPCurrentUser.info.balls.value > 0) {
            self.ballsButton.hidden = NO;
            self.loyaltyButton.hidden = YES;
        }
    }
}

#pragma mark - UIModalTransitionDelegate

- (RPModalTransitionDelegate* )transitionDelegate {
    if(!_transitionDelegate) {
        _transitionDelegate = [RPModalTransitionDelegate new];
    }
    return _transitionDelegate;
}

#pragma mark - Methods

- (void)initialize{
    // hide navigation bar
    self.navigationController.navigationBar.hidden = YES;
    
    // set userName (format: Name L.P.)
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ %@ %@",RPCurrentUser.info.surname, RPCurrentUser.info.name, RPCurrentUser.info.patronymic];
    self.balanceLabel.text = [NSString stringWithFormat:@"%.2f Р.", RPCurrentUser.info.balance];
    
    self.summTicketLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)RPCurrentUser.info.summTicket];
    self.walletLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)RPCurrentUser.info.wallet];
}

- (void)showOrderListWithOrders:(NSArray <RPOrderInfoServerModel *> *)orders{
    RPOrderListVC *vc = [[RPOrderListVC alloc] initWithNib];
    vc.orderList = orders;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (IBAction)ordersButtonPressed:(id)sender {
    self.ordersButton.highlighted = NO;
    RPOrderListVC *vc = [[RPOrderListVC alloc] initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editUserInfoButtonPresssed:(id)sender {
    RPEditUserInfoVC *editUserInfoVC = [[RPEditUserInfoVC alloc]initWithNib];
    self.transitionDelegate.modalAppearanceDirection = RPModalAppearanceDirectionFromLeft;
    [self RP_customPresentViewController:editUserInfoVC animated:YES delegate:self.transitionDelegate];
}

- (IBAction)qrButtonTouchUpInside:(id)sender {
    self.qrButton.highlighted = NO;
    RPQRCodeVC *qrCodeVC = [[RPQRCodeVC alloc]initWithNib];
    [self RP_presentViewControllerCrossDisolve:qrCodeVC animated:YES];
}

- (IBAction)qrButtonDragInside:(id)sender {
    self.qrButton.highlighted = YES;
}

- (IBAction)qrButtonDragOutside:(id)sender {
    self.qrButton.highlighted = NO;
}

- (IBAction)qrButtonTouchDown:(id)sender {
    self.qrButton.highlighted = YES;
}

- (IBAction)reportButtonTouchDown:(id)sender {
    self.reportButton.highlighted = YES;
}

- (IBAction)reportButtonDragInside:(id)sender {
    self.reportButton.highlighted = YES;
}

- (IBAction)reportButtonDragOutside:(id)sender {
    self.reportButton.highlighted = NO;
}

- (IBAction)ordersButtonTouchDown:(id)sender {
    self.ordersButton.highlighted = YES;
}

- (IBAction)ordersButtonDragInside:(id)sender {
    self.ordersButton.highlighted = YES;
}

- (IBAction)orderButtonDragOutside:(id)sender {
    self.reportButton.highlighted = NO;
}

- (IBAction)reportButtonTouchUpInside:(id)sender {
    self.reportButton.highlighted = NO;
    RPReportListVC *reportListVC = [[RPReportListVC alloc]initWithNib];
    self.transitionDelegate.modalAppearanceDirection = RPModalAppearanceDirectionFromRight;
    [self RP_customPresentViewController:reportListVC animated:YES delegate:self.transitionDelegate];
}

- (IBAction)aboutProjectButtonPressed:(id)sender {
    RPAboutProjectVC *stockListVC = [[RPAboutProjectVC alloc]initWithNib];
    [self RP_presentViewController:stockListVC animated:YES];
}

- (IBAction)loyalButtonPressed:(id)sender {
    RPLoyaltyViewController *vc = [[RPLoyaltyViewController alloc] initWithNib];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)ballsButtonPressed:(id)sender {
    RPPayBallsViewController *vc = [[RPPayBallsViewController alloc] initWithNib];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
