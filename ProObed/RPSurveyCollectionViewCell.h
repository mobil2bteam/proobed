//
//  RPSurveyCollectionViewCell.h
//  ProObed
//
//  Created by Ruslan on 2/8/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOptionServerModel;

@interface RPSurveyCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

@property (weak, nonatomic) IBOutlet UIView *circleView;

@property (weak, nonatomic) IBOutlet UIView *innerCircleView;

- (void)configureCellWithOption:(RPOptionServerModel *)option answerType:(NSString *)answerType;

- (void)checkAnswer:(BOOL)check;

@end
