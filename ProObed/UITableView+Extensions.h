//
//  UITableView+Extensions.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extensions)

@property (nonatomic) IBInspectable NSString *cellName;

- (void)registerCell:(Class)classType;

@end
