#import "RPConfirmOrderVC.h"
#import "RPProductCollectionViewCell.h"
#import "UICollectionView+Extensions.h"
#import "RPOrderServerModel.h"
#import "RPCompanyListServerModel.h"
#import "RPServerManager.h"

@interface RPConfirmOrderVC () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *summLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (nonatomic, strong) UIDatePicker *datePickerView;
@property (nonatomic, strong) UIPickerView *deliveryPickerView;
@property (weak, nonatomic) IBOutlet UITextField *deliveryTextField;
@property (nonatomic, strong) RPPointServerModel *selectedDelivery;
@property (nonatomic, strong) NSString *selectedDate;
@property (weak, nonatomic) IBOutlet UILabel *deliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation RPConfirmOrderVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.productsCollectionView registerNib:[RPProductCollectionViewCell class]];
    self.summLabel.text = [NSString stringWithFormat:@"%ld p.", (long)self.order.order.total];
    self.priceLabel.text = [NSString stringWithFormat:@"%ld p.", (long)self.order.order.total];
    // Add UIPickerView for dates
    self.dateTextField.inputView = self.datePickerView;
    self.deliveryTextField.inputView = self.deliveryPickerView;
    [self.datePickerView addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Methods

- (void)finishOrder{
    __weak typeof(self) weakSelf = self;
    [self showMessage:@"Заказ успешно оформлен" withCompilation:^{
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popToRootViewControllerAnimated:YES];
    }];
}

#pragma mark - Getters 

- (UIDatePicker *)datePickerView{
    if (_datePickerView) {
        return _datePickerView;
    }
    _datePickerView = [[UIDatePicker alloc] init];
    _datePickerView.datePickerMode = UIDatePickerModeDate;
    _datePickerView.minimumDate = [[NSDate date] dateByAddingTimeInterval:1 * 24 * 60 * 60];
    _datePickerView.maximumDate = [[NSDate date] dateByAddingTimeInterval:30 * 24 * 60 * 60];
    return _datePickerView;
}

- (UIPickerView *)deliveryPickerView{
    if (_deliveryPickerView) {
        return _deliveryPickerView;
    }
    _deliveryPickerView = [[UIPickerView alloc] init];
    _deliveryPickerView.dataSource = self;
    _deliveryPickerView.delegate = self;
    return _deliveryPickerView;
}

#pragma mark - Actions

- (IBAction)datePickerValueChanged:(UIDatePicker *)datePicker {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    NSString *date = [dateFormat stringFromDate:datePicker.date];
    self.selectedDate = date;
    self.dateLabel.text = self.selectedDate;
}

- (IBAction)orderButtonPressed:(id)sender {
    if (!self.selectedDate) {
        [self showMessage:@"Выберите дату доставки"];
        return;
    }
    if (!self.selectedDelivery) {
        [self showMessage:@"Выберите место доставки"];
        return;
    }
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager] postOrderForProducts:self.cart date:self.selectedDate receiptId:self.selectedDelivery.ID onSuccess:^(RPOrderInfoServerModel *order, RPErrorServerModel *error) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        if (order) {
            [strongSelf finishOrder];
        } else {
            [strongSelf showMessage:error.text];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf hideMBProgressHUD];
        [strongSelf showMessage:error.localizedDescription];
    }];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return self.order.points.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.order.points[row].name;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == self.deliveryPickerView) {
        self.selectedDelivery = self.order.points[row];
        self.deliveryLabel.text = self.selectedDelivery.name;
    }
}

@end
