
#import "AppDelegate.h"
#import "RPLoadDataVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <MagicalRecord/MagicalRecord.h>
#import "RPCacheManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "RPStartVC.h"
#import "RPUserServerModel.h"
#import "SAMKeychain.h"
#import "RPServerManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // customize some controls
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Rupster Script Free" size:20]};
    [[UISegmentedControl appearance]setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [[UITextField appearance] setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    // add Fabric to testing app
    [Fabric with:@[[Crashlytics class]]];

    // add IQKeyboardmMnager to automatically scroll view when keyboard is showing
    [IQKeyboardManager sharedManager].enable = YES;
    
    // set up MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    // update cache every start
    [RPCacheManager updateCache];
    
    [SAMKeychain setAccessibilityType:kSecAttrAccessibleWhenUnlocked];
    [self registerPushNotifications:application];
    // set up start VC
    RPLoadDataVC *loadDataVC = [[RPLoadDataVC alloc] init];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = loadDataVC;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)registerPushNotifications:(UIApplication *) application {
    [FIRApp configure];
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    [FIRMessaging messaging].delegate = self;
    [application registerForRemoteNotifications];
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    self.fcmToken = fcmToken;
}

- (void)setCurrentUser:(RPUserServerModel *)currentUser{
    _currentUser = currentUser;
    // save data for log in to keychain
    [kUserDefaults setValue:currentUser.info.phone forKey:kPhoneKey];
    [kUserDefaults setValue:currentUser.info.accessToken forKey:kTokenKey];
    [kUserDefaults setValue:currentUser.info.pin forKey:kPinKey];

    [kUserDefaults synchronize];
    [[RPServerManager sharedManager] sendFCMTokenToServer];
//    [SAMKeychain setPassword:currentUser.info.phone forService:kPhoneKey account:kBundleID];
//    [SAMKeychain setPassword:currentUser.info.accessToken forService:kTokenKey account:kBundleID];
//    [SAMKeychain setPassword:currentUser.info.pin forService:kPinKey account:kBundleID];
}
@end
