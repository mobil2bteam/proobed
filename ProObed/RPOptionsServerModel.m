
#import "RPOptionsServerModel.h"

@implementation RPOptionsServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPTypeServerModel class] forKeyPath:@"types"];
        [mapping hasMany:[RPUrlServerModel class] forKeyPath:@"urls"];
        [mapping mapPropertiesFromArray:@[@"about"]];
    }];
}

@end

@implementation RPTypeServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"weight"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


@implementation RPUrlServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"url"]];
    }];
}

@end
