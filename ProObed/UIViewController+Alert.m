
#import "UIViewController+Alert.h"
#import "MBProgressHUD.h"
#import "RPErrorVC.h"

@implementation UIViewController (Alert)

- (void)showMessage:(NSString *)message{
    [self showMessage:message withCompilation:nil];
}


- (void)showMessage:(NSString *)message
  withCompilation:(void (^)(void))compilationBlock
{
    RPErrorVC *errorVC = [[RPErrorVC alloc]initWithNib];
    errorVC.message = message;
    if (compilationBlock) {
        errorVC.compilationBlock = compilationBlock;
    }
    [self RP_presentViewControllerCrossDisolve:errorVC animated:YES];
}


- (void)showMessage:(NSString *)message title:(NSString *)title
    withCompilation:(void (^)(void))compilationBlock
{
    RPErrorVC *errorVC = [[RPErrorVC alloc]initWithNib];
    errorVC.message = message;
    errorVC.titleText = title;
    if (compilationBlock) {
        errorVC.compilationBlock = compilationBlock;
    }
    [self RP_presentViewControllerCrossDisolve:errorVC animated:YES];
}

- (void)addMBProgressHUD{
    // if HUD is not already showed
    if (![MBProgressHUD HUDForView:self.view]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

- (void)hideMBProgressHUD{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
