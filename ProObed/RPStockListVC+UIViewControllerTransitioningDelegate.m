//
//  RPStockListVC+UIViewControllerTransitioningDelegate.m
//  ProObed
//
//  Created by Ruslan on 1/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStockListVC+UIViewControllerTransitioningDelegate.h"
#import "RPStockPreviewCollectionVeiwCell.h"

@implementation RPStockListVC (UIViewControllerTransitioningDelegate)

#pragma mark - <RMPZoomTransitionAnimating>

- (UIImageView *)transitionSourceImageView
{
    NSIndexPath *selectedIndexPath = [[self.stocksCollectionView indexPathsForSelectedItems] firstObject];
    RPStockPreviewCollectionVeiwCell *cell = (RPStockPreviewCollectionVeiwCell *)[self.stocksCollectionView cellForItemAtIndexPath:selectedIndexPath];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:cell.stockImageView.image];
    imageView.contentMode = cell.stockImageView.contentMode;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled = NO;
    imageView.frame = [cell.stockImageView convertRect:cell.stockImageView.bounds toView:self.view];
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_1"]];
}

- (CGRect)transitionDestinationImageViewFrame
{
    NSIndexPath *selectedIndexPath = [[self.stocksCollectionView indexPathsForSelectedItems] firstObject];
    RPStockPreviewCollectionVeiwCell *cell = (RPStockPreviewCollectionVeiwCell *)[self.stocksCollectionView cellForItemAtIndexPath:selectedIndexPath];
    CGRect cellFrameInSuperview = [cell.stockImageView convertRect:cell.stockImageView.bounds toView:self.view];
    return cellFrameInSuperview;
}

#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> sourceTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)source;
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> destinationTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)presented;
    if ([sourceTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)]) {
        RMPZoomTransitionAnimator *animator = [[RMPZoomTransitionAnimator alloc] init];
        animator.goingForward = YES;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> sourceTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)dismissed;
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> destinationTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)self;
    if ([sourceTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)]) {
        RMPZoomTransitionAnimator *animator = [[RMPZoomTransitionAnimator alloc] init];
        animator.goingForward = NO;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}

- (void)zoomTransitionAnimator:(RMPZoomTransitionAnimator *)animator
         didCompleteTransition:(BOOL)didComplete
      animatingSourceImageView:(UIImageView *)imageView{
    if (didComplete) {
        NSLog(@"finish 1");
    }
}

@end
