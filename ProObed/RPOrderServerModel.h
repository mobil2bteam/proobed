

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPCartServerModel;
@class RPStatusServerModel;
@class RPOrderListServerModel;
@class RPOrderInfoServerModel;
@class RPPointServerModel;
@class RPStatusOrderServerModel;

@interface RPOrderServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, strong) NSArray<RPPointServerModel *> *points;
@property (nonatomic, strong) RPOrderInfoServerModel *order;

+(EKObjectMapping *)objectMapping;
@end

@interface RPOrderInfoServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, assign) NSString *name;
@property (nonatomic, assign) NSInteger balls;;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger totalCart;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *statusTest;
@property (nonatomic, assign) NSInteger statusIdTest;
@property (nonatomic, strong) NSArray<RPCartServerModel *> *cart;
@property (nonatomic, strong) RPPointServerModel *point;

+(EKObjectMapping *)objectMapping;
@end

@interface RPPointServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *work;
@property (nonatomic, assign) NSInteger ID;

+(EKObjectMapping *)objectMapping;
@end

@interface RPStatusOrderServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger ID;

+(EKObjectMapping *)objectMapping;
@end

@interface RPOrderListServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, strong) NSArray<RPOrderServerModel *> *orders;

+(EKObjectMapping *)objectMapping;
@end

@interface RPStatusServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *color;
@property (nonatomic, copy) NSString *text_color;
@property (nonatomic, copy) NSString *type;

+(EKObjectMapping *)objectMapping;
@end


@interface RPCartServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger total;

+(EKObjectMapping *)objectMapping;
@end
