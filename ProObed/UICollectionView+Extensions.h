//
//  UICollectionView+Extensions.h
//  ProObed
//
//  Created by Ruslan on 2/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (Extensions)

- (void)registerNib:(Class)nibClass;

- (void)registerNib:(Class)nibClass forSupplementaryViewOfKind:(NSString *)kind;

@end
