//
//  RPBalls.h
//  ProObed
//
//  Created by Ruslan on 11/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPOperationServerModel;

@interface RPBallServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger value;

@property (nonatomic, strong) NSArray<RPOperationServerModel *> *operations;

+(EKObjectMapping *)objectMapping;

@end


@interface RPOperationServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) CGFloat value;

@property (nonatomic, assign) NSInteger balsId;

@property (nonatomic, strong) NSString *date;

@end
