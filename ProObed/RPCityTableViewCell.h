//
//  RPCityTableViewCell.h
//  ProObed
//
//  Created by Ruslan on 11/4/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPCityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@end
