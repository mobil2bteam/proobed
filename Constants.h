//
//  Constants.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UIViewController+Alert.h"
#ifndef Constants_h
#define Constants_h

// ru.duckman.proservice
// com.proService.ProObed

typedef NS_ENUM(NSInteger, UserType)
{
    UserTypeWorker = 1,
    UserTypeParent = 2,
    UserTypeChild = 3
};

//------------------------------------- API Keys ---------------------------------------------

static NSString * const APIKeyOptions = @"url_options";

static NSString * const APIKeyCities = @"url_cities";

static NSString * const APIKeyCompanies = @"url_companies";

static NSString * const APIKeyLogin = @"url_login";

static NSString * const APIKeyUserVerify = @"url_user_verify";

static NSString * const APIKeyEmailVerify = @"url_user_verify_email";

static NSString * const APIKeyRegistration = @"url_registration";

static NSString * const APIKeySecondParentRegistration = @"url_child_registration_parent";

static NSString * const APIKeyChildRegistration = @"url_child_registration";

static NSString * const APIKeyVerifyChild = @"url_child_verify";

static NSString * const APIKeyFeedbackCreate = @"url_feedback";

static NSString * const APIKeyStocks = @"url_stocks";

static NSString * const APIKeyRecoverPassword = @"url_recover";

static NSString * const APIKeyChangePin = @"url_recover";

static NSString * const APIKeyMenu = @"url_menu";

static NSString * const APIKeyProducts = @"url_products";

static NSString * const APIKeyCart = @"url_cart";

static NSString * const APIKeyOrder = @"url_order";

static NSString * const APIKeyUserOrders = @"url_user_orders";

static NSString * const APIKeySurvey = @"url_survey";

static NSString * const APIKeySendSurvey = @"url_survey_success";

static NSString * const APIKeyBals = @"url_bals";

static NSString * const APIKeyTickets = @"url_tickets";

static NSString * const APIKeyChildTickets = @"url_child_tickets";

static NSString * const APIKeyFeedbackTypes = @"url_feedback_types";

//

static NSString * const kAskPinCodeKey = @"ask_pin_code";

static NSString * const kLoginKey = @"login";

static NSString * const kPhoneKey = @"phone";

static NSString * const kTokenKey = @"token";

static NSString * const kPinKey = @"pin";

//---------------------------------------------Defines--------------------------------------------------

#define kAppDelegate ((AppDelegate *)[[UIApplication sharedApplication]delegate])

#define RPCurrentUser kAppDelegate.currentUser

#define kUserDefaults [NSUserDefaults standardUserDefaults]

#define kScreenWidth [UIScreen mainScreen].bounds.size.width

#define kGreenColor [UIColor colorWithRed:0.000 green:0.671 blue:0.400 alpha:1.00]

#define kRedColor [UIColor colorWithRed:0.808 green:0.208 blue:0.208 alpha:1.00]

//

//------------------------------------ Error Messages----------------------------------

static NSString * const kBundleID = @"com.ProService.ProObed";

static NSString * const RPVKIdentifier = @"5331790";

static NSString * const kAddressSite =  @"http://proobed.procervic.ru/api";

//

#endif /* Constants_h */



